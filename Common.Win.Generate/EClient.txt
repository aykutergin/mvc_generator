﻿using #dtons;
using #restns;
using System;
using System.Collections.Generic;

namespace #clientns
{
    public class #tblnameClient : EClient
    {
      
        public RestClient rest { get; set; }
        
        public #tblnameClient(){
             rest = new RestClient(baseAddress,apiKey);
            }

        public ResultModel Get(List<UriParam> p) {
            return rest.Get<ResultModel>("#tblname/get", p); 
        }

        public ResultModel Get(int id,List<UriParam> p)
        {
            return rest.Get<ResultModel>(String.Format("#tblname/{0}/get",id), p);
        }

        public int Count(int id, List<UriParam> p)
        {
            return rest.Get<int>(String.Format("#tblname/count", id), p);
        }

        public ResultModel Create(#tblnameDto dto)
        {
            return rest.Post<#tblnameDto,ResultModel>(dto,String.Format("#tblname/create"));
        }

        public ResultModel Update(#tblnameDto dto)
        {
            return rest.Post<#tblnameDto, ResultModel>(dto, String.Format("#tblname/update"));
        }

        public bool Delete(int id)
        {
            return rest.Post<int, bool>(id, String.Format("#tblname/delete"));
        }
    }
}
