﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Avs.Win.Generate
{
    public class ForeignKey
    {
        public Column column { get; set; }
        public Table table { get; set;}

        public ForeignKey()
        { 
            
        }

        public ForeignKey(Column column, Table table)
        {
            this.column = column;
            this.table = table;
        }
    }
}
