﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Avs.Win.Generate
{
    internal class Relationship
    {
        public string foreignKeyName { get; set; }
        public string tableName { get; set; }
        public string columnName { get; set; }
        public string refTableName { get; set; }
        public string refColumnName { get; set; }

        public Relationship()
        { 
            
        }
        public Relationship(string foreignKeyName, string tableName, string columnName, string refTableName, string refColumnName)
        {
            this.foreignKeyName = foreignKeyName;
            this.tableName = tableName;
            this.columnName = columnName;
            this.refTableName = refTableName;
            this.refColumnName = refColumnName;
        }

    }
}
