﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Avs.Win.Generate
{
    public class Column
    {
        public string name { get; set; }
        public string dataType { get; set; }
        public string sqlDataType { get; set; }
        private string m_sqlType;
        public string sqlType 
        {
            get { return m_sqlType; }
            set
            {
                m_sqlType = value;
                columntype = GetColumnType();
            }
        
        }
        public int dataLength { get; set; }
        public bool isIdentity { get; set; }
        public bool isNullable { get; set; }
        public bool isPrimery { get; set; }
        public bool isForeign { get; set; }
        public string description { get; set; }
        public ForeignKey foreign { get; set; }
        public ColumnType columntype { get; set;}
        public Column()
        {
            foreign = new ForeignKey();
        }

        public Column(string name, string dataType, string sqlDataType,string sqlType, int dataLength, bool isIdentity, bool isNullable, string description, bool isPrimery, bool isForeign)
        {
            foreign = new ForeignKey();
            this.name = name;
            this.dataType = dataType;
            this.sqlDataType = sqlDataType;
            this.sqlType = sqlType;
            this.dataLength = dataLength;
            this.isIdentity = isIdentity;
            this.isNullable = isNullable;
            this.description = description;
            this.isPrimery = isPrimery;
            this.isForeign = isForeign;
            columntype=GetColumnType();
        }

        public Column GetColumn(string tableName, string columnName)
        {
            DataTable dt = tools.GetTableRows(tableName);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i][0].ToString() == tableName && dt.Rows[i][1].ToString() == columnName)
                {
                    string description = (dt.Rows[i][6].ToString() == String.Empty) ? dt.Rows[i][1].ToString() : dt.Rows[i][6].ToString();
                    return new Column(dt.Rows[i][1].ToString(), tools.convertSqlDataType(dt.Rows[i][2].ToString(), Convert.ToInt32(dt.Rows[i][5]), dt.Rows[i][0].ToString(), dt.Rows[i][1].ToString()), tools.convertSqlDbType(dt.Rows[i][2].ToString()), dt.Rows[i][2].ToString(), Convert.ToInt32(dt.Rows[i][3]), tools.IsIdentity(dt.Rows[i][4].ToString()), Convert.ToBoolean(dt.Rows[i][5]), description, tools.IsPrimeryKey(tools.GetPrimaryKeys(tableName), dt.Rows[i][1].ToString()), tools.IsForeignKey(tools.GetForeignKeys(tableName), dt.Rows[i][1].ToString()));
                }
            }
            return null;
          
        }

        public List<Column> GetColumnList(string tableName,bool isContainForeign)
        {
            List<Column> collist = new List<Column>();
            Column col = new Column();
            RelationshipList rellist = new RelationshipList();
            Table table = new Table();
            DataTable dt = tools.GetTableRows(tableName);
            for (int i = 0; i <dt.Rows.Count; i++)
            {
                string description = (dt.Rows[i][6].ToString() == String.Empty) ? dt.Rows[i][1].ToString() : dt.Rows[i][6].ToString();
                col = new Column(dt.Rows[i][1].ToString(), tools.convertSqlDataType(dt.Rows[i][2].ToString(), Convert.ToInt32(dt.Rows[i][5]), dt.Rows[i][0].ToString(), dt.Rows[i][1].ToString()), tools.convertSqlDbType(dt.Rows[i][2].ToString()), dt.Rows[i][2].ToString(), Convert.ToInt32(dt.Rows[i][3]), tools.IsIdentity(dt.Rows[i][4].ToString()), Convert.ToBoolean(dt.Rows[i][5]), description, tools.IsPrimeryKey(tools.GetPrimaryKeys(tableName), dt.Rows[i][1].ToString()), tools.IsForeignKey(tools.GetForeignKeys(tableName), dt.Rows[i][1].ToString()));
                if (isContainForeign)
                {
                    if(col.isForeign)
                        col.foreign = new ForeignKey(col.GetColumn(tableName, dt.Rows[i][1].ToString()), table.GetTable(rellist.GetTargetTableName(tableName, dt.Rows[i][1].ToString()), false));
                }
                collist.Add(col);
            }
            return collist;
        }

        public ColumnType GetColumnType()
        {
            string up_columnname = this.name.ToUpper();
            string up_sqltype = sqlType.ToUpper();
            string low_columnname = this.name.ToLower();
            string low_sqltype = sqlType.ToLower();
            if(this.isForeign) return ColumnType.ComboBox;
            if (this.isPrimery && this.isIdentity) return ColumnType.Primery;
            if (up_columnname.Contains("IMAGE") || low_columnname.Contains("image")) return ColumnType.Image;
            if (up_columnname.Contains("NAME") || low_columnname.Contains("name")) return ColumnType.Name;
            if (up_columnname.Contains("LINK") || low_columnname.Contains("link")) return ColumnType.Url;
            if (up_columnname.Contains("EMAIL") || low_columnname.Contains("email")) return ColumnType.Email;
            else if (up_columnname.Contains("FILE") || low_columnname.Contains("file") || up_sqltype == "BINARY" || low_sqltype == "binary" || up_sqltype== "IMAGE" || low_sqltype == "image") return ColumnType.File;
            else if (up_columnname.Contains("HTML")) return ColumnType.HtmlText;
            else if (up_sqltype == "NTEXT" || low_sqltype == "ntext" || up_sqltype == "TEXT" || low_sqltype == "text") return ColumnType.TextArea;
            else if (up_sqltype == "DATE" || low_sqltype == "date" || up_sqltype == "DATETIME" || low_sqltype == "datetime" || up_sqltype == "DATETIME2" || low_sqltype == "datetime2" || up_sqltype == "TIME" || low_sqltype == "time" || up_sqltype == "SMALLDATETIME" || low_sqltype == "smalldatetime") return ColumnType.Date;
            else if (up_sqltype == "BIT" || low_sqltype == "bit") return ColumnType.CheckBox;
            else if (up_sqltype == "INT" || low_sqltype == "int" || up_sqltype == "BIGINT" || low_sqltype == "bigint") return ColumnType.Number;
            else return ColumnType.Text;
        }
    }
}
