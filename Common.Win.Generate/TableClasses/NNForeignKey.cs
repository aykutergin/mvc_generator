﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Avs.Win.Generate
{
    public class NNForeignKey
    {
        public Column targetColumn { get; set; }
        public Column gapColumn { get; set; }
        public Table targetTable { get; set; }
        public Table gapTable { get; set; }

        public NNForeignKey(Column targetColumn, Table targetTable, Column gapColumn, Table gapTable)
        {
            this.targetColumn = targetColumn;
            this.targetTable = targetTable;
            this.gapColumn = gapColumn;
            this.gapTable = gapTable;
        }
               
    }
}
