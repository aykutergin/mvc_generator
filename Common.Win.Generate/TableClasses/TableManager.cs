﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Avs.Win.Generate
{
    class TableManager
    {
       public Table table { get; set; }
       public RelationshipList relationlist { get; set; }
       public Column column { get; set; }

       public TableManager()
       {
           table = new Table();
           relationlist = new RelationshipList();
           column = new Column();
       }

       public Table CreateTable(string tableName)
       {
           table = table.GetTable(tableName,true);
           table.SetConstructerParameter();
           table.nnrel_foreign = relationlist.GetNNForeignKeyList(tableName);
           table.ref_foreign = relationlist.GetRefForeign(tableName);
           return table;
       }
    }
}
