﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Avs.Win.Generate
{
    public class Table
    {
        #region variables

        private string m_name;
        public string namePlural { get; set; }


        public string name 
        {
            get { return m_name; }
            set 
            { 
                m_name = value;
                tabletype = GetTableType();
            }
        
        }

       
        public string allparameters { get; set; }
        public string allparameters_notprimery { get; set; }

        public TableType tabletype { get; set; }

        public Column primery_column { get; set; }

        private List<Column> m_columns;
        public List<Column> columns
        {
            get
            {
                return m_columns;
            }
            set
            {
                m_columns = value;
                SetForeignColumns();
                SetPrimeryColumn();
            }
        }
        
        public List<ForeignKey> foreigns { get; set; }
        public List<ForeignKey> ref_foreign { get; set; }
        public List<NNForeignKey> nnrel_foreign { get; set; }


        #endregion

        #region constructer
        public Table()
        {
            ref_foreign = new List<ForeignKey>();
            foreigns = new List<ForeignKey>();
            nnrel_foreign = new List<NNForeignKey>();
        }
        public Table(string name, List<Column> columns)
        {
            ref_foreign = new List<ForeignKey>();
            foreigns = new List<ForeignKey>();
            nnrel_foreign = new List<NNForeignKey>();
            this.name = name;
            this.columns = columns;
            allparameters = String.Empty;
            allparameters_notprimery = String.Empty;
            
        }
        #endregion

        #region functions
        public void SetConstructerParameter()
        {
            
            foreach (Column item in columns)
            {
                
                    allparameters += item.dataType + " " + item.name + ",";
                    if (item.isPrimery == false)
                        allparameters_notprimery += item.dataType + " " + item.name + ",";
               
            }
            allparameters=allparameters.Remove(allparameters.LastIndexOf(','));
            allparameters_notprimery = allparameters_notprimery.Remove(allparameters_notprimery.LastIndexOf(','));
            
           
        }

        public Table GetTable(string tableName,bool isContainForeign)
        {
            Column column=new Column();
            Table table = new Table(tableName, column.GetColumnList(tableName, isContainForeign));
            return table;
        }


        public string GetBcParameters(string prefix)
        {
            string parameters = String.Empty;
            foreach (Column item in columns)
            {
                parameters = parameters + " " + prefix + ".Get" + item.name + "(),";
            }
            parameters = parameters.Remove(parameters.LastIndexOf(','));
            return parameters;
        }

        public Column GetPrimeryColumn()
        {
            
            foreach (Column item in columns)
            {
                if (item.isPrimery)
                    return item;
            }

            return columns[0];
            
        }

        public string GetSqlColumns()
        {
            string par = String.Empty;
            foreach (Column item in columns)
            {
                par = par +" t."+ item.name + ",";
            }
            par = par.Remove(par.LastIndexOf(','));
            return par;
        }

        public string GetModelParameters(string prefix)
        {
            string parameters = String.Empty;
            foreach (Column item in columns)
            {
                parameters = parameters + " " + prefix + "." + item.name + ",";
            }
            parameters = parameters.Remove(parameters.LastIndexOf(','));
            return parameters;
        }

        public string GetModelParameters_withoutPrimery(string prefix)
        {
            string parameters = String.Empty;
            foreach (Column item in columns)
            {
                if(!item.isPrimery)
                parameters = parameters + " " + prefix + "." + item.name + ",";
            }
            parameters = parameters.Remove(parameters.LastIndexOf(','));
            return parameters;
        }

        public void SetForeignColumns()
        {
            foreach (Column col in columns)
            {
                if (col.isForeign)
                {
                    if(col.foreign!=null)
                     foreigns.Add(col.foreign);
                }
            } 
        
        }

        public void SetPrimeryColumn()
        {
            primery_column = GetPrimeryColumn();
        }

        public TableType GetTableType()
        {
            if (name.ToUpper().Contains("IMAGE") || name.ToLower().Contains("image")) return TableType.Image;
            else if (name.ToUpper().Contains("FILE") || name.ToLower().Contains("file")) return TableType.File;
            else return TableType.Default;
        }


        public Column GetNameColumn()
        {
            Column temp=new Column();
            foreach (Column item in columns)
            {
                if (item.columntype==ColumnType.Name)
                    return item;
                if (item.columntype == ColumnType.Primery)
                    temp = item;
            }
            return temp;
        }

        public Column GetFileColumn()
        {

            foreach (Column item in columns)
            {
                if (item.columntype == ColumnType.File)
                    return item;
            }
            return null;

        }
        public Column GetImageColumn()
        {

            foreach (Column item in columns)
            {
                if (item.columntype == ColumnType.Image)
                    return item;
            }
            return null;

        }

        #endregion

        internal bool isNNTable()
        {
            return this.name.Contains("_X_");
        }
    }
}
