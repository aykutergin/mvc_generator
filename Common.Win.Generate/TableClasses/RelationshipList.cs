﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Avs.Win.Generate
{
    class RelationshipList
    {
        public List<Relationship> relationshipList { get; set;}
        public Column column { get; set; }
        public Table table { get; set; }

        public RelationshipList()
        {
            relationshipList = new List<Relationship>();
            column = new Column();
            table = new Table();
            FillRelationshipList();
        }

        public void FillRelationshipList()
        { 
           DataTable dt=tools.getForeginTable();
            for (int i = 0; i < dt.Rows.Count; i++)
			{
			  relationshipList.Add(new Relationship(dt.Rows[i][0].ToString(),dt.Rows[i][2].ToString(),dt.Rows[i][3].ToString(),dt.Rows[i][5].ToString(),dt.Rows[i][6].ToString()));
			}
        }
        public List<ForeignKey> GetRefForeign(string tableName)
        {
            List<ForeignKey> keylist = new List<ForeignKey>();

            ForeignKey forkey;
            foreach (Relationship item in relationshipList)
            {
                if (item.refTableName == tableName)
                {
                    if (!item.tableName.Contains("_X_"))
                    {
                        forkey = new ForeignKey(column.GetColumn(item.tableName, item.columnName), table.GetTable(item.tableName,false));
                        keylist.Add(forkey);
                    }
                }
            }
            return keylist;
            
        }
        public List<NNForeignKey> GetNNForeignKeyList(string tableName)
        {
            List<NNForeignKey> nnforlist = new List<NNForeignKey>();
            Column col=new Column();
            Table table=new Table();
            Relationship temp;
            foreach (Relationship item in relationshipList)
            {
                if (item.refTableName == tableName)
                {
                    if (item.tableName.Contains("_X_"))
                    {
                        temp = GetTargetRel(item.tableName, item.columnName);
                        nnforlist.Add(new NNForeignKey(column.GetColumn(temp.refTableName,temp.refColumnName),table.GetTable(temp.refTableName,false),column.GetColumn(temp.tableName,temp.columnName),table.GetTable(temp.tableName,false)));
                    }
                }
            }
            return nnforlist;
        }
        public Relationship GetTargetRel(string tableName,string NotContainColumnName)
        {
            foreach (Relationship item in relationshipList)
            {
                if (item.tableName == tableName && item.columnName != NotContainColumnName)
                {
                    return item;
                }
            }
            return null;
        }

        public string GetTargetTableName(string tableName, string ColumnName)
        {
            foreach (Relationship item in relationshipList)
            {
                if (item.tableName == tableName && item.columnName == ColumnName)
                {
                    return item.refTableName;
                }
            }
            return null;
        }
    }
}
