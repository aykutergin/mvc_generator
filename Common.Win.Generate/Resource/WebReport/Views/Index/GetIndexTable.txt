﻿@using Dcl.Avs3.Web.Report.Modul
@using Dcl.Avs3.Web.Report.Models
@using Dcl.Avs3.Web.Report.Enums
@model #tPureNameViewModel
@if (Model._#tNameModelList != null && Model._#tNameModelList.Count > 0)
{
	<div>
		<table class="table table-bordered table-hover table-checkable" id="kt_datatable">
			<thead>
				<tr>#thList
				</tr>
			</thead>
			<tbody>
				@foreach (var item in Model._#tNameModelList)
				{
					<tr id="item-@item.#primaryKey">#tdList
					</tr>
				}
			</tbody>
		</table>
	</div>
    @Html.Partial("_Pagination", Model.pagination)
    <input type="hidden" id="paginationFor" value="#tPureName" />
}
else
{
    <hr />
    <div class="text-center"><span class="text-primary font-size-h4-lg">@LangVal.Ins.GetVal("#tName.Index.Alert.Table", "Aradığınız kriterlere uygun kayıt bulunamadı.")</span></div>
    <hr />
}