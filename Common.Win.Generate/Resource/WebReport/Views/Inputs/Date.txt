﻿
				<label class="col-lg-2 col-6 col-form-label text-lg-right">@LangVal.Ins.GetVal("#tName.#pageName.Text.#colName","#colName")</label>
                <div class="col-lg-3 col-6 ">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="la la-calendar-check-o"></i>
                            </span>
                        </div>
                        @Html.TextBoxFor(model => model.#colName, new { @class = "form-control", @type = "text", id = "kt_datepicker", name = "#colName", autocomplete = "off", placeholder = @LangVal.Ins.GetVal("#tName.#pageName.Text.#colName","#colName") })
                    </div>
                    <span class="form-text text-muted"></span>
                </div>