﻿
				<label class="col-lg-2 col-6  col-form-label text-lg-right">@LangVal.Ins.GetVal("#tForName.#pageName.Text.#colName","#colName")</label>
                <div class="col-lg-3 col-6 ">
                    @Html.Action("_#tForNameDropdownPartial", "Dropdown", new { selectedId = Model==null?0:Model.#colName,  inputName="_#tNameVwModel.#colName"})
                    <span class="form-text text-muted">@LangVal.Ins.GetVal("#tForName.#pageName.Info.#colName","#colName")</span>
                </div>