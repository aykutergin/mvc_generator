using Dcl.Avs3.Web.Report.Model.ApiCrud;
using Dcl.Avs3.Web.Report.Model.ApiGet;
using Dcl.Avs3.Web.Report.Model.Filter;
using System.Collections.Generic;

namespace Dcl.Avs3.Lib.Rest.ISource
{
    public interface I#tNameSource
    {
        List<#tNameVwModel> Get(#tNameVwFilterModel f=null);
        #tNameModel Get(#pcoltype id);
        List<#tNameModel> GetList(#tNameVwFilterModel f=null);
        #tNameModel Create(#tNameModel model);
        #tNameModel Update(#tNameModel model);
        bool Delete(#pcoltype id);
        List<#tNameModel> CreateAll(List<#tNameModel> modelList);
        List<#tNameModel> UpdateAll(List<#tNameModel> modelList);
        bool DeleteAll(List<#tNameModel> modelList);
        int Count(#tNameVwFilterModel f = null);
    }
}
