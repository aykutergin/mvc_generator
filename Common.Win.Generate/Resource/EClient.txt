﻿using #dtons;
using #restns;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace #clientns
{
    public class #tblnameClient : EClient
    {
      
        public RestClient rest { get; set; }
        
        public #tblnameClient(){
             rest = new RestClient(baseAddress,apiKey);
            }

        public ResultModel Get(List<UriParam> p) {
			var result = rest.Get<ResultModel>("#tblname/get", p);
			if(result.result==1)
				result.resultData = JsonConvert.DeserializeObject<List<#tblnameDto>>(result.resultData.ToString());
            return result;
        }

        public ResultModel Get(#prcolumntype id,List<UriParam> p)
        {
			var result = rest.Get<ResultModel>(String.Format("#tblname/{0}/get",id.ToString().Replace(",",".")), p);
			if(result.result==1)
				result.resultData = JsonConvert.DeserializeObject<#tblnameDto>(result.resultData.ToString());
            return result;
        }
		          

        public int Count(List<UriParam> p)
        {
            var model = rest.Get<ResultModel>(String.Format("#tblname/count"), p);
			return Convert.ToInt32(model.resultData);
        }

        public ResultModel Create(#tblnameDto dto)
        {
			var result = rest.Post<#tblnameDto,ResultModel>(dto,String.Format("#tblname/create"));
            result.resultData = JsonConvert.DeserializeObject<#tblnameDto>(result.resultData.ToString());
            return result;
        }

        public ResultModel Update(#tblnameDto dto)
        {
            var result = rest.Post<#tblnameDto, ResultModel>(dto, String.Format("#tblname/update"));
			   result.resultData = JsonConvert.DeserializeObject<#tblnameDto>(result.resultData.ToString());
            return result;
        }

        public bool Delete(#tblnameDto dto)
        {
			var result = rest.Post<#tblnameDto, ResultModel>(dto, String.Format("#tblname/delete"));
            return Convert.ToBoolean(result.resultData);
        }
    }
}
