﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Avs.Win.Generate
{
    class GreatAdmin_Layout
    {
        static string filePath = Constants.ViewFolder + "\\Shared\\_AdminLayout.cshtml";
        StreamWriter file = new StreamWriter(filePath, false, Encoding.UTF8);//(filePath);
        public void Write()
        {
            MenuManager menu = Constants.menumanager;
            file.WriteLine("@{");
            file.WriteLine("Layout = null;");
            file.WriteLine("ViewBag.Title = \"Yönetici Modülü\";");
            file.WriteLine("}");
            file.WriteLine("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">");
            file.WriteLine("<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"cs\" lang=\"cs\">");
            file.WriteLine("  ");
            file.WriteLine("<!-- Mirrored from dl.dropbox.com/u/1652861/great-admin/gray/homepage.html by HTTrack Website Copier/3.x [XR&CO'2010], Sat, 14 Jan 2012 11:57:31 GMT -->");
            file.WriteLine("<!-- Added by HTTrack --><meta http-equiv=\"content-type\" content=\"text/html;charset=ascii\"><!-- /Added by HTTrack -->");
            file.WriteLine("<head>");
            file.WriteLine("    <meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />");
            file.WriteLine("    <meta name='robots' content='all, follow' />");
            file.WriteLine("    <meta name=\"description\" content=\"\" />");
            file.WriteLine("    <meta name=\"keywords\" content=\"\" />");
            file.WriteLine("    <title>Yönetim Paneli</title>   ");
            file.WriteLine("    <link href=\"@Url.Content(\"~/Content/admin/css/default.css\")\" rel=\"stylesheet\" type=\"text/css\" media=\"screen\" />");
            file.WriteLine("    <link href=\"@Url.Content(\"~/Content/admin/css/gray.css\")\" rel=\"stylesheet\" type=\"text/css\" media=\"screen\" /> <!-- color skin: blue / red / green / dark -->");
            file.WriteLine("    <link href=\"@Url.Content(\"~/Content/admin/css/datePicker.css\")\" rel=\"stylesheet\" type=\"text/css\" media=\"screen\" />");
            file.WriteLine("    <link href=\"@Url.Content(\"~/Content/admin/css/wysiwyg.css\")\" rel=\"stylesheet\" type=\"text/css\" media=\"screen\" />");
            file.WriteLine("    <link href=\"@Url.Content(\"~/Content/admin/css/fancybox-1.3.1.css\")\" rel=\"stylesheet\" type=\"text/css\" media=\"screen\" />");
            file.WriteLine("    <link href=\"@Url.Content(\"~/Content/admin/css/visualize.css\")\" rel=\"stylesheet\" type=\"text/css\" media=\"screen\" />");
            file.WriteLine("    ");
            file.WriteLine("    <script type=\"text/javascript\" src=\"@Url.Content(\"~/Content/admin/js/jquery-1.4.2.min.js\")\"></script>   ");
            file.WriteLine("    <script type=\"text/javascript\" src=\"@Url.Content(\"~/Content/admin/js/jquery.dimensions.min.js\")\"></script>");
            file.WriteLine("          <script src=\"@Url.Content(\"~/Content/admin/js/jquery.uploadify.js\")\" type=\"text/javascript\"></script>");
            file.WriteLine("    <link href=\"@Url.Content(\"~/Content/Admin/css/uploadify.css\")\" rel=\"stylesheet\" type=\"text/css\" />");
            file.WriteLine(" ");
            file.WriteLine("    <!-- // Tabs // -->");
            file.WriteLine("    <script type=\"text/javascript\" src=\"@Url.Content(\"~/Content/admin/js/ui.core.js\")\"></script>");
            file.WriteLine("    <script type=\"text/javascript\" src=\"@Url.Content(\"~/Content/admin/js/jquery.ui.tabs.min.js\")\"></script>");
            file.WriteLine(" ");
            file.WriteLine("    <!-- // Table drag and drop rows // -->");
            file.WriteLine("    <script type=\"text/javascript\" src=\"@Url.Content(\"~/Content/admin/js/tablednd.js\")\"></script>");

            file.WriteLine("    <!-- // Date Picker // -->");
            file.WriteLine("    <script type=\"text/javascript\" src=\"@Url.Content(\"~/Content/admin/js/date.js\")\"></script>");
            file.WriteLine("    <!--[if IE]><script type=\"text/javascript\" src=\"@Url.Content(\"~/Content/admin/js/jquery.bgiframe.js\")\"></script><![endif]-->");
            file.WriteLine("    <script type=\"text/javascript\" src=\"@Url.Content(\"~/Content/admin/js/jquery.datePicker.js\")\"></script>");

            file.WriteLine("    <!-- // Wysiwyg // -->");
            file.WriteLine("    <script type=\"text/javascript\" src=\"@Url.Content(\"~/Content/admin/js/jquery.wysiwyg.js\")\"></script>");

            file.WriteLine("    <!-- // Graphs // -->");
            file.WriteLine("    <script type=\"text/javascript\" src=\"@Url.Content(\"~/Content/admin/js/excanvas.js\")\"></script>");
            file.WriteLine("    <script type=\"text/javascript\" src=\"@Url.Content(\"~/Content/admin/js/jquery.visualize.js\")\"></script>");

            file.WriteLine("    <!-- // Fancybox // -->");
            file.WriteLine("  	<script type=\"text/javascript\" src=\"@Url.Content(\"~/Content/admin/js/jquery.fancybox-1.3.1.js\")\"></script>");

            file.WriteLine("    <!-- // File upload // --> ");
            file.WriteLine("    <script src=\"@Url.Content(\"~/Content/admin/js/jquery.filestyle.js\")\" type=\"text/javascript\"></script>");
            file.WriteLine("    ");
            file.WriteLine("    <script type=\"text/javascript\" src=\"@Url.Content(\"~/Content/admin/js/init.js\")\"></script>");
            file.WriteLine("        <script src=\"@Url.Content(\"~//Scripts/jquery.unobtrusive-ajax.min.js\")\" type=\"text/javascript\"></script>");
            file.WriteLine("    <script src=\"@Url.Content(\"~/Scripts/jquery.validate.min.js\")\" type=\"text/javascript\"></script>");
            file.WriteLine("    <script src=\"@Url.Content(\"/Scripts/jquery.validate.unobtrusive.min.js\")\" type=\"text/javascript\"></script>");

            file.WriteLine("    <!-- Google Analytics -->");
            file.WriteLine("    <script type=\"text/javascript\">");
            file.WriteLine("        var _gaq = _gaq || [];");
            file.WriteLine("        _gaq.push(['_setAccount', 'UA-797086-8']);");
            file.WriteLine("        _gaq.push(['_trackPageview']);");

            file.WriteLine("        (function () {");
            file.WriteLine("            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;");
            file.WriteLine("            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';");
            file.WriteLine("            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);");
            file.WriteLine("        })();    ");


file.WriteLine("       function showSpinner() {");

file.WriteLine("           document.getElementById(\"spinner\").style.display = \"inline\";");
file.WriteLine("       }");

file.WriteLine("       function hideSpinner() {");
file.WriteLine("           document.getElementById(\"spinner\").style.display = \"none\");)");
file.WriteLine("       }");


   
   
   
  file.WriteLine("      function submitSearch() {");
  file.WriteLine("          document.getElementById(\"searchForm\").submit();");
  file.WriteLine("      }\");");


  file.WriteLine("      $(document).ready(function () {\");");
  file.WriteLine("          var a = readCookie('selectedMenu');");
  file.WriteLine("          if (a != null) {");
  file.WriteLine("              var str = \"topmenu\";");
  file.WriteLine("              var d = document.getElementById(str.concat(a));");
  file.WriteLine("              d.className = d.className + \" active\";");

 file.WriteLine("               str = \"submenu\";");
  file.WriteLine("              if (document.getElementById(str.concat(a))!=null)");
  file.WriteLine("              document.getElementById(str.concat(a)).style.display = \"block\";");

 file.WriteLine("           }");


 file.WriteLine("              $(\".fancyopener\").fancybox({");
 file.WriteLine("                  'titlePosition': 'inside',");
 file.WriteLine("                  'transitionIn': 'none',");
         file.WriteLine("        'transitionOut': 'none'\");");
 file.WriteLine("              });");
 file.WriteLine("          });");
     

     

 file.WriteLine("          function createCookie(name, value, days) {");
 file.WriteLine("              if (days) {");
 file.WriteLine("                  var date = new Date();");
  file.WriteLine("                 date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));");
 file.WriteLine("                  var expires = \"; expires=\" + date.toGMTString();");
 file.WriteLine("              }");
 file.WriteLine("              else var expires = \"\";");
 file.WriteLine("              document.cookie = name + \"=\" + value + expires + \"; path=/\";");
 file.WriteLine("          }");

 file.WriteLine("        function readCookie(name) {");
 file.WriteLine("            var nameEQ = name + \"=\";");
 file.WriteLine("            var ca = document.cookie.split(';');");
 file.WriteLine("            for (var i = 0; i < ca.length; i++) { ");
  file.WriteLine("               var c = ca[i];");
  file.WriteLine("               while (c.charAt(0) == ' ') c = c.substring(1, c.length);");
 file.WriteLine("                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);");
  file.WriteLine("           }");
  file.WriteLine("           return null;");
  file.WriteLine("       }");

   file.WriteLine("      function eraseCookie(name) {");
   file.WriteLine("          createCookie(name, \"\", -1);");
  file.WriteLine("       }");
   
   
   

            file.WriteLine("    </script>");

   
            file.WriteLine("            <style type=\"text/css\">");
         file.WriteLine("      .spinner");
         file.WriteLine("      {");
         file.WriteLine("          position: fixed;");
         file.WriteLine("          top: 50%;");
          file.WriteLine("         left: 50%;");
         file.WriteLine("          margin-left: -100px; /* half width of the spinner gif */");
          file.WriteLine("         margin-top: -100px; /* half height of the spinner gif */");
          file.WriteLine("         text-align: center;");
          file.WriteLine("         z-index: 1234;");
          file.WriteLine("         overflow: auto;");
          file.WriteLine("         width: 220px; /* width of the spinner gif */");
           file.WriteLine("        height: 220px; /*hight of the spinner gif +2px to fix IE8 issue */");
        file.WriteLine("       }");
      file.WriteLine("     </style>");


            file.WriteLine("  </head>   ");
            file.WriteLine("  <body>");
            file.WriteLine("  <div id=\"main\">");
            file.WriteLine("    <!-- #header -->");
            file.WriteLine("    <div id=\"header\"> ");
            file.WriteLine("      <!-- #logo --> ");
            file.WriteLine("      <div id=\"logo\">");
            file.WriteLine("        <a href=\"index.html\" title=\"Go to Homepage\"><span>Great Admin</span></a>");
            file.WriteLine("      </div>");
            file.WriteLine("      <!-- /#logo -->");
            file.WriteLine("      <!-- #user -->                        ");
            file.WriteLine("      <div id=\"user\">");
            file.WriteLine("        <h2>Forest Gump <span>(admin)</span></h2>");
            file.WriteLine("        <a href=\"#\">7 messages</a> - <a href=\"#\">settings</a> - @Html.ActionLink(\"Çıkış\", \"LogOn\", \"Admin\")");
            file.WriteLine("      </div>");
            file.WriteLine("      <!-- /#user -->  ");
            file.WriteLine("    </div>");
            file.WriteLine("    <!-- /header -->");
            file.WriteLine("    <!-- #content -->");
            file.WriteLine("     ");
            file.WriteLine("     <!-- #sidebar -->");
            file.WriteLine("    <div id=\"sidebar\">");

            file.WriteLine("        <!-- mainmenu -->");
            file.WriteLine("      <ul id=\"floatMenu\" class=\"mainmenu\">");
            int i = 1;
            foreach (_Menu item in menu.menulist)
            {
                string cssclass = "";
                if (i == 1) cssclass = "class=\"first\"";
                if (i == menu.menulist.Count) cssclass = "class=\"last\"";
                if (item.submenulist.Count == 0)
                {
                    file.WriteLine("<li " + cssclass + " >@Html.ActionLink(\"" + item.name + "\", \"" + item.action + "\", \"" + item.controller + "\",new { }, new { @class = \"link\" })</li>");
                }
                else
                {
                    file.WriteLine("<li " + cssclass + "><a href=\"#\">" + item.name + "</a>");
                    file.WriteLine("<ul class=\"submenu\">");
                    foreach (SubMenu submenu in item.submenulist)
                    {
                        file.WriteLine("<li>@Html.ActionLink(\"" + submenu.name + "\",\"" + submenu.action + "\",\"" + submenu.controller + "\")</li>");
                    }
                    file.WriteLine("</ul>");
                    file.WriteLine("</li>");
                }
                i++;
            }
            file.WriteLine("</ul>");
            file.WriteLine("</div>");


            file.WriteLine("    <div id=\"content\">");
            file.WriteLine("    @RenderBody()");



            file.WriteLine("   ");
            file.WriteLine("      ");
            file.WriteLine("        ");
            file.WriteLine("      ");

            file.WriteLine("    </div>");
            file.WriteLine("    <!-- /#content -->");
            file.WriteLine("   ");
            file.WriteLine("    ");
            file.WriteLine("    <!-- /#sidebar -->");
            file.WriteLine("    <!-- #footer -->");
            file.WriteLine("    <div id=\"footer\">");
            file.WriteLine("      <p>By Erhan Eren | <a href=\"#main\">Yukarı</a></p>");
            file.WriteLine("    </div>");
            file.WriteLine("    <!-- /#footer -->");
            file.WriteLine("	");



            file.WriteLine("	");
            file.WriteLine("	");
            file.WriteLine("  </div>");
            file.WriteLine("  <!-- /#main --> ");

       file.WriteLine("           <script type=\"text/javascript\">");
    file.WriteLine("      jQuery(function ($) {");
    file.WriteLine("          $(\".phoneid\").mask(\"(999) 999-9999\");");
    file.WriteLine("      });");
    file.WriteLine("      jQuery(function ($) {");
    file.WriteLine("          $('.macid').mask('**:**:**:**:**:**');");
   file.WriteLine("       });");
      
  file.WriteLine("    </script>");


            file.WriteLine("  </body>");

           
            file.WriteLine("<!-- Added by HTTrack --><meta http-equiv=\"content-type\" content=\"text/html;charset=ascii\"><!-- /Added by HTTrack -->");

            file.WriteLine("</html>");

            file.Close();

        }
    }
}
