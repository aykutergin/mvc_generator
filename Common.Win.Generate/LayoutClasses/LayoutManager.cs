﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Avs.Win.Generate
{
    class LayoutManager
    {
        public  void WriteMenu(StreamWriter file, MenuManager menus)
        {

            file.WriteLine("<ul>");
            foreach (_Menu menu in menus.menulist)
            {
                if (menu.submenulist.Count > 0)
                {
                    file.WriteLine("<li>");
                    file.WriteLine("text =" + menu.name);
                    file.WriteLine("</li>");
                    file.WriteLine("<ul>");
                    foreach (SubMenu submenu in menu.submenulist)
                    {
                        file.WriteLine("<li>");
                        file.WriteLine("text =" + submenu.name + "action =" + submenu.action + "controller =" + submenu.controller);
                        file.WriteLine("</li>");
                    }
                }
                else
                {
                    file.WriteLine("<li>");
                    file.WriteLine("text =" + menu.name + "action =" + menu.action + "controller =" + menu.controller);
                    file.WriteLine("</li>");
                }
            }
            file.WriteLine("</ul>");
        }

        public  int InsertSqlRoles(MenuManager menus)
        {
            int UserTypeID = InsertDefaultUser();
            int roleID=0;
            foreach (Roles role in menus.rolelist)
            {
                string query = "Insert Into Role(Action,Controller) Values('" + role.action + "','" + role.controller + "')";
                Sql komut = new Sql(query);
                roleID=Convert.ToInt32(komut.InsertReturnIdentity());

                string gatequery = "Insert Into UserType_X_Role(UserTypeID,RoleID) Values('" + UserTypeID + "','" + roleID + "')";
                Sql gatekomut = new Sql(gatequery);
                gatekomut.Insert();
            }
            return UserTypeID;
            
        }

        public void InsertSqlMenusAndRoles()
        {
            MenuManager menus = Constants.menumanager;
            int UserTypeID = InsertSqlRoles(menus);
            int MenuID = 0;
            foreach (_Menu menu in menus.menulist)
            {
                string query = "Insert Into Menu(Name,Action,Controller) Values('" + menu.name + "','" + menu.action + "','" + menu.controller + "')";
                Sql komut = new Sql(query);
                MenuID = Convert.ToInt32(komut.InsertReturnIdentity());

                string gatequery = "Insert Into UserType_X_Menu(UserTypeID,MenuID) Values('" + UserTypeID + "','" + MenuID + "')";
                Sql gatekomut = new Sql(gatequery);
                gatekomut.Insert();
            }
        }

        public  int InsertDefaultUserType()
        {
            string query = "Insert Into UserType(Name) Values('Admin')";
            Sql komut = new Sql(query);
            return Convert.ToInt32(komut.InsertReturnIdentity());
        }

        public  int InsertDefaultUser()
        {
            int UserTypeID = InsertDefaultUserType();
            string query = "Insert Into Users(UserTypeID,Name,Surname,UserName,Password) Values('" + UserTypeID + "','Admin','Admin','Admin','123456')";
            Sql komut = new Sql(query);
            komut.Insert();
            return UserTypeID;
        }
    }
}
