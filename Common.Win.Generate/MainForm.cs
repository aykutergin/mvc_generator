﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using Avs.Win.Generate.Classes;


namespace Avs.Win.Generate
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        public void StartCreation()
        {
            LayoutManager layoutmanager = new LayoutManager();
            TableManager manager = new TableManager();
            Table table;
            progbarclasses.Minimum = 1;
            progbarclasses.Maximum = clbtables.Items.Count + clbview.Items.Count + 1;
            progbarclasses.Visible = true;
            string tableName = String.Empty;
            Constants.rootPath = folderBrowserDialog1.SelectedPath;
            SetNamespace();
            SetFolderPath();
            int i = 0;
            int j = 0;
            SetCheckControl();

            for (i = 0; i < clbtables.Items.Count; i++)
            {
                progbarclasses.Value = i + 1;
                if (clbtables.GetItemChecked(i) == true)
                {
                    tableName = clbtables.Items[i].ToString().Trim();
                    table = manager.CreateTable(tableName);

                    CheckTablePlural(table);


                    //tools.IsExistCreateDirectory(Constants.CrudsFolder);
                    //Crud crud = new Crud(table);
                    //crud.WriteCrudFile();

                    tools.IsExistCreateDirectory(Constants.ModelsFolder);
                    Models model = new Models(table);
                    model.WriteModelFile();

                    tools.IsExistCreateDirectory(Constants.OpModelFolder);
                    OpModel opmodel = new OpModel(table);
                    opmodel.WriteOpModelFile();

                    tools.IsExistCreateDirectory(Constants.OpFilterFolder);
                    OpFilter opfilter = new OpFilter(table);
                    opfilter.WriteOpFilterFile();

                    tools.IsExistCreateDirectory(Constants.OpSelectFolder);
                    OpSelect OpSelect = new OpSelect(table);
                    OpSelect.WriteOpSelectFile();

                    tools.IsExistCreateDirectory(Constants.ApiServiceFolder);
                    ApiService ApiService = new ApiService(table);
                    ApiService.WriteApiServiceFile();

                    tools.IsExistCreateDirectory(Constants.ApiControllerFolder);
                    ApiController ApiController = new ApiController(table);
                    ApiController.WriteApiControllerFile();

                    tools.IsExistCreateDirectory(Constants.ApiClientFolder);
                    ApiClient apiClient = new ApiClient(table);
                    apiClient.WriteApiClientFile();

                    //Action Tablousunu Güncelle

                }

                Application.DoEvents();
            }
            for (j = 0; j < clbview.Items.Count; j++)
            {
                progbarclasses.Value = i + j + 1;
                if (clbview.GetItemChecked(j) == true)
                {
                    tableName = clbview.Items[j].ToString().Trim();
                    table = manager.CreateTable(tableName);

                    CheckTablePlural(table);

                    //tools.IsExistCreateDirectory(Constants.CrudsFolder);
                    //Crud crud = new Crud(table);
                    //crud.WriteCrudFile();

                    tools.IsExistCreateDirectory(Constants.ModelsFolder);
                    Models model = new Models(table);
                    model.WriteModelFile();


                    tools.IsExistCreateDirectory(Constants.OpModelFolder);
                    OpModel opmodel = new OpModel(table);
                    opmodel.WriteOpModelFile();

                    tools.IsExistCreateDirectory(Constants.OpFilterFolder);
                    OpFilter opfilter = new OpFilter(table);
                    opfilter.WriteOpFilterFile();

                    tools.IsExistCreateDirectory(Constants.OpSelectFolder);
                    OpSelect OpSelect = new OpSelect(table);
                    OpSelect.WriteOpSelectFile();

                    tools.IsExistCreateDirectory(Constants.ApiServiceFolder);
                    ApiService ApiService = new ApiService(table);
                    ApiService.WriteApiServiceFile();

                    tools.IsExistCreateDirectory(Constants.ApiControllerFolder);
                    ApiController ApiController = new ApiController(table);
                    ApiController.WriteApiControllerFile();

                    tools.IsExistCreateDirectory(Constants.ApiClientFolder);
                    ApiClient apiClient = new ApiClient(table);
                    apiClient.WriteApiClientFile();
                }
                Application.DoEvents();
            }

            //if (Constants.IsHomePage == true || Constants.IsLayout==true)
            //{
            //    Constants.menumanager.menulist.Add(Constants.mainMenu);
            //    Constants.menumanager.menulist.AddRange(Constants.refMenuList);
            //}

            //if (Constants.IsHomePage == true)
            //{
            //    tools.IsExistCreateDirectory(Constants.ControllerFolder);
            //    tools.IsExistCreateDirectory(Constants.ViewFolder);
            //    HomeController home = new HomeController();
            //    home.Write();
            //}
            //if (Constants.IsHomePage == true)
            //{
            //    tools.IsExistCreateDirectory(Constants.ViewFolder + "\\Shared");
            //    GreatAdmin_Layout layout = new GreatAdmin_Layout();
            //    layout.Write();
            //    layoutmanager.InsertSqlMenusAndRoles();
            //}



            progbarclasses.Value = progbarclasses.Value + 1;
            Application.DoEvents();

            progbarclasses.Visible = false;
            lblwarning.Visible = true;
        }

        public void StartMvcCrudCreation()
        {
            LayoutManager layoutmanager = new LayoutManager();
            TableManager manager = new TableManager();
            FileUtility file = new FileUtility();
            Table table;
            progbarclasses.Minimum = 1;
            progbarclasses.Maximum = clbtables.Items.Count + clbview.Items.Count + 1;
            progbarclasses.Visible = true;
            string tableName = String.Empty;
            Constants.rootPath = folderBrowserDialog1.SelectedPath;
            SetNamespace();
            SetFolderPath();
            int i = 0;
            int j = 0;
            SetCheckControl();
            CreateMvcDirectories();
            for (i = 0; i < clbtables.Items.Count; i++)
            {
                progbarclasses.Value = i + 1;
                if (clbtables.GetItemChecked(i) == true)
                {
                    tableName = clbtables.Items[i].ToString().Trim();
                    table = manager.CreateTable(tableName);

                    CheckTablePlural(table);

                    ReportISource reportISource = new ReportISource(table);
                    reportISource.WriteReportISourceFile();

                    ReportSource reportSource = new ReportSource(table);
                    reportSource.WriteReportSourceFile();

                    ReportService reportService = new ReportService(table);
                    reportService.WriteReportServiceFile();

                    ReportController reportController = new ReportController(table);
                    reportController.WriteReportControllerFile();

                    ReportViewModels reportViewModels = new ReportViewModels(table);
                    reportViewModels.WriteReportViewModelsFile();

                    ReportSourceFactory reportSourceFactory = new ReportSourceFactory(table);
                    reportSourceFactory.WriteReportSourceFactoryFile();

                    ReportSourceTableTransfer reportSourceTableTransfer = new ReportSourceTableTransfer(table);
                    reportSourceTableTransfer.WriteReportSourceTableTransferFile();

                    ReportModel reportModel = new ReportModel(table);
                    reportModel.WriteModelFile();

                    ValidationModel validationModel = new ValidationModel(table);
                    validationModel.WriteModelFile();

                    ////Action Tablousunu Güncelle
                    ////Views
                    tools.IsExistCreateDirectory(Constants.Views + "//" + table.name);
                    Index index = new Index(table);
                    index.WriteIndexFile();

                    tools.IsExistCreateDirectory(Constants.Views + "//" + table.name);
                    Create create = new Create(table, file);
                    create.WriteCreateFile();

                    tools.IsExistCreateDirectory(Constants.Views + "//" + table.name);
                    Update update = new Update(table, file);
                    update.WriteUpdateFile();

                    //tools.IsExistCreateDirectory(Constants.Views + "//" + table.name);
                    //Detail detail = new Detail(table);
                    //detail.WriteDetailFile();

                }

                Application.DoEvents();
            }
            for (j = 0; j < clbview.Items.Count; j++)
            {
                progbarclasses.Value = i + j + 1;
                if (clbview.GetItemChecked(j) == true)
                {
                    tableName = clbview.Items[j].ToString().Trim();
                    table = manager.CreateTable(tableName);

                    CheckTablePlural(table);

                    ReportFilterModel filterModel = new ReportFilterModel(table);
                    filterModel.WriteFilterModelFile();

                    ReportSourceVwTransfer reportSourceVwTransfer = new ReportSourceVwTransfer(table);
                    reportSourceVwTransfer.WriteReportSourceVwTransferFile();

                    ReportVwModel reportVwModel = new ReportVwModel(table);
                    reportVwModel.WriteVwModelFile();

                    //Views-klasör bakılmadı tables dönerken oluşturluduğu için
                    tools.IsExistCreateDirectory(Constants.Views + "//" + table.name);
                    IndexTable indextable = new IndexTable(table);
                    indextable.WriteIndexTableFile();


                }
                Application.DoEvents();
            }

            progbarclasses.Value = progbarclasses.Value + 1;
            Application.DoEvents();

            progbarclasses.Visible = false;
            lblwarning.Visible = true;
        }

        public void StartMvcGetCreation()
        {
            LayoutManager layoutmanager = new LayoutManager();
            TableManager manager = new TableManager();
            FileUtility file = new FileUtility();
            Table table;
            progbarclasses.Minimum = 1;
            progbarclasses.Maximum = clbtables.Items.Count + clbview.Items.Count + 1;
            progbarclasses.Visible = true;
            string tableName = String.Empty;
            Constants.rootPath = folderBrowserDialog1.SelectedPath;
            SetNamespace();
            SetFolderPath();
            int i = 0;
            int j = 0;
            SetCheckControl();
            CreateGetMvcDirectories();
            for (j = 0; j < clbview.Items.Count; j++)
            {
                progbarclasses.Value = i + j + 1;
                if (clbview.GetItemChecked(j) == true)
                {
                    tableName = clbview.Items[j].ToString().Trim();
                    table = manager.CreateTable(tableName);
                    if (table.primery_column == null)
                    {
                        table.primery_column = table.columns.FirstOrDefault();
                    }
                    CheckTablePlural(table);


                    ReportISource reportISource = new ReportISource(table);
                    reportISource.WriteGetReportISourceFile();

                    ReportSource reportSource = new ReportSource(table);
                    reportSource.WriteGetReportSourceFile();


                    ReportSourceVwTransfer reportSourceVwTransfer = new ReportSourceVwTransfer(table);
                    reportSourceVwTransfer.WriteGetReportSourceVwTransferFile();

                    ReportService reportService = new ReportService(table);
                    reportService.WriteGetReportServiceFile();

                    ReportController reportController = new ReportController(table);
                    reportController.WriteGetReportControllerFile();

                    ReportViewModels reportViewModels = new ReportViewModels(table);
                    reportViewModels.WriteGetReportViewModelsFile();

                    ReportSourceFactory reportSourceFactory = new ReportSourceFactory(table);
                    reportSourceFactory.WriteReportSourceFactoryFile();

                    ReportModel reportModel = new ReportModel(table);
                    reportModel.WriteGetModelFile();

                    ReportFilterModel filterModel = new ReportFilterModel(table);
                    filterModel.WriteFilterModelFile();

                    tools.IsExistCreateDirectory(Constants.Views + "//" + table.name);
                    Index index = new Index(table);
                    index.WriteGetIndexFile();

                    tools.IsExistCreateDirectory(Constants.Views + "//" + table.name);
                    IndexTable indextable = new IndexTable(table);
                    indextable.WriteGetIndexTableFile();

                    tools.IsExistCreateDirectory(Constants.Views + "//" + table.name);
                    ScriptPartial scriptPartial = new ScriptPartial(table);
                    scriptPartial.WriteScriptPartialFile();
                }

                Application.DoEvents();
            }


            progbarclasses.Value = progbarclasses.Value + 1;
            Application.DoEvents();

            progbarclasses.Visible = false;
            lblwarning.Visible = true;
        }

        public void StartDropdownCreation()
        {
            progbarclasses.Minimum = 1;
            progbarclasses.Maximum = clbtables.Items.Count + clbview.Items.Count + 1;
            progbarclasses.Visible = true;
            Constants.rootPath = folderBrowserDialog1.SelectedPath;
            SetNamespace();
            Constants.DropdownControllerFolder = Constants.rootPath + "//Dcl.Avs3.Web.Report//Controllers";
            Constants.DropdownServiceFolder = Constants.rootPath + "//Dcl.Avs3.Web.Report//Service";
            SetCheckControl();
            tools.IsExistCreateDirectory(Constants.DropdownControllerFolder);
            tools.IsExistCreateDirectory(Constants.DropdownServiceFolder);

            DropdownController dropdownController = new DropdownController();
            dropdownController.WriteGetDropdownControllerFile(clbview);


            progbarclasses.Visible = false;
            lblwarning.Visible = true;
        }
        private void CreateMvcDirectories()
        {
            tools.IsExistCreateDirectory(Constants.ReportISourceFolder);
            tools.IsExistCreateDirectory(Constants.ReportSourceFolder);
            tools.IsExistCreateDirectory(Constants.ReportServiceFolder);
            tools.IsExistCreateDirectory(Constants.ReportControllerFolder);
            tools.IsExistCreateDirectory(Constants.ReportViewModelsFolder);
            tools.IsExistCreateDirectory(Constants.ReportSourceFactoryFolder);
            tools.IsExistCreateDirectory(Constants.ReportSourceTableTransferFolder);
            tools.IsExistCreateDirectory(Constants.ReportModelFolder);
            tools.IsExistCreateDirectory(Constants.ReportValidationModelFolder);
            tools.IsExistCreateDirectory(Constants.ReportSourceVwTransferFolder);
            tools.IsExistCreateDirectory(Constants.ReportVwModelFolder);
            tools.IsExistCreateDirectory(Constants.ReportFilterModelFolder);
        }
        private void CreateGetMvcDirectories()
        {
            tools.IsExistCreateDirectory(Constants.ReportGetISourceFolder);
            tools.IsExistCreateDirectory(Constants.ReportGetSourceFolder);
            tools.IsExistCreateDirectory(Constants.ReportSourceVwTransferFolder);
            tools.IsExistCreateDirectory(Constants.ReportSourceFactoryFolder);
            tools.IsExistCreateDirectory(Constants.ReportServiceFolder);
            tools.IsExistCreateDirectory(Constants.ReportControllerFolder);
            tools.IsExistCreateDirectory(Constants.ReportViewModelsFolder);
            tools.IsExistCreateDirectory(Constants.ReportVwModelFolder);
            tools.IsExistCreateDirectory(Constants.ReportFilterModelFolder);
        }
        private void CheckTablePlural(Table table)
        {
            table.name = tools.GetSingular(table.name);
            table.namePlural = tools.GetPlural(table.name);
            foreach (var item in table.columns)
            {
                if (item.isForeign)
                {
                    item.foreign.table.name = tools.GetSingular(item.foreign.table.name);
                    item.foreign.table.namePlural = tools.GetPlural(item.foreign.table.name);
                }
            }
            foreach (var item in table.foreigns)
            {
                item.table.name = tools.GetSingular(item.table.name);
                item.table.namePlural = tools.GetPlural(item.table.name);
            }
            foreach (var item in table.ref_foreign)
            {
                item.table.name = tools.GetSingular(item.table.name);
                item.table.namePlural = tools.GetPlural(item.table.name);
            }
            foreach (var item in table.nnrel_foreign)
            {
                item.targetTable.name = tools.GetSingular(item.targetTable.name);
                item.targetTable.namePlural = tools.GetPlural(item.targetTable.name);
                item.gapTable.name = tools.GetSingular(item.gapTable.name);
                item.gapTable.namePlural = tools.GetPlural(item.gapTable.name);

            }
        }

        public void StartSqlViewCreation()
        {
            LayoutManager layoutmanager = new LayoutManager();
            TableManager manager = new TableManager();
            Table table;
            progbarclasses.Minimum = 1;
            progbarclasses.Maximum = clbtables.Items.Count;
            progbarclasses.Visible = true;
            string tableName = String.Empty;
            Constants.rootPath = folderBrowserDialog1.SelectedPath;
            SetFolderPath();
            int i = 0;
            try { File.Delete(Constants.SqlViewFolder + "\\VW_All.sql"); } catch { }

            for (i = 0; i < clbtables.Items.Count; i++)
            {
                progbarclasses.Value = i + 1;
                if (clbtables.GetItemChecked(i) == true)
                {
                    tableName = clbtables.Items[i].ToString().Trim();
                    table = manager.CreateTable(tableName);

                    ViewCreator vw = new ViewCreator(table, true);
                    tools.IsExistCreateDirectory(Constants.SqlViewFolder);
                    vw.WriteSqlFile();
                }

                Application.DoEvents();
            }



            Application.DoEvents();

            progbarclasses.Visible = false;
            lblwarning.Visible = true;
        }

        public static string URL = "";

        private void button2_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.ShowDialog();
        }

        private void btnconnect_Click(object sender, EventArgs e)
        {
            string baglanti = String.Empty;
            string sorgutable = "SELECT name FROM dbo.sysobjects WHERE xtype = 'U' and SUBSTRING(name,0,4)!='sys'";
            string sorguview = "SELECT name FROM dbo.sysobjects WHERE xtype = 'V' and SUBSTRING(name,0,4)!='sys'";
            Sql sql1;
            Sql sql2;
            if (cbdefaultcon.Checked == true)
            {
                sql1 = new Sql(sorgutable);
                sql2 = new Sql(sorguview);
            }
            else
            {
                baglanti = "Data Source=" + txtdatasource.Text.Trim() + ";Initial Catalog=" + txtdbname.Text.Trim() + ";User ID=" + txtusername.Text.Trim() + ";Password=" + txtpassword.Text.Trim();
                sql1 = new Sql(sorgutable, baglanti);
                sql2 = new Sql(sorguview, baglanti);
            }
            DataTable dttable = sql1.select();
            DataTable dtview = sql2.select();
            for (int i = 0; i < dttable.Rows.Count; i++)
            {
                clbtables.Items.Add(dttable.Rows[i][0].ToString());
            }
            for (int j = 0; j < dtview.Rows.Count; j++)
            {
                clbview.Items.Add(dtview.Rows[j][0].ToString());
            }

        }

        private void cbdefaultcon_CheckedChanged(object sender, EventArgs e)
        {
            if (cbdefaultcon.Checked == true)
            {
                txtdatasource.Enabled = false;
                txtdbname.Enabled = false;
                txtusername.Enabled = false;
                txtpassword.Enabled = false;
            }
            else
            {
                txtdatasource.Enabled = true;
                txtdbname.Enabled = true;
                txtusername.Enabled = true;
                txtpassword.Enabled = true;
            }
        }

        private void btnCreate_Click_1(object sender, EventArgs e)
        {
            StartCreation();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

            for (int i = 0; i < clbtables.Items.Count; i++)
            {
                clbtables.SetItemChecked(i, cbtables.Checked);
            }

        }

        private void cbview_CheckedChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < clbview.Items.Count; i++)
            {
                clbview.SetItemChecked(i, cbview.Checked);
            }
        }



        public void SetCheckControl()
        {

            Constants.IsDto = cbmodels.Checked;
            Constants.IsOpClass = cbopclasses.Checked;
            Constants.IsController = cbcontroller.Checked;
            Constants.IsNull = cbpersistnull.Checked;
        }
        private void SetNamespace()
        {



            if (txtcomp.Text != string.Empty && txtsolution.Text != String.Empty && txtapiname.Text != String.Empty)
            {
                Constants.nsitem_company = txtcomp.Text.Trim();
                Constants.nsitem_solution = txtsolution.Text.Trim();
                Constants.nsitem_proje = txtapiname.Text.Trim();
            }

            Constants.ent_ns = String.Format("{0}.{1}.{2}.{3}.{4}", Constants.nsitem_company, Constants.nsitem_solution, "Lib", Constants.nsitem_proje, "Entity"); //
            Constants.dto_ns = String.Format("{0}.{1}.{2}.{3}.{4}", Constants.nsitem_company, Constants.nsitem_solution, "Lib", Constants.nsitem_proje, "Dto");
            Constants.obj_ns = String.Format("{0}.{1}.{2}.{3}.{4}", Constants.nsitem_company, Constants.nsitem_solution, "Lib", Constants.nsitem_proje, "Dto.Obj");
            Constants.api_ns = String.Format("{0}.{1}.{2}.{3}.{4}", Constants.nsitem_company, Constants.nsitem_solution, "Web", Constants.nsitem_proje, "Api");
            Constants.client_ns = String.Format("{0}.{1}.{2}.{3}.{4}", Constants.nsitem_company, Constants.nsitem_solution, "Lib", Constants.nsitem_proje, "Client");
            Constants.rest_ns = String.Format("{0}.{1}.{2}", Constants.nsitem_company, "Lib", "Rest.Client");
            Constants.utility_ns = String.Format("{0}.{1}.{2}", Constants.nsitem_company, "Lib", "Utility");







        }

        public void SetFolderPath()
        {
            Constants.BusinessFolder = Constants.rootPath + "//Business";
            Constants.EntityFolder = Constants.rootPath + "//Entities";
            Constants.ModelsFolder = Constants.rootPath + "//" + Constants.obj_ns + "//Dto";
            Constants.ProjectOperationClassesFolder = Constants.rootPath + "//OpClasses";
            Constants.ControllerFolder = Constants.rootPath + "//Controllers";
            Constants.ViewFolder = Constants.rootPath + "//Views";
            Constants.SqlViewFolder = Constants.rootPath + "//Sql";
            Constants.CrudsFolder = Constants.rootPath + "//" + Constants.ent_ns + "//Crud//Dal";
            Constants.OpModelFolder = Constants.rootPath + "//" + Constants.dto_ns + "//DtoOpClass";
            Constants.OpFilterFolder = Constants.rootPath + "//" + Constants.dto_ns + "//Filter";
            Constants.OpSelectFolder = Constants.rootPath + "//" + Constants.dto_ns + "//Select";
            Constants.ApiServiceFolder = Constants.rootPath + "//" + Constants.api_ns + "//Services//Api";
            Constants.ApiControllerFolder = Constants.rootPath + "//" + Constants.api_ns + "//Controllers//Api";
            Constants.ApiClientFolder = Constants.rootPath + "//" + Constants.client_ns + "//Client";
            Constants.ReportISourceFolder = Constants.rootPath + "//Dcl.Avs3.Lib.Rest.ISource//Crud";
            Constants.ReportGetISourceFolder = Constants.rootPath + "//Dcl.Avs3.Lib.Rest.ISource//Get";
            Constants.ReportSourceFolder = Constants.rootPath + "//Dcl.Avs3.Lib.Rest.Source//Source//Crud";
            Constants.ReportGetSourceFolder = Constants.rootPath + "//Dcl.Avs3.Lib.Rest.Source//Source//Get";
            Constants.ReportSourceTableTransferFolder = Constants.rootPath + "//Dcl.Avs3.Lib.Rest.Source//Transfers//CrudTransfer";
            Constants.ReportSourceVwTransferFolder = Constants.rootPath + "//Dcl.Avs3.Lib.Rest.Source//Transfers//GetTransfer";
            Constants.ReportControllerFolder = Constants.rootPath + "//Dcl.Avs3.Web.Report//Controllers";
            Constants.ReportServiceFolder = Constants.rootPath + "//Dcl.Avs3.Web.Report//Service";
            Constants.ReportSourceFactoryFolder = Constants.rootPath + "//Dcl.Avs3.Web.Report//SourceFactory//Partial";
            Constants.ReportModelFolder = Constants.rootPath + "//Dcl.Avs3.Web.Report.Model//Crud";
            Constants.ReportValidationModelFolder = Constants.rootPath + "//Dcl.Avs3.Web.Report.Model//ValidationModel";
            Constants.ReportVwModelFolder = Constants.rootPath + "//Dcl.Avs3.Web.Report.Model//Models//Get";
            Constants.ReportFilterModelFolder = Constants.rootPath + "//Dcl.Avs3.Web.Report.Model//Filter";
            Constants.ReportViewModelsFolder = Constants.rootPath + "//Dcl.Avs3.Web.Report//Models//ViewModels";
            Constants.MonoDtoFolder = Constants.rootPath + "//Dcl.Avs3.Get//Mono//Dcl.Avs3.Lib.Mono.Get.Dto.Obj//Dto";
            Constants.MonoClientFolder = Constants.rootPath + "//Dcl.Avs3.Get//Mono/Dcl.Avs3.Lib.Mono.Get.Client//Client";
            Constants.DocFolder = Constants.rootPath + "//Doc";

            Constants.Views = Constants.rootPath + "//Dcl.Avs3.Web.Report//Views";

        }

        private void btnmenukayit_Click(object sender, EventArgs e)
        {

            TableManager manager = new TableManager();
            Table table;
            progbarclasses.Minimum = 1;
            progbarclasses.Maximum = clbtables.Items.Count + clbview.Items.Count + 1;
            progbarclasses.Visible = true;
            string tableName = String.Empty;
            Constants.rootPath = folderBrowserDialog1.SelectedPath;
            SetFolderPath();
            int i = 0;
            int j = 0;

            for (i = 0; i < clbtables.Items.Count; i++)
            {
                progbarclasses.Value = i + 1;
                if (clbtables.GetItemChecked(i) == true)
                {
                    tableName = clbtables.Items[i].ToString().Trim();
                    table = manager.CreateTable(tableName);

                    tools.CreateAction(table, 7);

                }

                Application.DoEvents();
            }






            progbarclasses.Value = progbarclasses.Value + 1;
            Application.DoEvents();

            progbarclasses.Visible = false;
            lblwarning.Visible = true;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string renderbody = File.ReadAllText("DocTemplete//renderbody.html");
            string menurender = File.ReadAllText("DocTemplete//menurender.html");
            string master = File.ReadAllText("DocTemplete//master.html");

            TableManager manager = new TableManager();
            Table table;
            progbarclasses.Minimum = 1;
            progbarclasses.Maximum = clbtables.Items.Count + clbview.Items.Count + 1;
            progbarclasses.Visible = true;
            string tableName = String.Empty;
            Constants.rootPath = folderBrowserDialog1.SelectedPath;
            SetFolderPath();
            int i = 0;
            int j = 0;
            if (!Directory.Exists(Constants.DocFolder)) Directory.CreateDirectory(Constants.DocFolder);
            FileInfo f = new FileInfo(Constants.DocFolder + "//doc.html");
            StreamWriter file = new StreamWriter(Constants.DocFolder + "//doc.html", true, Encoding.UTF8);
            string renderbody_text_item = "";
            string menurender_text_item = "";
            int index = 1;
            for (i = 0; i < clbtables.Items.Count; i++)
            {
                progbarclasses.Value = i + 1;
                if (clbtables.GetItemChecked(i) == true)
                {
                    tableName = clbtables.Items[i].ToString().Trim();
                    table = manager.CreateTable(tableName);

                    renderbody_text_item += ReplaceRenderBody(renderbody, table, index);
                    menurender_text_item += ReplaceMenuRender(menurender, table, index);
                    index++;
                }

                Application.DoEvents();
            }
            for (i = 0; i < clbview.Items.Count; i++)
            {
                progbarclasses.Value = i + 1;
                if (clbview.GetItemChecked(i) == true)
                {
                    tableName = clbview.Items[i].ToString().Trim();
                    table = manager.CreateTable(tableName);

                    renderbody_text_item += ReplaceRenderBody(renderbody, table, index);
                    menurender_text_item += ReplaceMenuRender(menurender, table, index);
                    index++;

                }

                Application.DoEvents();
            }

            file.Write(master.Replace("#RenderBody", renderbody_text_item).Replace("#MenuRender", menurender_text_item));
            file.Close();

            progbarclasses.Value = progbarclasses.Value + 1;
            Application.DoEvents();

            progbarclasses.Visible = false;
            lblwarning.Visible = true;

        }


        public string ReplaceRenderBody(string str, Table table, int index)
        {
            string temp_str = str;
            temp_str = temp_str.Replace("#tname", table.name);
            temp_str = temp_str.Replace("#pkey", table.primery_column.name);
            temp_str = temp_str.Replace("#index", index.ToString());

            string loop1 = tools.GetLoopItem(temp_str, 1);
            string loop2 = tools.GetLoopItem(temp_str, 2);
            string loop3 = tools.GetLoopItem(temp_str, 3);
            string loop4 = tools.GetLoopItem(temp_str, 4);
            string loop5 = tools.GetLoopItem(temp_str, 5);
            string loop6 = tools.GetLoopItem(temp_str, 6);
            string loop7 = tools.GetLoopItem(temp_str, 7);
            string loop8 = tools.GetLoopItem(temp_str, 8);
            string loop9 = tools.GetLoopItem(temp_str, 9);
            string r_loop1 = "";
            string r_loop2 = "";
            string r_loop3 = "";
            string r_loop4 = "";
            string r_loop5 = "";
            string r_loop6 = "";
            string r_loop7 = "";
            string r_loop8 = "";
            string r_loop9 = "";


            foreach (var item in table.columns)
            {
                r_loop1 += loop1.Replace("#colname", item.name).Replace("#coltype", item.dataType);
            }
            temp_str = temp_str.Replace(loop1, r_loop1);


            foreach (Column item in table.columns)
            {
                if (item.isForeign)
                {
                    r_loop2 += loop2.Replace("#forName", item.foreign.table.name);
                    r_loop4 += loop4.Replace("#forName", item.foreign.table.name);
                    r_loop5 += loop5.Replace("#forName", item.foreign.table.name);
                    r_loop7 += loop7.Replace("#forName", item.foreign.table.name);
                    r_loop9 += loop9.Replace("#forName", item.foreign.table.name);
                }
            }

            foreach (ForeignKey item in table.ref_foreign)
            {
                r_loop3 += loop3.Replace("#refName", item.table.name);
                r_loop6 += loop6.Replace("#refName", item.table.name);
                r_loop8 += loop8.Replace("#refName", item.table.name);
            }

            temp_str = temp_str.Replace(loop1, r_loop1);
            temp_str = temp_str.Replace(loop2, r_loop2);
            temp_str = temp_str.Replace(loop3, r_loop3);
            temp_str = temp_str.Replace(loop4, r_loop4);
            temp_str = temp_str.Replace(loop5, r_loop5);
            temp_str = temp_str.Replace(loop6, r_loop6);
            temp_str = temp_str.Replace(loop7, r_loop7);
            temp_str = temp_str.Replace(loop8, r_loop8);
            temp_str = temp_str.Replace(loop9, r_loop9);

            return temp_str;
        }

        public string ReplaceMenuRender(string str, Table table, int index)
        {
            string temp_str = str;
            temp_str = temp_str.Replace("#tname", table.name);
            temp_str = temp_str.Replace("#index", index.ToString());

            return temp_str;
        }
        private void btnclear_Click(object sender, EventArgs e)
        {

            txtcomp.Text = String.Empty;
            txtapiname.Text = String.Empty;
            txtsolution.Text = String.Empty;

        }

        private void btnsqlview_Click(object sender, EventArgs e)
        {
            StartSqlViewCreation();
        }

        private void btnmvcCreate_Click(object sender, EventArgs e)
        {
            StartMvcCrudCreation();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            StartMvcGetCreation();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            CreateDbRel();
        }
        public void CreateDbRel()
        {
            LayoutManager layoutmanager = new LayoutManager();
            TableManager manager = new TableManager();
            progbarclasses.Minimum = 1;
            progbarclasses.Maximum = clbtables.Items.Count;
            progbarclasses.Visible = true;
            string tableName = String.Empty;
            Constants.rootPath = folderBrowserDialog1.SelectedPath;
            SetFolderPath();
            int i = 0;
            try { File.Delete(Constants.SqlViewFolder + "\\dbrel.sql"); } catch { }
            string str = File.ReadAllText("WebReport//foreignRel.txt", Encoding.GetEncoding("iso-8859-9"));

            List<Table> tableList = new List<Table>();

            for (i = 0; i < clbtables.Items.Count; i++)
            {
                progbarclasses.Value = i + 1;
                tableName = clbtables.Items[i].ToString().Trim();
                tableList.Add(manager.CreateTable(tableName));
            }

            string content = "";
            StreamWriter file = new StreamWriter(Constants.SqlViewFolder + "\\dbrel.sql", false, Encoding.UTF8);//(filepath);
            for (i = 0; i < tableList.Count; i++)
            {
                progbarclasses.Value = i + 1;
                if (clbtables.GetItemChecked(i) == true)
                {
                    foreach (var col in tableList[i].columns)
                    {
                        if (col.name.Substring(col.name.Length - 2) == "Id" && tableList[i].primery_column.name != col.name && !col.isForeign)//eğer son iki karakter Id ise
                        {
                            Table refTable = GetRefTableName(tableList, col.name);
                            if (refTable != null)
                            {
                                content += str.Replace("#tName", tableList[i].name).Replace("#refTName", refTable.name).Replace("#tColName", col.name).Replace("#refColName", refTable.primery_column.name);
                            }
                        }
                    }
                }
            }

            file.Write(content);
            file.Close();
            Application.DoEvents();

            progbarclasses.Visible = false;
            lblwarning.Visible = true;
        }
        private Table GetRefTableName(List<Table> tableList, string key)
        {
            foreach (var table in tableList)
            {
                if (table.primery_column.name == key)
                {
                    return table;
                }
            }
            return null;
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            StartDropdownCreation();
        }
    }
}
