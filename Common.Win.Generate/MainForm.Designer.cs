﻿namespace Avs.Win.Generate
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cbtables = new System.Windows.Forms.CheckBox();
            this.clbtables = new System.Windows.Forms.CheckedListBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cbview = new System.Windows.Forms.CheckBox();
            this.clbview = new System.Windows.Forms.CheckedListBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnclear = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtsolution = new System.Windows.Forms.TextBox();
            this.txtapiname = new System.Windows.Forms.TextBox();
            this.txtcomp = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.cbmodels = new System.Windows.Forms.CheckBox();
            this.cbcontroller = new System.Windows.Forms.CheckBox();
            this.cbopclasses = new System.Windows.Forms.CheckBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.lblwarning = new System.Windows.Forms.Label();
            this.progbarclasses = new System.Windows.Forms.ProgressBar();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnmvcCreate = new System.Windows.Forms.Button();
            this.btnsqlview = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.cbpersistnull = new System.Windows.Forms.CheckBox();
            this.cbdefaultcon = new System.Windows.Forms.CheckBox();
            this.btnconnect = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtpassword = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtusername = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtdbname = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtdatasource = new System.Windows.Forms.TextBox();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.panel1.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.PaleTurquoise;
            this.panel1.Controls.Add(this.groupBox7);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1276, 694);
            this.panel1.TabIndex = 11;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.groupBox2);
            this.groupBox7.Controls.Add(this.groupBox3);
            this.groupBox7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox7.Location = new System.Drawing.Point(547, 0);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(729, 694);
            this.groupBox7.TabIndex = 17;
            this.groupBox7.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox2.Controls.Add(this.cbtables);
            this.groupBox2.Controls.Add(this.clbtables);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.groupBox2.ForeColor = System.Drawing.SystemColors.Desktop;
            this.groupBox2.Location = new System.Drawing.Point(12, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(378, 682);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "TABLES";
            // 
            // cbtables
            // 
            this.cbtables.AutoSize = true;
            this.cbtables.Dock = System.Windows.Forms.DockStyle.Top;
            this.cbtables.Location = new System.Drawing.Point(3, 27);
            this.cbtables.Name = "cbtables";
            this.cbtables.Size = new System.Drawing.Size(372, 29);
            this.cbtables.TabIndex = 1;
            this.cbtables.Text = "Tümünü Seç";
            this.cbtables.UseVisualStyleBackColor = true;
            this.cbtables.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // clbtables
            // 
            this.clbtables.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.clbtables.CheckOnClick = true;
            this.clbtables.FormattingEnabled = true;
            this.clbtables.Location = new System.Drawing.Point(3, 60);
            this.clbtables.Name = "clbtables";
            this.clbtables.Size = new System.Drawing.Size(361, 342);
            this.clbtables.Sorted = true;
            this.clbtables.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox3.Controls.Add(this.cbview);
            this.groupBox3.Controls.Add(this.clbview);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.groupBox3.ForeColor = System.Drawing.SystemColors.Desktop;
            this.groupBox3.Location = new System.Drawing.Point(402, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(427, 682);
            this.groupBox3.TabIndex = 13;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "VIEWS";
            // 
            // cbview
            // 
            this.cbview.AutoSize = true;
            this.cbview.Dock = System.Windows.Forms.DockStyle.Top;
            this.cbview.Location = new System.Drawing.Point(3, 27);
            this.cbview.Name = "cbview";
            this.cbview.Size = new System.Drawing.Size(421, 29);
            this.cbview.TabIndex = 2;
            this.cbview.Text = "Tümünü Seç";
            this.cbview.UseVisualStyleBackColor = true;
            this.cbview.CheckedChanged += new System.EventHandler(this.cbview_CheckedChanged);
            // 
            // clbview
            // 
            this.clbview.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.clbview.CheckOnClick = true;
            this.clbview.FormattingEnabled = true;
            this.clbview.Location = new System.Drawing.Point(15, 60);
            this.clbview.Name = "clbview";
            this.clbview.Size = new System.Drawing.Size(406, 342);
            this.clbview.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnclear);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtsolution);
            this.groupBox1.Controls.Add(this.txtapiname);
            this.groupBox1.Controls.Add(this.txtcomp);
            this.groupBox1.Controls.Add(this.groupBox6);
            this.groupBox1.Controls.Add(this.cbdefaultcon);
            this.groupBox1.Controls.Add(this.btnconnect);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtpassword);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtusername);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtdbname);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtdatasource);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.groupBox1.ForeColor = System.Drawing.SystemColors.Desktop;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(547, 694);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "DATABASE CONNECTION";
            // 
            // btnclear
            // 
            this.btnclear.Location = new System.Drawing.Point(138, 841);
            this.btnclear.Name = "btnclear";
            this.btnclear.Size = new System.Drawing.Size(154, 31);
            this.btnclear.TabIndex = 24;
            this.btnclear.Text = "TEMIZLE";
            this.btnclear.UseVisualStyleBackColor = true;
            this.btnclear.Click += new System.EventHandler(this.btnclear_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label8.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label8.Location = new System.Drawing.Point(4, 793);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(98, 22);
            this.label8.TabIndex = 23;
            this.label8.Text = "Api Name :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label7.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label7.Location = new System.Drawing.Point(4, 757);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(86, 22);
            this.label7.TabIndex = 23;
            this.label7.Text = "Solution :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label6.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label6.Location = new System.Drawing.Point(4, 721);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(97, 22);
            this.label6.TabIndex = 22;
            this.label6.Text = "Company :";
            // 
            // txtsolution
            // 
            this.txtsolution.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtsolution.Location = new System.Drawing.Point(174, 749);
            this.txtsolution.Name = "txtsolution";
            this.txtsolution.Size = new System.Drawing.Size(360, 30);
            this.txtsolution.TabIndex = 21;
            this.txtsolution.Text = "Avs3";
            // 
            // txtapiname
            // 
            this.txtapiname.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtapiname.Location = new System.Drawing.Point(174, 785);
            this.txtapiname.Name = "txtapiname";
            this.txtapiname.Size = new System.Drawing.Size(360, 30);
            this.txtapiname.TabIndex = 20;
            this.txtapiname.Text = "Mono.Crud";
            // 
            // txtcomp
            // 
            this.txtcomp.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtcomp.Location = new System.Drawing.Point(174, 713);
            this.txtcomp.Name = "txtcomp";
            this.txtcomp.Size = new System.Drawing.Size(360, 30);
            this.txtcomp.TabIndex = 17;
            this.txtcomp.Text = "Dcl";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.groupBox9);
            this.groupBox6.Controls.Add(this.groupBox4);
            this.groupBox6.Controls.Add(this.groupBox5);
            this.groupBox6.Controls.Add(this.groupBox8);
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.groupBox6.ForeColor = System.Drawing.SystemColors.Desktop;
            this.groupBox6.Location = new System.Drawing.Point(12, 255);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(514, 452);
            this.groupBox6.TabIndex = 16;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "SEÇİMLER";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.cbmodels);
            this.groupBox9.Controls.Add(this.cbcontroller);
            this.groupBox9.Controls.Add(this.cbopclasses);
            this.groupBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.groupBox9.Location = new System.Drawing.Point(9, 30);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(137, 101);
            this.groupBox9.TabIndex = 9;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Creating Classes";
            // 
            // cbmodels
            // 
            this.cbmodels.AutoSize = true;
            this.cbmodels.Checked = true;
            this.cbmodels.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbmodels.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.cbmodels.Location = new System.Drawing.Point(6, 23);
            this.cbmodels.Name = "cbmodels";
            this.cbmodels.Size = new System.Drawing.Size(72, 20);
            this.cbmodels.TabIndex = 3;
            this.cbmodels.Text = "Models";
            this.cbmodels.UseVisualStyleBackColor = true;
            // 
            // cbcontroller
            // 
            this.cbcontroller.AutoSize = true;
            this.cbcontroller.Checked = true;
            this.cbcontroller.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbcontroller.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.cbcontroller.Location = new System.Drawing.Point(6, 62);
            this.cbcontroller.Name = "cbcontroller";
            this.cbcontroller.Size = new System.Drawing.Size(84, 20);
            this.cbcontroller.TabIndex = 5;
            this.cbcontroller.Text = "Controller";
            this.cbcontroller.UseVisualStyleBackColor = true;
            // 
            // cbopclasses
            // 
            this.cbopclasses.AutoSize = true;
            this.cbopclasses.Checked = true;
            this.cbopclasses.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbopclasses.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.cbopclasses.Location = new System.Drawing.Point(6, 43);
            this.cbopclasses.Name = "cbopclasses";
            this.cbopclasses.Size = new System.Drawing.Size(129, 20);
            this.cbopclasses.TabIndex = 4;
            this.cbopclasses.Text = "Process Classes";
            this.cbopclasses.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.lblwarning);
            this.groupBox4.Controls.Add(this.progbarclasses);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.groupBox4.ForeColor = System.Drawing.SystemColors.Desktop;
            this.groupBox4.Location = new System.Drawing.Point(12, 353);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(296, 93);
            this.groupBox4.TabIndex = 14;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "PROGRESS";
            // 
            // lblwarning
            // 
            this.lblwarning.AutoSize = true;
            this.lblwarning.ForeColor = System.Drawing.Color.OrangeRed;
            this.lblwarning.Location = new System.Drawing.Point(6, 56);
            this.lblwarning.Name = "lblwarning";
            this.lblwarning.Size = new System.Drawing.Size(257, 25);
            this.lblwarning.TabIndex = 8;
            this.lblwarning.Text = "Tüm İşlemler Tamamlandı";
            this.lblwarning.Visible = false;
            // 
            // progbarclasses
            // 
            this.progbarclasses.Location = new System.Drawing.Point(6, 30);
            this.progbarclasses.Name = "progbarclasses";
            this.progbarclasses.Size = new System.Drawing.Size(280, 23);
            this.progbarclasses.TabIndex = 7;
            this.progbarclasses.Visible = false;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.button3);
            this.groupBox5.Controls.Add(this.button1);
            this.groupBox5.Controls.Add(this.btnmvcCreate);
            this.groupBox5.Controls.Add(this.btnsqlview);
            this.groupBox5.Controls.Add(this.button2);
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.groupBox5.ForeColor = System.Drawing.SystemColors.Desktop;
            this.groupBox5.Location = new System.Drawing.Point(14, 139);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(481, 208);
            this.groupBox5.TabIndex = 15;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "CREATE CLASS";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(338, 67);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(123, 67);
            this.button3.TabIndex = 27;
            this.button3.Text = "Dropdown";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click_1);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(9, 67);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 67);
            this.button1.TabIndex = 26;
            this.button1.Text = "Mvc Get";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // btnmvcCreate
            // 
            this.btnmvcCreate.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnmvcCreate.Location = new System.Drawing.Point(112, 67);
            this.btnmvcCreate.Name = "btnmvcCreate";
            this.btnmvcCreate.Size = new System.Drawing.Size(104, 68);
            this.btnmvcCreate.TabIndex = 25;
            this.btnmvcCreate.Text = "MVC Crud";
            this.btnmvcCreate.UseVisualStyleBackColor = true;
            this.btnmvcCreate.Click += new System.EventHandler(this.btnmvcCreate_Click);
            // 
            // btnsqlview
            // 
            this.btnsqlview.Location = new System.Drawing.Point(222, 67);
            this.btnsqlview.Name = "btnsqlview";
            this.btnsqlview.Size = new System.Drawing.Size(110, 67);
            this.btnsqlview.TabIndex = 20;
            this.btnsqlview.Text = "Sql View";
            this.btnsqlview.UseVisualStyleBackColor = true;
            this.btnsqlview.Click += new System.EventHandler(this.btnsqlview_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(134, 29);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(135, 32);
            this.button2.TabIndex = 17;
            this.button2.Text = "Browse";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label5.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label5.Location = new System.Drawing.Point(16, 39);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(112, 22);
            this.label5.TabIndex = 16;
            this.label5.Text = "Project Path:";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.cbpersistnull);
            this.groupBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.groupBox8.Location = new System.Drawing.Point(157, 30);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(162, 101);
            this.groupBox8.TabIndex = 8;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Functionally";
            // 
            // cbpersistnull
            // 
            this.cbpersistnull.AutoSize = true;
            this.cbpersistnull.Checked = true;
            this.cbpersistnull.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbpersistnull.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.cbpersistnull.Location = new System.Drawing.Point(0, 18);
            this.cbpersistnull.Name = "cbpersistnull";
            this.cbpersistnull.Size = new System.Drawing.Size(142, 20);
            this.cbpersistnull.TabIndex = 6;
            this.cbpersistnull.Text = "Persist Null Column";
            this.cbpersistnull.UseVisualStyleBackColor = true;
            // 
            // cbdefaultcon
            // 
            this.cbdefaultcon.AutoSize = true;
            this.cbdefaultcon.Location = new System.Drawing.Point(16, 155);
            this.cbdefaultcon.Name = "cbdefaultcon";
            this.cbdefaultcon.Size = new System.Drawing.Size(214, 29);
            this.cbdefaultcon.TabIndex = 16;
            this.cbdefaultcon.Text = "Default Connection";
            this.cbdefaultcon.UseVisualStyleBackColor = true;
            this.cbdefaultcon.CheckedChanged += new System.EventHandler(this.cbdefaultcon_CheckedChanged);
            // 
            // btnconnect
            // 
            this.btnconnect.Location = new System.Drawing.Point(12, 190);
            this.btnconnect.Name = "btnconnect";
            this.btnconnect.Size = new System.Drawing.Size(154, 47);
            this.btnconnect.TabIndex = 12;
            this.btnconnect.Text = "CONNECT";
            this.btnconnect.UseVisualStyleBackColor = true;
            this.btnconnect.Click += new System.EventHandler(this.btnconnect_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label4.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label4.Location = new System.Drawing.Point(12, 127);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(99, 22);
            this.label4.TabIndex = 14;
            this.label4.Text = "Password :";
            // 
            // txtpassword
            // 
            this.txtpassword.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtpassword.Location = new System.Drawing.Point(160, 119);
            this.txtpassword.Name = "txtpassword";
            this.txtpassword.Size = new System.Drawing.Size(122, 30);
            this.txtpassword.TabIndex = 15;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label3.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label3.Location = new System.Drawing.Point(13, 95);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 22);
            this.label3.TabIndex = 12;
            this.label3.Text = "UserName :";
            // 
            // txtusername
            // 
            this.txtusername.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtusername.Location = new System.Drawing.Point(160, 87);
            this.txtusername.Name = "txtusername";
            this.txtusername.Size = new System.Drawing.Size(122, 30);
            this.txtusername.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label2.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label2.Location = new System.Drawing.Point(11, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(143, 22);
            this.label2.TabIndex = 10;
            this.label2.Text = "Database Name :";
            // 
            // txtdbname
            // 
            this.txtdbname.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtdbname.Location = new System.Drawing.Point(160, 54);
            this.txtdbname.Name = "txtdbname";
            this.txtdbname.Size = new System.Drawing.Size(230, 30);
            this.txtdbname.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label1.Location = new System.Drawing.Point(12, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(115, 22);
            this.label1.TabIndex = 1;
            this.label1.Text = "Data Source :";
            // 
            // txtdatasource
            // 
            this.txtdatasource.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtdatasource.Location = new System.Drawing.Point(160, 22);
            this.txtdatasource.Name = "txtdatasource";
            this.txtdatasource.Size = new System.Drawing.Size(229, 30);
            this.txtdatasource.TabIndex = 2;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1276, 694);
            this.Controls.Add(this.panel1);
            this.Name = "MainForm";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.panel1.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ProgressBar progbarclasses;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckedListBox clbview;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckedListBox clbtables;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnconnect;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtpassword;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtusername;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtdbname;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtdatasource;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.CheckBox cbdefaultcon;
        private System.Windows.Forms.Label lblwarning;
        private System.Windows.Forms.CheckBox cbview;
        private System.Windows.Forms.CheckBox cbtables;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.CheckBox cbmodels;
        private System.Windows.Forms.CheckBox cbcontroller;
        private System.Windows.Forms.CheckBox cbopclasses;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.CheckBox cbpersistnull;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtsolution;
        private System.Windows.Forms.TextBox txtapiname;
        private System.Windows.Forms.TextBox txtcomp;
        private System.Windows.Forms.Button btnclear;
        private System.Windows.Forms.Button btnsqlview;
        private System.Windows.Forms.Button btnmvcCreate;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button3;
    }
}

