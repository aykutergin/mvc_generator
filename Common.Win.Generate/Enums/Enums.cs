﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Avs.Win.Generate
{
    public enum ColumnType
    {
        Image,
        File,
        Date,
        Text,
        TextArea,
        HtmlText,
        CheckBox,
        ComboBox,
        RadioButton,
        Primery,
        Name,
        Email,
        Url,
        Number,
    }

    public enum TableType
    {
        Image,
        File,
        Default
    }
}
