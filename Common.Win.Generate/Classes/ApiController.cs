﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;

namespace Avs.Win.Generate
{
    public class ApiController
    {
        private Table table = new Table();
        private string filepath = String.Empty;

        public ApiController(Table table)
        {
            this.table = table;
            this.filepath = Constants.ApiControllerFolder + "\\" + table.name + "Controller.cs";
        }

        public bool WriteApiControllerFile()
        {
            try
            {
                FillApiControllerClassContent();
                return true;
            }
            catch (Exception)
            {

            }
            return false;

        }
        private void FillApiControllerClassContent()
        {
            StreamWriter file = new StreamWriter(filepath, false, Encoding.UTF8);//(filepath);
            file.WriteLine("using log4net;");
            file.WriteLine("using System;");
            file.WriteLine("using System.Collections.Generic;");
            file.WriteLine("using System.Web.Http;");
            file.WriteLine("using System.Linq;");
            file.WriteLine("using Newtonsoft.Json;");
            file.WriteLine("using "+Constants.api_ns+".Enums;");
            file.WriteLine("using "+Constants.api_ns+".Models;");
            file.WriteLine("using "+Constants.api_ns+".Security;");
            file.WriteLine("using "+Constants.dto_ns+";");
            file.WriteLine("using "+Constants.api_ns+".Services;");
            file.WriteLine("using "+Constants.dto_ns+".Select;");
            file.WriteLine("using "+Constants.dto_ns+".Filter;");
        
            file.WriteLine("");
            
            file.WriteLine("namespace "+Constants.api_ns+".Controllers");
            file.WriteLine("{");
            file.WriteLine("public class " + table.name + "Controller : ApiController");
            file.WriteLine("{");
            file.WriteLine("private static ILog log = LogManager.GetLogger(typeof(" + table.name + "Controller));");
            file.WriteLine("");
            file.WriteLine("private " + table.name + "Service " + table.name + "_service = new " + table.name + "Service();");
            file.WriteLine("");
            
            file.WriteLine("[ApiAuthorize]");
            file.WriteLine("[HttpGet]");
            file.WriteLine("[Route(\"" + table.name + "/get\")]");
            file.WriteLine("public IHttpActionResult Get" + table.name + "([FromUri] " + table.name + "Param p)");

            file.WriteLine("{");
            file.WriteLine("ResultModel result;");
            file.WriteLine("" + table.name + "Filter filter = null;");
            file.WriteLine("" + table.name + "Select select = null;");
            file.WriteLine("if (p != null)");
            file.WriteLine("{");
            file.WriteLine("filter = p.filter;");
            file.WriteLine("select = p.select;");
            file.WriteLine("}");
            file.WriteLine("try");
            file.WriteLine("{");
            file.WriteLine("#if DEBUG");
            file.WriteLine("log.InfoFormat(\"Get" + table.name + "s filter {0}\", JsonConvert.SerializeObject(filter));");
            file.WriteLine("log.InfoFormat(\"Get" + table.name + "s select {0}\", JsonConvert.SerializeObject(select));");
            file.WriteLine("#endif");
            file.WriteLine("");
            file.WriteLine("List<" + table.name + "Dto> retrived = " + table.name + "_service.Get(filter, select);");
            file.WriteLine("if (retrived == null)");
            file.WriteLine("{");
            file.WriteLine("result = new ResultModel(ResultEnum.Error, ResultCodeEnum.GlobalError);");
            file.WriteLine("}");
            file.WriteLine("else");
            file.WriteLine("{");
            file.WriteLine("result = new ResultModel(ResultEnum.Successfull, ResultCodeEnum.GlobalOk, retrived);");
            file.WriteLine("}");
            file.WriteLine("}");
            file.WriteLine("catch (Exception ex)");
            file.WriteLine("{");
            file.WriteLine("log.Error(\"Get" + table.name + "s\", ex);");
            file.WriteLine("result = new ResultModel(ResultEnum.Error, ResultCodeEnum.GlobalError);");
            file.WriteLine("}");
            file.WriteLine("");
            file.WriteLine("#if DEBUG");
            file.WriteLine("log.InfoFormat(\"Get" + table.name + "s resp {0}\", JsonConvert.SerializeObject(result));");
            file.WriteLine("#endif");
            file.WriteLine("");
            file.WriteLine("return Ok(result);");
            file.WriteLine("}");
            file.WriteLine("");
            
            file.WriteLine("[ApiAuthorize]");
            file.WriteLine("[HttpGet]");
            file.WriteLine("[Route(\"" + table.name + "/{id}/get\")]");
            file.WriteLine("public IHttpActionResult Get" + table.name + "("+table.primery_column.dataType+" id, [FromUri] " + table.name + "Select select)");
            file.WriteLine("{");
            file.WriteLine("ResultModel result;");
            file.WriteLine("try");
            file.WriteLine("{");
            file.WriteLine("#if DEBUG");
            file.WriteLine("log.InfoFormat(\"Get" + table.name + " select {0}\", JsonConvert.SerializeObject(select));");
            file.WriteLine("#endif");
            file.WriteLine("");
            file.WriteLine("" + table.name + "Dto retrived = " + table.name + "_service.Get(id, select);");
            file.WriteLine("if (retrived == null)");
            file.WriteLine("{");
            file.WriteLine("result = new ResultModel(ResultEnum.Error, ResultCodeEnum.GlobalError);");
            file.WriteLine("}");
            file.WriteLine("else");
            file.WriteLine("{");
            file.WriteLine("result = new ResultModel(ResultEnum.Successfull, ResultCodeEnum.GlobalOk, retrived);");
            file.WriteLine("}");
            file.WriteLine("}");
            file.WriteLine("catch (Exception ex)");
            file.WriteLine("{");
            file.WriteLine("log.Error(\"Get" + table.name + "\", ex);");
            file.WriteLine("result = new ResultModel(ResultEnum.Error, ResultCodeEnum.GlobalError);");
            file.WriteLine("}");
            file.WriteLine("");
            file.WriteLine("#if DEBUG");
            file.WriteLine("log.InfoFormat(\"Get" + table.name + " resp {0}\", JsonConvert.SerializeObject(result));");
            file.WriteLine("#endif");
            file.WriteLine("");
            file.WriteLine("return Ok(result);");
            file.WriteLine("}");
            file.WriteLine("");
            file.WriteLine("");
            
            file.WriteLine("[ApiAuthorize]");
            file.WriteLine("[HttpGet]");
            file.WriteLine("[Route(\"" + table.name + "/count\")]");
            file.WriteLine("public IHttpActionResult Count" + table.name + "([FromUri] " + table.name + "Filter filter) ");
            file.WriteLine("{ ");
            file.WriteLine("ResultModel result; ");
            file.WriteLine("try ");
            file.WriteLine("{");
            file.WriteLine("#if DEBUG");
            file.WriteLine("log.InfoFormat(\"Count" + table.name + " filter {0}\", JsonConvert.SerializeObject(filter));");
            file.WriteLine("#endif");
            file.WriteLine("");
            file.WriteLine("int? count= " + table.name + "_service.Count(filter);");
            file.WriteLine("if (count == null)");
            file.WriteLine("{");
            file.WriteLine("result = new ResultModel(ResultEnum.Error, ResultCodeEnum.GlobalError);");
            file.WriteLine("}");
            file.WriteLine("else");
            file.WriteLine("{");
            file.WriteLine("result = new ResultModel(ResultEnum.Successfull, ResultCodeEnum.GlobalOk, count);");
            file.WriteLine("}");
            file.WriteLine("}");
            file.WriteLine("catch (Exception ex)");
            file.WriteLine("{");
            file.WriteLine("log.Error(\"Count" + table.name + "\", ex);");
            file.WriteLine("result = new ResultModel(ResultEnum.Error, ResultCodeEnum.GlobalError);");
            file.WriteLine("}");
            file.WriteLine("");
            file.WriteLine("#if DEBUG");
            file.WriteLine("log.InfoFormat(\"Count" + table.name + " resp {0}\", JsonConvert.SerializeObject(result));");
            file.WriteLine("#endif");
            file.WriteLine("");
            file.WriteLine("return Ok(result);");
            file.WriteLine("}");
            
            //create
            file.WriteLine("[ApiAuthorize]");
            file.WriteLine("[HttpPost]");
            file.WriteLine("[Route(\"" + table.name + "/create\")]");
            file.WriteLine("public IHttpActionResult Create" + table.name + "(" + table.name + "Dto " + table.name + ")");
            file.WriteLine("{");
            file.WriteLine("ResultModel result;");
            file.WriteLine("try");
            file.WriteLine("{");
            file.WriteLine("#if DEBUG");
            file.WriteLine("log.InfoFormat(\"Create" + table.name + " {0}\", JsonConvert.SerializeObject(" + table.name + "));");
            file.WriteLine("#endif");
            file.WriteLine("");
            file.WriteLine("" + table.name + "Dto created = " + table.name + "_service.Create(" + table.name + ");");
            file.WriteLine("if (created == null)");
            file.WriteLine("{");
            file.WriteLine("result = new ResultModel(ResultEnum.Error, ResultCodeEnum.GlobalError);");
            file.WriteLine("}");
            file.WriteLine("else");
            file.WriteLine("{");
            file.WriteLine("result = new ResultModel(ResultEnum.Successfull, ResultCodeEnum.GlobalOk, created);");
            file.WriteLine("}");
            file.WriteLine("}");
            file.WriteLine("catch (Exception ex)");
            file.WriteLine("{");
            file.WriteLine("log.Error(\"Create" + table.name + "\", ex);");
            file.WriteLine("result = new ResultModel(ResultEnum.Error, ResultCodeEnum.GlobalError);");
            file.WriteLine("}");
            file.WriteLine("");
            file.WriteLine("#if DEBUG");
            file.WriteLine("log.InfoFormat(\"Create" + table.name + " resp {0}\", JsonConvert.SerializeObject(result));");
            file.WriteLine("#endif");
            file.WriteLine("");
            file.WriteLine("return Ok(result);");
            file.WriteLine("}");
            file.WriteLine("");
            
            //update
            file.WriteLine("[ApiAuthorize]");
            file.WriteLine("[HttpPost]");
            file.WriteLine("[Route(\"" + table.name + "/update\")]");
            file.WriteLine("public IHttpActionResult Update" + table.name + "(" + table.name + "Dto " + table.name + ")");
            file.WriteLine("{");
            file.WriteLine("ResultModel result;");
            file.WriteLine("try");
            file.WriteLine("{");
            file.WriteLine("#if DEBUG");
            file.WriteLine("log.InfoFormat(\"Update" + table.name + " {0}\", JsonConvert.SerializeObject(" + table.name + "));");
            file.WriteLine("#endif");
            file.WriteLine("");
            file.WriteLine("" + table.name + "Dto updated = " + table.name + "_service.Update(" + table.name + ");");
            file.WriteLine("if (updated == null)");
            file.WriteLine("{");
            file.WriteLine("result = new ResultModel(ResultEnum.Error, ResultCodeEnum.GlobalError);");
            file.WriteLine("}");
            file.WriteLine("else");
            file.WriteLine("{");
            file.WriteLine("result = new ResultModel(ResultEnum.Successfull, ResultCodeEnum.GlobalOk, updated);");
            file.WriteLine("}");
            file.WriteLine("}");
            file.WriteLine("catch (Exception ex)");
            file.WriteLine("{");
            file.WriteLine("log.Error(\"update" + table.name + "\", ex);");
            file.WriteLine("result = new ResultModel(ResultEnum.Error, ResultCodeEnum.GlobalError);");
            file.WriteLine("}");
            file.WriteLine("");
            file.WriteLine("#if DEBUG");
            file.WriteLine("log.InfoFormat(\"Update" + table.name + " resp {0}\", JsonConvert.SerializeObject(result));");
            file.WriteLine("#endif");
            file.WriteLine("");
            file.WriteLine("return Ok(result);");
            file.WriteLine("}");
            file.WriteLine("");

            //delete
            file.WriteLine("[ApiAuthorize]");
            file.WriteLine("[HttpPost]");
            file.WriteLine("[Route(\"" + table.name + "/delete\")]");
            file.WriteLine("public IHttpActionResult Delete" + table.name + "(" + table.name + "Dto " + table.name + ")");
            file.WriteLine("{");
            file.WriteLine("ResultModel result;");
            file.WriteLine("try");
            file.WriteLine("{");
            file.WriteLine("#if DEBUG");
            file.WriteLine("log.InfoFormat(\"Delete" + table.name + " {0}\", JsonConvert.SerializeObject(" + table.name + "));");
            file.WriteLine("#endif");
            file.WriteLine("");
            file.WriteLine("bool deleted= " + table.name + "_service.Delete("+ table.name + "."+table.primery_column.name+");");
            file.WriteLine("if (deleted == false)");
            file.WriteLine("{");
            file.WriteLine("result = new ResultModel(ResultEnum.Error, ResultCodeEnum.GlobalError);");
            file.WriteLine("}");
            file.WriteLine("else");
            file.WriteLine("{");
            file.WriteLine("result = new ResultModel(ResultEnum.Successfull, ResultCodeEnum.GlobalOk, deleted);");
            file.WriteLine("}");
            file.WriteLine("}");
            file.WriteLine("catch (Exception ex)");
            file.WriteLine("{");
            file.WriteLine("log.Error(\"Delete" + table.name + "\", ex);");
            file.WriteLine("result = new ResultModel(ResultEnum.Error, ResultCodeEnum.GlobalError);");
            file.WriteLine("}");
            file.WriteLine("");
            file.WriteLine("#if DEBUG");
            file.WriteLine("log.InfoFormat(\"Delete" + table.name + " resp {0}\", JsonConvert.SerializeObject(result));");
            file.WriteLine("#endif");
            file.WriteLine("");
            file.WriteLine("return Ok(result);");
            file.WriteLine("}");


            //create All
            file.WriteLine("[ApiAuthorize]");
            file.WriteLine("[HttpPost]");
            file.WriteLine("[Route(\"" + table.name + "/createAll\")]");
            file.WriteLine("public IHttpActionResult Create" + table.name + "(List<" + table.name + "Dto> " + table.name + ")");
            file.WriteLine("{");
            file.WriteLine("ResultModel result;");
            file.WriteLine("try");
            file.WriteLine("{");
            file.WriteLine("#if DEBUG");
            file.WriteLine("log.InfoFormat(\"Create " + table.name + " All {0}\", JsonConvert.SerializeObject(" + table.name + "));");
            file.WriteLine("#endif");
            file.WriteLine("");
            file.WriteLine("List<" + table.name + "Dto> created = " + table.name + "_service.Create(" + table.name + ");");
            file.WriteLine("if (created == null)");
            file.WriteLine("{");
            file.WriteLine("result = new ResultModel(ResultEnum.Error, ResultCodeEnum.GlobalError);");
            file.WriteLine("}");
            file.WriteLine("else");
            file.WriteLine("{");
            file.WriteLine("result = new ResultModel(ResultEnum.Successfull, ResultCodeEnum.GlobalOk, created);");
            file.WriteLine("}");
            file.WriteLine("}");
            file.WriteLine("catch (Exception ex)");
            file.WriteLine("{");
            file.WriteLine("log.Error(\"Create" + table.name + "\", ex);");
            file.WriteLine("result = new ResultModel(ResultEnum.Error, ResultCodeEnum.GlobalError);");
            file.WriteLine("}");
            file.WriteLine("");
            file.WriteLine("#if DEBUG");
            file.WriteLine("log.InfoFormat(\"Create" + table.name + " All resp {0}\", JsonConvert.SerializeObject(result));");
            file.WriteLine("#endif");
            file.WriteLine("");
            file.WriteLine("return Ok(result);");
            file.WriteLine("}");
            file.WriteLine("");

            //update All
            file.WriteLine("[ApiAuthorize]");
            file.WriteLine("[HttpPost]");
            file.WriteLine("[Route(\"" + table.name + "/updateAll\")]");
            file.WriteLine("public IHttpActionResult Update" + table.name + "(List<" + table.name + "Dto> " + table.name + ")");
            file.WriteLine("{");
            file.WriteLine("ResultModel result;");
            file.WriteLine("try");
            file.WriteLine("{");
            file.WriteLine("#if DEBUG");
            file.WriteLine("log.InfoFormat(\"Update" + table.name + " All {0}\", JsonConvert.SerializeObject(" + table.name + "));");
            file.WriteLine("#endif");
            file.WriteLine("");
            file.WriteLine("List<" + table.name + "Dto> updated = " + table.name + "_service.Update(" + table.name + ");");
            file.WriteLine("if (updated == null)");
            file.WriteLine("{");
            file.WriteLine("result = new ResultModel(ResultEnum.Error, ResultCodeEnum.GlobalError);");
            file.WriteLine("}");
            file.WriteLine("else");
            file.WriteLine("{");
            file.WriteLine("result = new ResultModel(ResultEnum.Successfull, ResultCodeEnum.GlobalOk, updated);");
            file.WriteLine("}");
            file.WriteLine("}");
            file.WriteLine("catch (Exception ex)");
            file.WriteLine("{");
            file.WriteLine("log.Error(\"update" + table.name + "\", ex);");
            file.WriteLine("result = new ResultModel(ResultEnum.Error, ResultCodeEnum.GlobalError);");
            file.WriteLine("}");
            file.WriteLine("");
            file.WriteLine("#if DEBUG");
            file.WriteLine("log.InfoFormat(\"Update" + table.name + " All resp {0}\", JsonConvert.SerializeObject(result));");
            file.WriteLine("#endif");
            file.WriteLine("");
            file.WriteLine("return Ok(result);");
            file.WriteLine("}");
            file.WriteLine("");

            //delete ALl
            file.WriteLine("[ApiAuthorize]");
            file.WriteLine("[HttpPost]");
            file.WriteLine("[Route(\"" + table.name + "/deleteAll\")]");
            file.WriteLine("public IHttpActionResult Delete" + table.name + "(List<" + table.name + "Dto> " + table.name + ")");
            file.WriteLine("{");
            file.WriteLine("ResultModel result;");
            file.WriteLine("try");
            file.WriteLine("{");
            file.WriteLine("#if DEBUG");
            file.WriteLine("log.InfoFormat(\"Delete" + table.name + " {0}\", JsonConvert.SerializeObject(" + table.name + "));");
            file.WriteLine("#endif");
            file.WriteLine("");
            file.WriteLine("bool deleted= " + table.name + "_service.Delete(" + table.name + ".Select(x=>x." + table.primery_column.name + ").ToList());");
            file.WriteLine("if (deleted == false)");
            file.WriteLine("{");
            file.WriteLine("result = new ResultModel(ResultEnum.Error, ResultCodeEnum.GlobalError);");
            file.WriteLine("}");
            file.WriteLine("else");
            file.WriteLine("{");
            file.WriteLine("result = new ResultModel(ResultEnum.Successfull, ResultCodeEnum.GlobalOk, deleted);");
            file.WriteLine("}");
            file.WriteLine("}");
            file.WriteLine("catch (Exception ex)");
            file.WriteLine("{");
            file.WriteLine("log.Error(\"Delete" + table.name + "\", ex);");
            file.WriteLine("result = new ResultModel(ResultEnum.Error, ResultCodeEnum.GlobalError);");
            file.WriteLine("}");
            file.WriteLine("");
            file.WriteLine("#if DEBUG");
            file.WriteLine("log.InfoFormat(\"Delete" + table.name + " resp {0}\", JsonConvert.SerializeObject(result));");
            file.WriteLine("#endif");
            file.WriteLine("");
            file.WriteLine("return Ok(result);");
            file.WriteLine("}");



            //endclass
            file.WriteLine("");
            file.WriteLine("");
            file.WriteLine("");
            file.WriteLine("}");
            file.WriteLine("}");

            file.Close();

        }

    }
}
