﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;

namespace Avs.Win.Generate
{
    public class ReportSourceTableTransfer
    {
        private Table table = new Table();
        private string filepath = String.Empty;

        public ReportSourceTableTransfer(Table table)
        {
            this.table = table;
            this.filepath = Constants.ReportSourceTableTransferFolder + "\\" + table.name + "Transfer.cs";
        }

        public bool WriteReportSourceTableTransferFile()
        {
            try
            {
                FillReportSourceTableTransferClassContent();
                return true;
            }
            catch (Exception)
            {

            }
            return false;

        }
        private void FillReportSourceTableTransferClassContent()
        {
            string str = File.ReadAllText("WebReport//SourceTableTransfer.txt", Encoding.GetEncoding("iso-8859-9"));
            StreamWriter file = new StreamWriter(filepath, false, Encoding.UTF8);//(filepath);
            str = str.Replace("#tName", table.name).Replace("#dtoToModel", GetObjContent("dto")).Replace("#modelToDto", GetObjContent("model")).Replace("#projName", GetMainName());
            file.Write(str);
            file.Close();
        }
        private string GetObjContent(string objName)
        {
            string context = "";
            foreach (var column in table.columns)
            {
                context += column.name + " = " + objName + "." + column.name + ",\n\t\t\t\t";
            }
            return context.Remove(context.Length - 6);
        }
        Dictionary<string, string> mainTableNameList = new Dictionary<string, string>() {
            {"Al","Alarm"},
            {"Ap","App"},
            {"Ca","Cash"},
            {"Cc","CreditCard"},
            {"Cf","Config"},
            {"Ch","Cashier"},
            {"Co","Comp"},
            {"Cu","Cust"},
            {"De","Device"},
            {"Dm","DeviceMaint"},
            {"En","Endpoint"},
            {"Eo","Eod"},
            {"Ev","EventLog"},
            {"In","Intro"},
            {"Ki","Kiosk"},
            {"La","Lang"},
            {"Ma","Master"},
            {"Me","Media"},
            {"Mn","Maint"},
            {"Pr","Print"},
            {"Rc","Reco"},
            {"Ri","Right"},
            {"Rp","Repo"},
            {"Tr","Trnx"},
            {"Us","User"}
        };
        private string GetMainName()
        {
            string name = "";
            foreach (var item in mainTableNameList)
            {
                if (table.name.Substring(0,2) == item.Key)
                    return item.Value;
            }
            return name;
        }

    }
}
