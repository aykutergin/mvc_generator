﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;

namespace Avs.Win.Generate
{
    public class Crud
    {
        private Table table = new Table();
        private string filepath = String.Empty;

        public Crud(Table table)
        {
            this.table = table;
            this.filepath = Constants.CrudsFolder + "\\" + table.name + "Dal.cs";
        }

        public bool WriteCrudFile()
        {
            try
            {
                FillCrudClassContent();
                return true;
            }
            catch (Exception)
            {

            }
            return false;

        }
 

        private void FillCrudClassContent()
        {
            int index = table.name.Length - 1;
            //string dbpulirze = tools.GetPulirze(table.name);
            string dbpulirze = "db." + table.namePlural;

            StreamWriter file = new StreamWriter(filepath);
            file.WriteLine("using System;");
            file.WriteLine("using System.Collections.Generic;");
            file.WriteLine("using System.Data.Entity;");
            file.WriteLine("using System.Linq;");


            file.WriteLine("namespace "+Constants.ent_ns+".DAL");
            file.WriteLine("{");
            file.WriteLine("public partial class " + table.name + "Dal : ADal");

            file.WriteLine("{");
            file.WriteLine("public IEnumerable<" + table.name + "> Get" + table.name + "() {");
            file.WriteLine("return " + dbpulirze + ";");
            file.WriteLine("}");

            file.WriteLine("public " + table.name + " Get" + table.name + "("+table.primery_column.dataType+" id)");
            file.WriteLine("{");
            file.WriteLine("return " + dbpulirze + ".Find(id);");
            file.WriteLine("}");

            file.WriteLine("public " + table.name + " Create" + table.name + "(" + table.name + " " + table.name.ToLower() + ")");
            file.WriteLine("{");
            file.WriteLine(dbpulirze + ".Add(" + table.name.ToLower() + ");");
            file.WriteLine("db.SaveChanges();");
            file.WriteLine("return " + table.name.ToLower() + ";");
            file.WriteLine("}");

            file.WriteLine("public " + table.name + " Update" + table.name + "(" + table.name + " " + table.name.ToLower() + ")");
            file.WriteLine("{");
            file.WriteLine("db.Entry(" + table.name.ToLower() + ").State = EntityState.Modified;");
            file.WriteLine("db.SaveChanges();");
            file.WriteLine("return " + table.name.ToLower() + ";");
            file.WriteLine("}");

            file.WriteLine("public void Delete" + table.name + "(" + table.primery_column.dataType + " id)");
            file.WriteLine("{");
            file.WriteLine(dbpulirze + ".Remove(" + dbpulirze + ".Find(id));");
            file.WriteLine("db.SaveChanges();");
            file.WriteLine("}");
        
            file.WriteLine("public bool Exist" + table.name + "(" + table.primery_column.dataType + " id) {");
            file.WriteLine("return " + dbpulirze + ".Any(i => i." + table.GetPrimeryColumn().name + "==id);");
            file.WriteLine("}");
            file.WriteLine("}");
            file.WriteLine("}");

            file.Close();

        }
    }
}
