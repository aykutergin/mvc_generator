﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;

namespace Avs.Win.Generate
{
    public class ScriptPartial
    {
        private Table table = new Table();
        private string filepath = String.Empty;
        private string filepathget = String.Empty;

        public ScriptPartial(Table table)
        {
            this.table = table;
            this.filepath = Constants.Views + "\\" + table.name + "\\" + "_ScriptPartial.cshtml";
        }

        public bool WriteScriptPartialFile()
        {
            try
            {
                FillScriptPartialClassContent();
                return true;
            }
            catch (Exception ex)
            {

            }
            return false;
        }
        private void FillScriptPartialClassContent()
        {
            string str = File.ReadAllText("WebReport//Views//Index//ScriptPartial.txt", Encoding.GetEncoding("iso-8859-9"));
            StreamWriter file = new StreamWriter(filepath, false, Encoding.UTF8);
            file.Write(str);
            file.Close();
        }
    }
}
