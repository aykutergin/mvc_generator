﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;

namespace Avs.Win.Generate
{
    public class IndexTable
    {
        private Table table = new Table();
        private string filepath = String.Empty;
        private string filepathget = String.Empty;

        public IndexTable(Table table)
        {
            this.table = table;
            this.filepath = Constants.Views + "\\" + table.name + "\\" + "_" + table.name + ".cshtml";
            this.filepathget = Constants.Views + "\\" + table.name + "\\" + "_" + table.name + ".cshtml";
        }

        public bool WriteIndexTableFile()
        {
            try
            {
                FillIndexTableClassContent();
                return true;
            }
            catch (Exception ex)
            {

            }
            return false;

        }
        public bool WriteGetIndexTableFile()
        {
            try
            {
                FillGetIndexTableClassContent();
                return true;
            }
            catch (Exception ex)
            {

            }
            return false;

        }
        private void FillGetIndexTableClassContent()
        {
            string str = File.ReadAllText("WebReport//Views//Index//GetIndexTable.txt", Encoding.GetEncoding("iso-8859-9"));
            StreamWriter file = new StreamWriter(filepathget, false, Encoding.UTF8);//(filepath);
            str = str.Replace("#tName", table.name).Replace("#tPureName", table.name).Replace("#thList", GetThList()).Replace("#tdList", GetTdList()).Replace("#primaryKey", table.primery_column.name);
            file.Write(str);
            file.Close();
        }
        private void FillIndexTableClassContent()
        {
            string str = File.ReadAllText("WebReport//Views//Index//IndexTable.txt", Encoding.GetEncoding("iso-8859-9"));

            StreamWriter file = new StreamWriter(filepath, false, Encoding.UTF8);//(filepath);
            str = str.Replace("#tName", table.name).Replace("#tPureName", table.name).Replace("#thList", GetThList()).Replace("#tdList", GetTdList()).Replace("#primaryKey", table.primery_column.name);
            file.Write(str);
            file.Close();
        }
        private string GetThList()
        {
            string temp = "";
            foreach (var item in table.columns)
            {
                temp += "\n\t\t\t\t\t<th> @LangVal.Ins.GetVal(\"" + table.name + ".Index.Text." + item.name + "\",\"" + item.name + "\") </th>";
            }
            return temp;
        }
        private string GetTdList()
        {
            string temp = "";
            foreach (var item in table.columns)
            {
                temp += "\n\t\t\t\t\t\t<td> @item." + item.name + "</td>";
            }
            return temp;
        }

    }
}
