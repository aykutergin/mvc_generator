﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;

namespace Avs.Win.Generate
{
    public class Index
    {
        private Table table = new Table();
        private string filepath = String.Empty;

        public Index(Table table)
        {
            this.table = table;
            this.filepath = Constants.Views + "\\" + table.name + "\\" + "Index.cshtml";
        }

        public bool WriteIndexFile()
        {
            try
            {
                FillIndexClassContent();
                return true;
            }
            catch (Exception ex)
            {

            }
            return false;

        }
        public bool WriteGetIndexFile()
        {
            try
            {
                FillGetIndexClassContent();
                return true;
            }
            catch (Exception ex)
            {

            }
            return false;

        }
        private void FillGetIndexClassContent()
        {
            string str = File.ReadAllText("WebReport//Views//Index//GetIndex.txt", Encoding.GetEncoding("iso-8859-9"));
            StreamWriter file = new StreamWriter(filepath, false, Encoding.UTF8);//(filepath);
            str = str.Replace("#tName", table.name).Replace("#selectList", GetWithIdSelectContent());
            file.Write(str);
            file.Close();
        }
        private string GetWithIdSelectContent()
        {
            string str = File.ReadAllText("WebReport//Views//Index//GetSelectList.txt", Encoding.GetEncoding("iso-8859-9"));
            string context = "\n";
            string temp = "";
            temp = str;
            foreach (var cols in table.columns)
            {
                if (cols.name.Contains("Id"))
                {
                    temp = str;
                    context += "\n" + temp.Replace("#tName", table.name).Replace("#colName", cols.name);
                }
            }
            return context;
        }
        private void FillIndexClassContent()
        {
            string str = File.ReadAllText("WebReport//Views//Index//Index.txt", Encoding.GetEncoding("iso-8859-9"));
            StreamWriter file = new StreamWriter(filepath, false, Encoding.UTF8);//(filepath);
            str = str.Replace("#tName", table.name).Replace("#selectList", GetSelectContent());
            file.Write(str);
            file.Close();
        }
        private string GetSelectContent()
        {
            string str = File.ReadAllText("WebReport//Views//Index//SelectList.txt", Encoding.GetEncoding("iso-8859-9"));
            string context = "\n";
            string temp = "";
            temp = str;
            context+= temp.Replace("#tName", table.name).Replace("#colName", table.primery_column.name);//kendi primary keyi ile üretecek.
            foreach (var foreign in table.foreigns)
            {
                temp = str;
                context +="\n"+ temp.Replace("#tName", foreign.table.name).Replace("#colName", foreign.table.primery_column.name);
            }
            return context;
        }

    }
}
