﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;

namespace Avs.Win.Generate
{
    public class Detail
    {
        private Table table = new Table();
        private string filepath = String.Empty;

        public Detail(Table table)
        {
            this.table = table;
            this.filepath = Constants.Views + "\\" + table.name + "\\" + "Detail.cshtml";
        }

        public bool WriteDetailFile()
        {
            try
            {
                FillDetailClassContent();
                return true;
            }
            catch (Exception ex)
            {

            }
            return false;

        }
        private void FillDetailClassContent()
        {
            string detail = File.ReadAllText("WebReport//Views//Detail.txt", Encoding.GetEncoding("iso-8859-9"));
            StreamWriter file = new StreamWriter(filepath, false, Encoding.UTF8);//(filepath);
            detail = detail.Replace("#tName", table.name).Replace("#menuItem", GetDetailMenu()).Replace("#topMenu", GetTopMenu());
            file.Write(detail);
            file.Close();
        }
        private string GetDetailMenu()
        {//_DetailRowListPartial partialName
            string detailMenu = File.ReadAllText("WebReport//Views//DetailMenu.txt", Encoding.GetEncoding("iso-8859-9"));
            string temp = "\n";
            string context = "";
            temp += detailMenu;
            context += temp.Replace("#tForName", table.name).Replace("#tName", table.name).Replace("#colName", table.primery_column.name);//kendi primary keyi ile üretecek.
            foreach (var foreign in table.foreigns)
            {
                temp = detailMenu;
                context += "\n" + temp.Replace("#tForName", foreign.table.name).Replace("#tName", table.name).Replace("#colName", foreign.column.name);
            }
            return context;
        }
        private string GetTopMenu()
        {
            string detailTopMenu = File.ReadAllText("WebReport//Views//DetailTopMenu.txt", Encoding.GetEncoding("iso-8859-9"));
            string temp = "";
            string context = "";
            for (int i = 0; i < table.columns.Count/2; i++)
            {
                if (table.columns[i].isPrimery)
                {
                    continue;
                }
                temp = detailTopMenu;
                context += "\n" + temp.Replace("#tName", table.name).Replace("#colName", table.columns[i].name);
            }
            return context;
        }

    }
}
