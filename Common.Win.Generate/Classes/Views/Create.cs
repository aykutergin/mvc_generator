﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;

namespace Avs.Win.Generate
{
    public class Create
    {
        private Table table = new Table();
        private string filePathCreate = String.Empty;
        //private string filePathCreatePartial = String.Empty;
        private string filePathScriptPartial = String.Empty;
        private string filePathInputsUpdate = String.Empty;
        private string filePathInputsCreate = String.Empty;
        private FileUtility file = null;

        public Create(Table table, FileUtility file)
        {
            this.table = table;
            this.filePathCreate = Constants.Views + "\\" + table.name + "\\" + "Create.cshtml";
            this.filePathInputsCreate = Constants.Views + "\\" + table.name + "\\" + "InputsCreate.cshtml";
            this.filePathInputsUpdate = Constants.Views + "\\" + table.name + "\\" + "InputsUpdate.cshtml";
            //this.filePathCreatePartial = Constants.Views + "\\" + table.name + "\\" + "_CreatePartial.cshtml";
            this.filePathScriptPartial = Constants.Views + "\\" + table.name + "\\" + "_ScriptPartial.cshtml";
            this.file = file;//tüm tabloları dönmeden önce dışarıda script ve inputlar file içerisine okunup veriliyor.
        }

        public bool WriteCreateFile()
        {
            try
            {
                FillCreateClassContent();
                return true;
            }
            catch (Exception ex)
            {

            }
            return false;

        }
        private void FillCreateClassContent()
        {
            string create = File.ReadAllText("WebReport//Views//Create.txt", Encoding.GetEncoding("iso-8859-9"));
            string inputsUpdate = File.ReadAllText("WebReport//Views//InputsUpdate.txt", Encoding.GetEncoding("iso-8859-9"));
            string inputsCreate = File.ReadAllText("WebReport//Views//InputsCreate.txt", Encoding.GetEncoding("iso-8859-9"));
            string script = File.ReadAllText("WebReport//Views//Script.txt", Encoding.GetEncoding("iso-8859-9"));
            string rowGroupsFile = File.ReadAllText("WebReport//Views//RowGroup.txt", Encoding.GetEncoding("iso-8859-9"));
            string inputs = GetInputs(rowGroupsFile);
            string validations = GetValidations();
            string scripts = GetSripts();
            //string scriptTags = GetSriptTags();


            create = create.Replace("#tName", table.name);
            inputsUpdate = inputsUpdate.Replace("#tName", table.name).Replace("#inputs", inputs).Replace("#tForName", table.name);
            inputsCreate = inputsCreate.Replace("#tName", table.name).Replace("#inputs", inputs).Replace("#tForName", table.name);

            //şuan için create içerisinde üretilsin. Daha sonra dışarı alınacak.
            script = script.Replace("#inputs", inputs).Replace("#validations", validations).Replace("#scripts", scripts).Replace("#tName", table.name).Replace("#pColName", table.primery_column.name);


            StreamWriter fileCreate = new StreamWriter(filePathCreate, false, Encoding.UTF8);//(filepath);
            fileCreate.Write(create);
            fileCreate.Close();
            StreamWriter fileInputsCreate = new StreamWriter(filePathInputsCreate, false, Encoding.UTF8);//(filepath);
            fileInputsCreate.Write(inputsCreate);
            fileInputsCreate.Close();
            StreamWriter fileInputsUpdate = new StreamWriter(filePathInputsUpdate, false, Encoding.UTF8);//(filepath);
            fileInputsUpdate.Write(inputsUpdate);
            fileInputsUpdate.Close();

            //StreamWriter fileCreatePartial = new StreamWriter(filePathCreatePartial, false, Encoding.UTF8);//(filepath);
            //fileCreatePartial.Write(create);
            //fileCreatePartial.Close();

            StreamWriter fileScriptPartial = new StreamWriter(filePathScriptPartial, false, Encoding.UTF8);//(filepath);
            fileScriptPartial.Write(script);
            fileScriptPartial.Close();
        }
        private string GetInputs(string rowGroupFile)
        {
            string inputs = "";
            string tempRow = rowGroupFile;
            string tempInput = "";
            bool lastRow = true;
            int rowCount = 0;
            for (int i = 0; i < table.columns.Count(); i++)
            {
                if (table.columns[i].isPrimery)//primary key için input üretme.
                {
                    tempInput += GetInputText(table.columns[i].foreign, table.columns[i].name, inputs, table.columns[i].isForeign, ColumnType.Primery);
                    continue;
                }
                tempInput += GetInputText(table.columns[i].foreign,table.columns[i].name, inputs, table.columns[i].isForeign, table.columns[i].columntype);

                tempInput=table.columns[i].isNullable ? tempInput.Replace("#required", "") : tempInput.Replace("#required", "required");

                lastRow = true;
                if (rowCount % 2 == 1)//rowgroup şeklinde olduğu için 2 inputu 1 tag ile sarmalama işlemi yapılıyor. input sayısı tek ise lastrow ile son input tek elemanlı rowgroupta olacak.
                {
                    lastRow = false;
                    inputs += tempRow.Replace("#inputs", tempInput);
                    tempInput = "";
                    tempRow = rowGroupFile;
                }
                rowCount++;
            }
            if (lastRow)//tek elemanlı ise 
            {
                inputs += tempRow.Replace("#inputs", tempInput);
            }
            return inputs;
        }
        static int count = 1;//modelden bağımsız date alanı birden fazla olduğunda id farklılaştırmak için kullanıldı.
        private string GetInputText(ForeignKey foreign, string colName, string inputs, bool isForeign, ColumnType columnType)
        {
            string input = null;
            if (isForeign)
            {
                return file.inputDropdown.Replace("#colName", colName).Replace("#tName", foreign.table.name).Replace("#pageName", "Crt").Replace("#tName", table.name).Replace("#selectedId", "0");
            }
            switch (columnType)
            {
                case ColumnType.Primery:
                    input = file.inputHidden.Replace("#colName", colName).Replace("#tName", table.name);
                    break;
                case ColumnType.Number:
                    input = file.inputOnlyNumber.Replace("#colName", colName).Replace("#tName", table.name);
                    break;
                case ColumnType.ComboBox:
                    input = file.inputDropdown.Replace("#colName", colName).Replace("#tName", table.name).Replace("#selectedId", "0");
                    break;
                case ColumnType.Date://birden fazla date olduğu durumda her birisi için ayrı id, script ve validation üretiliyor.
                    if (!inputs.Contains("kt_datepicker"))
                    {
                        input = file.inputDate.Replace("#colName", colName).Replace("#tName", table.name);
                    }
                    else
                    {
                        input += file.inputDate.Replace("#colName", colName).Replace("#tName", table.name).Replace("kt_datepicker", "kt_datepicker_" + count);
                        count++;
                    }
                    break;
                case ColumnType.HtmlText:
                case ColumnType.TextArea:
                    input = file.inputTextArea.Replace("#colName", colName).Replace("#tName", table.name);
                    break;
                //case ColumnType.Image:
                //    input = file.inputImage.Replace("#colName", colName).Replace("#tName", table.name);
                //    break;
                case ColumnType.CheckBox:
                    input = file.inputCheckBox.Replace("#colName", colName).Replace("#tName", table.name);
                    break;
                case ColumnType.Email:
                    input = file.inputEmail.Replace("#colName", colName).Replace("#tName", table.name);
                    break;
                case ColumnType.Url:
                    input = file.inputLink.Replace("#colName", colName).Replace("#tName", table.name);
                    break;
                case ColumnType.Name:
                    input = file.inputDefault.Replace("#colName", colName).Replace("#tName", table.name);
                    break;
                //case ColumnType.File:
                //    input = file.inputFile.Replace("#colName", colName).Replace("#tName", table.name);
                //    break;
                case ColumnType.Text:
                    if (colName.Contains("phone"))
                    {
                        input = file.inputPhone.Replace("#colName", colName).Replace("#tName", table.name);
                    }
                    else if (colName.Contains("password"))
                    {
                        input = file.inputPassword.Replace("#colName", colName).Replace("#tName", table.name);
                    }
                    else if (colName.Contains("Id"))
                    {
                        input = file.inputDropdown.Replace("#colName", colName).Replace("#tName", table.name).Replace("#selectedId", "0");
                    }
                    else
                    {
                        input = file.inputDefault.Replace("#colName", colName).Replace("#tName", table.name);
                    }
                    break;
                default:
                    input = file.inputDefault.Replace("#colName", colName).Replace("#tName", table.name);
                    break;
            }
            return input.Replace("#pageName", "Crt");
        }
        private string GetValidations()
        {
            string validations = "";
            int count = 1;
            foreach (var col in table.columns)
            {
                if (!col.isNullable)
                {
                    switch (col.columntype)
                    {
                        case ColumnType.Primery:
                            break;
                        case ColumnType.Number:
                            validations += file.validationOnlyNumber.Replace("#colName", col.name);
                            break;
                        case ColumnType.HtmlText:
                        case ColumnType.TextArea:
                            validations += file.validationTextArea.Replace("#colName", col.name);
                            break;
                        case ColumnType.Email:
                            validations += file.validationEmail.Replace("#colName", col.name);
                            break;
                        case ColumnType.Url:
                            validations += file.validationLink.Replace("#colName", col.name);
                            break;
                        case ColumnType.Name:
                            validations += file.validationDefault.Replace("#colName", col.name);
                            break;
                        case ColumnType.File:
                            validations += file.validationDefault.Replace("#colName", col.name);
                            break;
                        case ColumnType.Date:
                            if (!validations.Contains("kt_datepicker"))
                            {
                                validations += file.validationDefault.Replace("#colName", col.name);
                            }
                            else
                            {
                                validations += file.validationDefault.Replace("#colName", col.name).Replace("kt_datepicker", "kt_datepicker_" + count);
                                count++;
                            }
                            break;
                        case ColumnType.Text:
                            if (col.name.Contains("phone"))
                            {
                                validations += file.validationPhone.Replace("#colName", col.name);
                            }
                            else
                            {
                                validations += file.validationDefault.Replace("#colName", col.name);
                            }
                            break;
                        default:
                            validations += file.validationDefault.Replace("#colName", col.name);
                            break;
                    }
                }
            }
            return validations.Replace("#pageName", "Crt").Replace("#tName", table.name);
        }
        private string GetSripts()
        {
            string scripts = "";
            int count = 1;
            foreach (var col in table.columns)
            {
                switch (col.columntype)
                {
                    case ColumnType.Primery:
                        break;
                    case ColumnType.Number:
                        if (col.name.Contains("phone") && !scripts.Contains("kt_inputmask_phone"))
                            scripts += file.scriptPhone;
                        break;
                    case ColumnType.Date:
                        if (!scripts.Contains("kt_datepicker"))
                            scripts += file.scriptDate;
                        else
                        {
                            scripts += file.scriptDate.Replace("kt_datepicker", "kt_datepicker_" + count);
                            count++;
                        }
                        break;
                    //case ColumnType.Image:
                    //    scripts += file.scriptImage;
                    //    break;
                    //case ColumnType.File:
                    //    scripts += file.scriptFile;
                    //    break;
                    case ColumnType.Email:
                        if (!scripts.Contains("kt_inputmask_email"))
                            scripts += file.scriptEmail;

                        break;
                    case ColumnType.Text:
                        if (col.name.Contains("phone") && !scripts.Contains("kt_inputmask_phone"))
                        {
                            scripts += file.scriptPhone;
                        }
                        break;
                }
            }
            return scripts;
        }
        private string GetSriptTags()
        {

            string scriptTags = "";
            foreach (var col in table.columns)
            {
                switch (col.columntype)
                {
                    case ColumnType.File:
                        scriptTags = file.scriptFile;
                        break;
                    case ColumnType.Text:
                        if (col.name.Contains("file") && !scriptTags.Contains("dropzonejs"))
                        {
                            scriptTags = file.scriptFile;
                        }
                        break;
                }
            }
            return scriptTags;
        }
    }
}
