﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;

namespace Avs.Win.Generate
{
    public class Update
    {
        private Table table = new Table();
        private string filepath = String.Empty;
        private FileUtility file = null;

        public Update(Table table, FileUtility file)
        {
            this.table = table;
            this.filepath = Constants.Views + "\\" + table.name + "\\" + "Update.cshtml";
            this.file = file;
        }

        public bool WriteUpdateFile()
        {
            try
            {
                FillUpdateClassContent();
                return true;
            }
            catch (Exception ex)
            {

            }
            return false;

        }
        private void FillUpdateClassContent()
        {
            string update = File.ReadAllText("WebReport//Views//Update.txt", Encoding.GetEncoding("iso-8859-9"));
            string rowGroupsFile = File.ReadAllText("WebReport//Views//RowGroup.txt", Encoding.GetEncoding("iso-8859-9"));
            string inputs = GetInputs(rowGroupsFile);


            update = update.Replace("#tName", table.name).Replace("#inputs", inputs).Replace("#pKey", table.primery_column.name);

            StreamWriter file = new StreamWriter(filepath, false, Encoding.UTF8);//(filepath);
            file.Write(update);
            file.Close();
        }
        private string GetInputs(string rowGroupFile)
        {
            string inputs = "";
            string tempRow = rowGroupFile;
            string tempInput = "";
            bool lastRow = true;
            int rowCount = 0;
            for (int i = 0; i < table.columns.Count(); i++)
            {
                if (table.columns[i].isPrimery)//primary key için input üretme.
                {
                    tempInput += GetInputText(table.columns[i].foreign, table.columns[i].name, inputs, table.columns[i].isForeign, ColumnType.Primery);
                    continue;
                }
                tempInput += GetInputText(table.columns[i].foreign, table.columns[i].name, inputs, table.columns[i].isForeign, table.columns[i].columntype);
                tempInput = table.columns[i].isNullable ? tempInput.Replace("#required", "") : tempInput.Replace("#required", "required");
                lastRow = true;
                if (rowCount % 2 == 1)//rowgroup şeklinde olduğu için 2 inputu 1 tag ile sarmalama işlemi yapılıyor. input sayısı tek ise lastrow ile son input tek elemanlı rowgroupta olacak.
                {
                    lastRow = false;
                    inputs += tempRow.Replace("#inputs", tempInput);
                    tempInput = "";
                    tempRow = rowGroupFile;
                }
                rowCount++;
            }
            if (lastRow)//tek elemanlı ise 
            {
                inputs += tempRow.Replace("#inputs", tempInput);
            }
            return inputs;
        }
        static int count = 1;//modelden bağımsız date alanı birden fazla olduğunda id farklılaştırmak için kullanıldı.
        private string GetInputText(ForeignKey foreign, string colName, string inputs, bool isForeign, ColumnType columnType)
        {
            string input = null;
            if (isForeign)
            {
                return file.inputDropdown.Replace("#selectedId", "Model._#tNameModel.#colName").Replace("#colName", colName).Replace("#tForName", foreign.table.name).Replace("#pageName", "Upd").Replace("#tName", table.name);
            }
            switch (columnType)
            {
                case ColumnType.Primery:
                    input = file.inputHidden.Replace("#colName", colName).Replace("#tName", table.name);
                    break;
                case ColumnType.Number:
                    input = file.inputOnlyNumber.Replace("#colName", colName).Replace("#tName", table.name);
                    break;
                case ColumnType.ComboBox:
                    input = file.inputDropdown.Replace("#selectedId", "Model._#tNameModel.#colName").Replace("#colName", colName).Replace("#tName", table.name);
                    break;
                case ColumnType.Date://birden fazla date olduğu durumda her birisi için ayrı id, script ve validation üretiliyor.
                    if (!inputs.Contains("kt_datepicker"))
                    {
                        input = file.inputDate.Replace("#colName", colName).Replace("#tName", table.name);
                    }
                    else
                    {
                        input += file.inputDate.Replace("#colName", colName).Replace("#tName", table.name).Replace("kt_datepicker", "kt_datepicker_" + count);
                        count++;
                    }
                    break;
                case ColumnType.HtmlText:
                case ColumnType.TextArea:
                    input = file.inputTextArea.Replace("#colName", colName).Replace("#tName", table.name);
                    break;
                //case ColumnType.Image:
                //    input = file.inputImage.Replace("#colName", colName).Replace("#tName", table.name);
                //    break;
                case ColumnType.CheckBox:
                    input = file.inputCheckBox.Replace("#colName", colName).Replace("#tName", table.name);
                    break;
                case ColumnType.Email:
                    input = file.inputEmail.Replace("#colName", colName).Replace("#tName", table.name);
                    break;
                case ColumnType.Url:
                    input = file.inputLink.Replace("#colName", colName).Replace("#tName", table.name);
                    break;
                case ColumnType.Name:
                    input = file.inputDefault.Replace("#colName", colName).Replace("#tName", table.name);
                    break;
                //case ColumnType.File:
                //    input = file.inputFile.Replace("#colName", colName).Replace("#tName", table.name);
                //    break;
                case ColumnType.Text:
                    if (colName.Contains("phone"))
                    {
                        input = file.inputPhone.Replace("#colName", colName).Replace("#tName", table.name);
                    }
                    else if (colName.Contains("password"))
                    {
                        input = file.inputPassword.Replace("#colName", colName).Replace("#tName", table.name);
                    }
                    else if (colName.Contains("Id"))
                    {
                        input = file.inputDropdown.Replace("#selectedId", "Model._#tNameModel.#colName").Replace("#colName", colName).Replace("#tName", table.name);
                    }
                    else
                    {
                        input = file.inputDefault.Replace("#colName", colName).Replace("#tName", table.name);
                    }
                    break;
                default:
                    input = file.inputDefault.Replace("#colName", colName).Replace("#tName", table.name);
                    break;
            }
            return input.Replace("#pageName", "Upd");
        }
    }
}
