﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;

namespace Avs.Win.Generate
{
    public class DetailPartials
    {
        private Table table = new Table();
        private string rowfilepath = String.Empty;
        private string columnfilepath = String.Empty;
        private string tablefilepath = String.Empty;

        public DetailPartials(Table table)
        {
            this.table = table;
            this.rowfilepath = Constants.Views + "\\" + table.name.Remove(table.name.Length - 2) + "\\" + "_DetailRowListPartial.cshtml";
            this.columnfilepath = Constants.Views + "\\" + table.name.Remove(table.name.Length - 2) + "\\" + "_DetailColumnListPartial.cshtml";
            this.tablefilepath = Constants.Views + "\\" + table.name.Remove(table.name.Length - 2) + "\\" + "_DetailTableListPartial.cshtml";
        }

        public bool WriteDetailPartialsFile()
        {
            try
            {
                FillDetailPartialsClassContent();
                return true;
            }
            catch (Exception ex)
            {

            }
            return false;

        }
        private void FillDetailPartialsClassContent()
        {
            string rowList = File.ReadAllText("WebReport//Views//DetailRowListPartial.txt", Encoding.GetEncoding("iso-8859-9"));
            StreamWriter row = new StreamWriter(rowfilepath, false, Encoding.UTF8);//(filepath);
            rowList = rowList.Replace("#tName", table.name).Replace("#tPureName",table.name.Remove(table.name.Length-2));
            row.Write(rowList);
            row.Close();

            string columnList = File.ReadAllText("WebReport//Views//DetailColumnListPartial.txt", Encoding.GetEncoding("iso-8859-9"));
            StreamWriter column = new StreamWriter(columnfilepath, false, Encoding.UTF8);//(filepath);
            columnList = columnList.Replace("#tName", table.name).Replace("#tPureName", table.name.Remove(table.name.Length - 2));
            column.Write(columnList);
            column.Close();

            string tableList = File.ReadAllText("WebReport//Views//DetailTableListPartial.txt", Encoding.GetEncoding("iso-8859-9"));
            StreamWriter tableSw = new StreamWriter(tablefilepath, false, Encoding.UTF8);//(filepath);
            tableList = tableList.Replace("#tName", table.name).Replace("#tPureName", table.name.Remove(table.name.Length - 2));
            tableSw.Write(tableList);
            tableSw.Close();
        }
    }
}
