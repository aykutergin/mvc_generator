﻿using System;
using System.Data;
using System.Configuration;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System.Xml;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using Avs.Win.Generate;
using System.Data.Entity.Design.PluralizationServices;
using System.Globalization;

/// <summary>
/// Summary description for tools
/// </summary>
public static class tools
{
    #region SQL DATA FUNCTIONS
    public static DataTable GetTableRows(string TableName)
    {
        string sorgu = "SELECT table_name=sysobjects.name,column_name=syscolumns.name,datatype=systypes.name,length=syscolumns.length,ident=syscolumns.status,isnullable=syscolumns.isnullable,ex.value FROM sysobjects JOIN syscolumns ON sysobjects.id = syscolumns.id JOIN systypes ON syscolumns.xtype=systypes.xtype LEFT JOIN sys.extended_properties ex ON  ex.major_id=syscolumns.id AND ex.minor_id=syscolumns.colid  AND ex.name = 'MS_Description' WHERE (sysobjects.xtype='U' or sysobjects.xtype='V') and sysobjects.name='" + TableName + "' and systypes.name!='sysname' ORDER BY sysobjects.name,syscolumns.colid";
        Sql sql = new Sql(sorgu);
        return sql.select();
    }

    public static DataTable GetTableRow(string TableName, string columnName)
    {
        string sorgu = "SELECT table_name=sysobjects.name,column_name=syscolumns.name,datatype=systypes.name,length=syscolumns.length,ident=syscolumns.status,isnullable=syscolumns.isnullable,ex.value FROM sysobjects JOIN syscolumns ON sysobjects.id = syscolumns.id JOIN systypes ON syscolumns.xtype=systypes.xtype LEFT JOIN sys.extended_properties ex ON  ex.major_id=syscolumns.id AND ex.minor_id=syscolumns.colid  AND ex.name = 'MS_Description' WHERE (sysobjects.xtype='U' or sysobjects.xtype='V') and syscolumns.name='" + columnName + "' and sysobjects.name='" + TableName + "' and systypes.name!='sysname' ORDER BY sysobjects.name,syscolumns.colid";
        Sql sql = new Sql(sorgu);
        return sql.select();
    }

    public static DataTable GetPrimaryKeys(string TableName)
    {
        string sorgu = "SELECT Col.Column_Name from INFORMATION_SCHEMA.TABLE_CONSTRAINTS Tab, INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE Col WHERE Col.Constraint_Name = Tab.Constraint_Name AND Col.Table_Name = Tab.Table_Name AND Constraint_Type = 'PRIMARY KEY ' AND Col.Table_Name = '" + TableName + "'";
        Sql sql = new Sql(sorgu);
        return sql.select();
    }

    public static DataTable GetForeignKeys(string TableName)
    {
        string sorgu = "SELECT Col.Column_Name from INFORMATION_SCHEMA.TABLE_CONSTRAINTS Tab, INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE Col WHERE Col.Constraint_Name = Tab.Constraint_Name AND Col.Table_Name = Tab.Table_Name AND Constraint_Type = 'FOREIGN KEY ' AND Col.Table_Name = '" + TableName + "'";
        Sql sql = new Sql(sorgu);
        return sql.select();
    }

    internal static void CreateAction(Table table, int parentId)
    {
        //action tablosunda yoksa ekle
        string Controller ="A"+ table.name;
        string action = "";
        string desc = table.primery_column.description;
        for (int i = 0; i < 2; i++)
        {
            action = (i == 0) ? "Index" : "Create";
            desc += (i == 0) ? "" : "Yeni Kayıt";
            string s = String.Format("select * from ActMenu where Controller='{0}' and ActionVal='{1}'", Controller, action);
      
            Sql sql = new Sql(s);
            DataTable dts = sql.select();
            if (!(dts != null && dts.Rows.Count > 0))
            {
                string c = String.Format("insert into ActMenu(ActionVal,Controller,ActionName,parentId) values('{0}','{1}','{2}',{3})", action, Controller, desc, parentId);
                Sql sqlc = new Sql(c);
                int aid = Convert.ToInt32(sqlc.InsertReturnIdentity());

                string x = String.Format("insert into ActionUserRole(MenuId,RoleId,EntityId) values({0},{1},{2})", aid, 1, 1);
                Sql sqlx = new Sql(x);
                sqlx.Insert();

            }
            else if(dts != null) {
                string u = String.Format("update ActMenu set ActionName = '{0}' where MenuId={1}", desc,Convert.ToInt32(dts.Rows[0]["MenuId"]));
                Sql sqlu = new Sql(u);
                sqlu.Insert();
            }
        }
       
        
    }

    internal static void IsExistCreateDirectory(object apiClientFolder)
    {
        throw new NotImplementedException();
    }

    public static DataTable getForeginTable()
    {
        string sorgu = "SELECT f.name AS ForeignKey,SCHEMA_NAME(f.SCHEMA_ID) SchemaName, OBJECT_NAME(f.parent_object_id) AS TableName, COL_NAME(fc.parent_object_id,fc.parent_column_id) AS ColumnName, SCHEMA_NAME(o.SCHEMA_ID) ReferenceSchemaName, OBJECT_NAME (f.referenced_object_id) AS ReferenceTableName, COL_NAME(fc.referenced_object_id,fc.referenced_column_id) AS ReferenceColumnName FROM sys.foreign_keys AS f INNER JOIN sys.foreign_key_columns AS fc ON f.OBJECT_ID = fc.constraint_object_id INNER JOIN sys.objects AS o ON o.OBJECT_ID = fc.referenced_object_id";
        Sql sql = new Sql(sorgu);
        return sql.select();
    }

    #endregion


    #region UTILITY FUNCTIONS

    public static void IsExistCreateDirectory(string FolderPath)
    {
      if(!Directory.Exists(FolderPath))
           Directory.CreateDirectory(FolderPath);
    }

    public static string convertSqlDataType(string str)
    {
        if (str.ToLower() == "int" || str.ToLower() == "smallint" || str.ToLower() == "tinyint")
        {
            str = "int";
        }
        else if (str.ToLower() == "numeric")
        {
            str = "decimal";
        }
        else if (str.ToLower() == "bigint")
        {
            str = "long";
        }
        else if (str.ToLower() == "binary" || str.ToLower() == "varbinary" || str.ToLower() == "image")
        {
            str = "byte[]";
        }
        else if (str.ToLower() == "decimal" || str.ToLower() == "money" || str.ToLower() == "smallmoney")
        {
            str = "decimal";
        }
        else if (str.ToLower() == "bit")
        {
            str = "bool";
        }
        else if (str.ToLower() == "char")
        {
            str = "char";
        }
        else if (str.ToLower() == "date" || str.ToLower() == "datetime" )
        {
            str = "DateTime";
        }
        else if (str.ToLower() == "time" || str.ToLower() == "timestamp")
        {
            str = "TimeSpan";
        }
        else
        {
            str = "string";
        }
        return str;

    }

    public static string convertSqlDataType(string str, int isnullable, string tableName, string columnName)
    {
        if (str.ToLower() == "int" || str.ToLower() == "smallint" || str.ToLower() == "tinyint")
        {
            str = "int";
        }
        else if (str.ToLower() == "numeric")
        {
            str = "decimal";
        }
        else if (str.ToLower() == "bigint")
        {
            str = "long";
        }
        else if (str.ToLower() == "binary" || str.ToLower() == "varbinary" || str.ToLower() == "image")
        {
            str = "byte[]";
        }
        else if (str.ToLower() == "decimal" || str.ToLower() == "money" || str.ToLower() == "smallmoney")
        {
            str = "decimal";
        }
        else if (str.ToLower() == "bit")
        {
            str = "bool";
        }
        else if (str.ToLower() == "char")
        {
            str = "char";
        }
        else if (str.ToLower() == "date" || str.ToLower() == "datetime")
        {
            str = "DateTime";
        }
        else if (str.ToLower() == "time" || str.ToLower() == "timestamp")
        {
            str = "TimeSpan";
        }

        else
        {
            str = "string";
        }
        if (Constants.IsNull == true)
        {
            //if (isnullable == 1 && IsForeignKey(GetForeignKeys(tableName), columnName) == false)
            if (isnullable == 1)
            {
                if (str == "byte[]") str = "byte[]";
                else if (str != "string") str = str + "?";
            }
        }

        return str;

    }

    public static string convertSqlDbType(string str)
    {

        switch (str.ToLower())
        {

            case "numeric":
                str = "DbType.Decimal";
                break;
            case "int":
                str = "DbType.Int32";
                break;
            case "smallint":
                str = "DbType.Int16";
                break;
            case "tinyint":
                str = "DbType.Int16";
                break;
            case "bigint":
                str = "DbType.Int64";
                break;
            case "binary":
                str = "DbType.Binary";
                break;
            case "varbinary":
                str = "DbType.Binary";
                break;
            case "image":
                str = "DbType.Binary";
                break;
            case "decimal":
                str = "DbType.Decimal";
                break;
            case "money":
                str = "DbType.Decimal";
                break;
            case "smallmoney":
                str = "DbType.Decimal";
                break;
            case "bit":
                str = "DbType.Boolean";
                break;
            case "char":
                str = "DbType.String";
                break;
            case "date":
                str = "DbType.Date";
                break;
            case "datetime":
                str = "DbType.DateTime";
                break;
            case "time":
                str = "DbType.Time";
                break;
            case "timestamp":
                str = "DbType.Time";
                break;
            case "nvarchar":
                str = "DbType.String";
                break;
            default:
                str = "DbType.String";
                break;
        }
        return str;

    }

    public static bool IsIdentity(string status)
    {
        if (status.Trim() == "128")
        {
            return true;
        }
        else return false;
    }

    public static bool IsPrimeryKey(DataTable dt_primerykey, string ColumnName)
    {
        foreach (DataRow dr in dt_primerykey.Rows)
        {
            if (ColumnName == dr["Column_Name"].ToString())
            {
                return true;
            }
        }
        return false;
    }

    public static bool IsForeignKey(DataTable dt_foreignkey, string ColumnName)
    {
        foreach (DataRow dr in dt_foreignkey.Rows)
        {
            if (ColumnName == dr["Column_Name"].ToString())
            {
                return true;
            }
        }
        return false;
    }


    public static string GetPlural(string tableName) {
        CultureInfo ci = new CultureInfo("en-us");
        PluralizationService ps =  PluralizationService.CreateService(ci);
        if (ps.IsSingular(tableName) == true)
        {
            return ps.Pluralize(tableName);
        }
        else return tableName;
    }

    public static string GetSingular(string tableName)
    {
        CultureInfo ci = new CultureInfo("en-us");
        PluralizationService ps = PluralizationService.CreateService(ci);
        if (ps.IsPlural(tableName) == true)
        {
            return ps.Singularize(tableName);
        }
        else return tableName;
    }

    //public static string CheckTableName(Table table)
    //{
    //    int index = table.name.Length - 1;
    //    int indexto = table.name.Length - 2;
    //    if (table.name.Substring(indexto) != "ss" && table.name.Substring(index) == "s")
    //    {
    //        table.name = table.name.Substring(0, index);
    //    }

    //    return table.name;
    //}

    //public static string GetPulirze(string tablename) {
    //    int index = tablename.Length - 1;
    //    int indexto= tablename.Length - 2;
    //    string dbpulirze = "";
    //    if(tablename.Substring(indexto) == "ey" || tablename.Substring(indexto) == "eo")
    //        dbpulirze = "db." + tablename + "s";
    //    else if (tablename.Substring(index) == "y")
    //        dbpulirze = "db." + tablename.Substring(0, index) + "ies";
    //    else if (tablename.Substring(index) == "o" || tablename.Substring(indexto) == "ss" || tablename.Substring(index) == "x")
    //        dbpulirze = "db." + tablename + "es";
    //    else
    //        dbpulirze = "db." + tablename + "s";

    //    return dbpulirze;
    //}
    //public static string GetPulirzeSingle(string tablename)
    //{
    //    int index = tablename.Length - 1;
    //    int indexto = tablename.Length - 2;
    //    string dbpulirze = "";
    //    if (tablename.Substring(indexto) == "ey")
    //        dbpulirze = "" + tablename + "s";
    //    else if (tablename.Substring(index) == "y")
    //        dbpulirze = "" + tablename.Substring(0, index) + "ies";
    //    else if (tablename.Substring(index) == "o" || tablename.Substring(indexto) == "ss" || tablename.Substring(index) == "x")
    //        dbpulirze = "" + tablename + "es";
    //    else
    //        dbpulirze = "" + tablename + "s";

    //    return dbpulirze;
    //}

    public static string GetLoopItem(string str, int loopNum) {
        int l1_start = str.IndexOf("<!--s"+loopNum+"-->") + 9;
        int l2_end = str.IndexOf("<!--e" + loopNum + "-->");
        int count = l2_end - l1_start;
        string loop = str.Substring(l1_start, count);
        return loop;
    }
    #endregion

}