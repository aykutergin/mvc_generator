﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;

namespace Avs.Win.Generate
{
    public class ReportSourceVwTransfer
    {
        private Table table = new Table();
        private string filepath = String.Empty;
        private string filepathget = String.Empty;

        public ReportSourceVwTransfer(Table table)
        {
            this.table = table;
            this.filepath = Constants.ReportSourceVwTransferFolder + "\\" + table.name + "Transfer.cs";
            this.filepathget = Constants.ReportSourceVwTransferFolder + "\\" + table.name + "Transfer.cs";
        }

        public bool WriteReportSourceVwTransferFile()
        {
            try
            {
                FillReportSourceVwTransferClassContent();
                return true;
            }
            catch (Exception)
            {

            }
            return false;

        }
        public bool WriteGetReportSourceVwTransferFile()
        {
            try
            {
                FillGetReportSourceVwTransferClassContent();
                return true;
            }
            catch (Exception)
            {

            }
            return false;

        }
        private void FillGetReportSourceVwTransferClassContent()
        {
            string str = File.ReadAllText("WebReport//SourceVwTransfer.txt", Encoding.GetEncoding("iso-8859-9"));
            StreamWriter file = new StreamWriter(filepathget, false, Encoding.UTF8);//(filepath);
            str = str.Replace("#tName", table.name).Replace("#modelToDto", GetObjContent("model")).Replace("#dtoToModel", GetObjContent("dto"));
            file.Write(str);
            file.Close();
        }
        private void FillReportSourceVwTransferClassContent()
        {
            string str = File.ReadAllText("WebReport//SourceVwTransfer.txt", Encoding.GetEncoding("iso-8859-9"));
            StreamWriter file = new StreamWriter(filepath, false, Encoding.UTF8);//(filepath);
            str = str.Replace("#tName", table.name).Replace("#modelToDto", GetObjContent("model")).Replace("#dtoToModel", GetObjContent("dto"));
            file.Write(str);
            file.Close();
        }
        private string GetObjContent(string objName)
        {
            string context = "";
            foreach (var column in table.columns)
            {
                context += column.name + " = " + objName + "." + column.name + ",\n\t\t\t\t";
            }
            return context.Remove(context.Length-6);
        }

    }
}
