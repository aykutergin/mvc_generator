﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Avs.Win.Generate
{
    public class Constants
    {
        public static bool IsNull = false;
        public static bool IsBusiness = false;
        public static bool IsEntity = false;
        public static bool IsDto = false;
        public static bool IsOpClass = false;
        public static bool IsController = false;
        public static bool IsLayout = false;
        public static bool IsHomePage = false;
        public static string rootPath = String.Empty;
        public static string BusinessFolder = String.Empty;
        public static string EntityFolder = String.Empty;
        public static string ModelsFolder = String.Empty;
        public static string OpModelFolder = String.Empty;
        public static string OpFilterFolder = String.Empty;
        public static string ApiServiceFolder = String.Empty;
        public static string ApiControllerFolder = String.Empty;
        public static string DocFolder = String.Empty;
        public static string OpSelectFolder = String.Empty;
        public static string ProjectOperationClassesFolder = String.Empty;
        public static string ControllerFolder = String.Empty;
        public static string ViewFolder = String.Empty;
        public static string ViewTableFolder = String.Empty;
        public static string SqlViewFolder = String.Empty;
        public static string SqlForeignFolder = String.Empty;
        public static string EntityContextName = "Avs3Entities";
        public static string CrudsFolder = String.Empty;

        public static _Menu mainMenu = new _Menu("Genel İşlemler", "", "");
        public static List<_Menu> refMenuList = new List<_Menu>();
        public static MenuManager menumanager = new MenuManager();

        public static string ent_ns = String.Empty;
        public static string dto_ns = String.Empty;
        public static string obj_ns = String.Empty;
        public static string api_ns = String.Empty;
        public static string client_ns = String.Empty;
        public static string rest_ns = String.Empty;
        public static string utility_ns = String.Empty;

        public static string nsitem_company = "Dcl";
        public static string nsitem_solution = "Avs3";
        public static string nsitem_proje = "Mono";

        public static string ApiClientFolder = String.Empty;
        public static string ReportISourceFolder = String.Empty;
        public static string ReportGetISourceFolder = String.Empty;
        public static string ReportSourceFolder = String.Empty;
        public static string ReportGetSourceFolder = String.Empty;
        public static string ReportServiceFolder = String.Empty;
        public static string ReportControllerFolder = String.Empty;
        public static string ReportSourceFactoryFolder = String.Empty;
        public static string ReportSourceTableTransferFolder = String.Empty;
        public static string ReportSourceVwTransferFolder = String.Empty;
        public static string ReportModelFolder = String.Empty;
        public static string ReportVwModelFolder = String.Empty;
        public static string ReportFilterModelFolder = String.Empty;
        public static string ReportViewModelsFolder = String.Empty;
        public static string MonoClientFolder = String.Empty;
        public static string MonoDtoFolder = String.Empty;
        public static string ReportValidationModelFolder = String.Empty;
        public static string DropdownControllerFolder = String.Empty;
        public static string DropdownServiceFolder = String.Empty;

        public static string Views = String.Empty;

        public static string ProjEntityFolder = String.Empty;
        public static string ProjDtoObjFolder = String.Empty;
        public static string ProjDtoFolder = String.Empty;
        public static string ProjApiFolder = String.Empty;
        public static string ProjClientFolder = String.Empty;
        

    }
}
