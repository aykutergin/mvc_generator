﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;

namespace Avs.Win.Generate
{
    public class ProjectOperationClasses
    {
        private Table table;
        private string filepath=String.Empty;
        public ProjectOperationClasses(Table table)
        {
            this.table = table;
            this.filepath = Constants.ProjectOperationClassesFolder + "\\" + table.name + ".cs";
        }
        public bool WriteProjectOperationClassesFile()
        {
            try
            {
                FillProjectOperationClassesClassContent();
                return true;
            }
            catch (Exception)
            {
                
            }
            return false;
            
        }
        private void FillProjectOperationClassesClassContent()
        {
            StreamWriter file = new StreamWriter(filepath,false, Encoding.UTF8);//(filepath);

            #region namespace ler
            file.WriteLine("using Avs.Lib.LocalSvr.Entity;");
            file.WriteLine("using System.Collections.Generic;");
            file.WriteLine("using System.Data.Entity;");
            file.WriteLine("using System.Linq;");
           
            file.WriteLine("namespace Avs.Web.MgmtReport.Models");
            file.WriteLine("{");
            file.WriteLine("public partial class " + table.name+"FormModel");
            file.WriteLine("{");

            //
            file.WriteLine("public " + table.name + " _" + table.name + " { get; set; }");

            foreach (ForeignKey foreign in table.foreigns)
            {
                file.WriteLine("public List<"+foreign.table.name+ "> _" + foreign.table.name + "List { get; set; }");
            }
            
            file.WriteLine("public void FillModel(" + table.primery_column.dataType + " " + table.primery_column.name + ")");
            file.WriteLine("{");
            file.WriteLine( table.name+ "Factory _" + table.name + "Factory = new " + table.name + "Factory();");
            file.WriteLine("this._" + table.name + " = _" + table.name + "Factory.Get" + table.name + "("+table.primery_column.name+");");
            file.WriteLine("}");


            file.WriteLine("public void FillSelectList()");
            file.WriteLine("{");
            foreach (ForeignKey foreign in table.foreigns)
            {
                file.WriteLine(""+foreign.table.name+ "Factory _"+foreign.table.name+ "Factory = new "+foreign.table.name+ "Factory();");
                file.WriteLine("_"+foreign.table.name+ "List = _"+foreign.table.name+ "Factory.Get"+foreign.table.name+ "();");
            }
            file.WriteLine("}");

            file.WriteLine("}");//end Form container

            file.WriteLine("");

            file.WriteLine("public partial class " + table.name + "ViewModel");
            file.WriteLine("{");

            //
            file.WriteLine("public List<" + table.name + "> _" + table.name + "List { get; set; }");

            foreach (ForeignKey foreign in table.foreigns)
            {
                file.WriteLine("public List<" + foreign.table.name + "> _" + foreign.table.name + "List { get; set; }");
            }

            file.WriteLine("public void FillModel()");
            file.WriteLine("{");
            file.WriteLine(table.name + "Factory _" + table.name + "Factory = new " + table.name + "Factory();");
            file.WriteLine("this._" + table.name + "List = _" + table.name + "Factory.Get" + table.name + "();");
            file.WriteLine("}");


            file.WriteLine("public void FillSelectList()");
            file.WriteLine("{");
            foreach (ForeignKey foreign in table.foreigns)
            {
                file.WriteLine("" + foreign.table.name + "Factory _" + foreign.table.name + "Factory = new " + foreign.table.name + "Factory();");
                file.WriteLine("_" + foreign.table.name + "List = _" + foreign.table.name + "Factory.Get" + foreign.table.name + "();");
            }
            file.WriteLine("}");

            file.WriteLine("}");//end View container



            file.WriteLine(" public partial class " + table.name + "Factory");
            file.WriteLine(" {");

            file.WriteLine("public " + table.name + " Get" + table.name + "(" + table.primery_column.dataType + " " + table.primery_column.name + ")");
                file.WriteLine("{");
                    file.WriteLine("using (" + Constants.EntityContextName + " context = new " + Constants.EntityContextName + "())");
                    file.WriteLine("{");
                    file.WriteLine(" var _" + table.name + " = (from a in context." + tools.GetPlural(table.name) + " where a." + table.primery_column.name + " == " + table.primery_column.name + " select a).SingleOrDefault<" + table.name + ">();");
                    file.WriteLine("return _" + table.name + ";");
                    file.WriteLine("}");
                file.WriteLine("}");


            file.WriteLine("public List<" + table.name + "> Get" + table.name + "()");
            file.WriteLine("{");
            file.WriteLine("using ("+Constants.EntityContextName+ " context = new " + Constants.EntityContextName + "())");
                file.WriteLine("{");
                file.WriteLine("context.Configuration.LazyLoadingEnabled = false;");
                file.WriteLine("var _" + table.name + " = (from a in context." + tools.GetPlural(table.name) + " select a)");
                foreach (ForeignKey foreign in table.foreigns)
                {
                    file.WriteLine(".Include(m => m." + foreign.table.name + ")");
                }
                file.WriteLine(".ToList();");

                file.WriteLine(" return _" + table.name + ";");
                file.WriteLine("}");
            file.WriteLine("}");

          file.WriteLine("}");//end factory class


            file.WriteLine("}");//end namespace


            #endregion

    
            file.Close();
        }

     
    }
}
