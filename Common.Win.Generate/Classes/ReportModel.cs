﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;

namespace Avs.Win.Generate
{
    public class ReportModel
    {
        private Table table = new Table();
        private string filepath = String.Empty;
        private string filepathGet = String.Empty;

        public ReportModel(Table table)
        {
            this.table = table;
            this.filepath = Constants.ReportModelFolder + "\\" + table.name + "Model.cs";
            this.filepathGet = Constants.ReportVwModelFolder + "\\" + table.name + "Model.cs";
        }

        public bool WriteModelFile()
        {
            try
            {
                FillModelClassContent();
                return true;
            }
            catch (Exception)
            {

            }
            return false;

        }
        public bool WriteGetModelFile()
        {
            try
            {
                FillGetModelClassContent();
                return true;
            }
            catch (Exception)
            {

            }
            return false;

        }
        private void FillGetModelClassContent()
        {
            string str = File.ReadAllText("WebReport//Model.txt", Encoding.GetEncoding("iso-8859-9"));
            StreamWriter file = new StreamWriter(filepathGet, false, Encoding.UTF8);//(filepath);
            str = str.Replace("#tName", table.name).Replace("#modelContent", GetModelContent()).Replace("#nameSpace", "ApiGet");
            file.Write(str);
            file.Close();
        }
        private void FillModelClassContent()
        {
            string str = File.ReadAllText("WebReport//Model.txt", Encoding.GetEncoding("iso-8859-9"));
            StreamWriter file = new StreamWriter(filepath, false, Encoding.UTF8);//(filepath);
            str = str.Replace("#tName", table.name).Replace("#modelContent", GetModelContent()).Replace("#nameSpace", "ApiCrud");
            file.Write(str);
            file.Close();
        }
        private string GetModelContent()
        {
            string context = "";
            foreach (var column in table.columns)
            {
                context += "public " + column.dataType + " " + column.name + " { get; set; } \n\t\t";
            }
            foreach (var column in table.foreigns)
            {
                context += "public " + column.table.name + "Model _" + column.table.name + " { get; set; } \n\t\t";
            }
            foreach (var column in table.ref_foreign)
            {
                context += "public List<" + column.table.name + "Model> " + column.table.name + "list { get; set; } \n\t\t";
            }
            return !string.IsNullOrEmpty(context) ? context.Remove(context.Length - 3) : "";
        }
    }
}
