﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Avs.Win.Generate.Classes
{
    class ViewCreator
    {
        private Table table = new Table();
        private string filepath = String.Empty;
        private bool isTable = true;

        public ViewCreator(Table table, bool isTable)
        {
            this.table = table;
            // this.filepath = Constants.SqlViewFolder + "\\VW_" + table.name + ".sql";

            this.filepath = Constants.SqlViewFolder + "\\VW_All.sql";
            this.isTable = isTable;

        }

        public bool WriteSqlFile()
        {
            try
            {
                FillSqlContent();
                return true;
            }
            catch (Exception)
            {

            }
            return false;

        }
        private void FillSqlContent()
        {

            //StreamWriter file = new StreamWriter(filepath,true, Encoding.UTF8);//(filepath,append:true);
            //int asref = 1;
            //file.WriteLine("CREATE VIEW " + table.name + "Vw AS");
            //file.WriteLine("Select " + table.GetSqlColumns());
            //foreach (var item in table.foreigns)
            //{
            //    foreach (var subitem in item.table.columns)
            //    {
            //        file.WriteLine(",t" + asref + "." + subitem.name + " as " + item.table.name + "_" + subitem.name);
            //    }
            //    asref++;
            //}

            //file.WriteLine(" FROM " + table.name + " t");
            //asref = 1;
            //foreach (var item in table.foreigns)
            //{
            //    file.WriteLine("INNER JOIN " + item.table.name + " t"+asref+" ON t." + item.column.name + " = t" + asref + "." + item.table.primery_column.name);
            //     asref++;
            //}

            //file.WriteLine("");
            //file.WriteLine("");
            //file.WriteLine("GO");
            //file.WriteLine("");
            //file.WriteLine("");
            //file.Close();




            StreamWriter file = new StreamWriter(filepath, true, Encoding.UTF8);//(filepath,append:true);
            bool first = true;

            file.WriteLine("DROP VIEW " + table.name + "Vw");
            file.WriteLine("");
            file.WriteLine("");
            file.WriteLine("GO");
            file.WriteLine("");
            file.WriteLine("");


            file.WriteLine("CREATE VIEW " + table.name + "Vw AS");
            file.WriteLine("Select ");
            foreach (var col in table.columns)
            {
                if (first) file.WriteLine(table.name + "." + col.name);
                else file.WriteLine("," + table.name + "." + col.name);
                first = false;
            }
            //if (table.foreigns.Count > 0)
            //{
            //    foreach (var item in table.foreigns)
            //    {
            //        foreach (var subitem in item.table.columns)
            //        {
            //            file.WriteLine("," + item.table.name + "." + subitem.name + " as " + item.table.name + "_" + subitem.name);
            //        }
            //    }

            //}

            file.WriteLine(" FROM " + table.name);

            //if (table.foreigns.Count > 0)
            //{
            //    foreach (var item in table.foreigns)
            //    {
            //        file.WriteLine("INNER JOIN " + item.table.name + " ON " + table.name + "." + item.column.name + " = " + item.table.name + "." + item.table.primery_column.name);
            //    }
            //}
            file.WriteLine("");
            file.WriteLine("");
            file.WriteLine("GO");
            file.WriteLine("");
            file.WriteLine("");
            file.Close();

        }

    }
}
