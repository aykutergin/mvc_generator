﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;

namespace Avs.Win.Generate
{
    public class ReportService
    {
        private Table table = new Table();
        private string filepath = String.Empty;

        public ReportService(Table table)
        {
            this.table = table;
            this.filepath = Constants.ReportServiceFolder + "\\" + table.name + "Service.cs";
        }

        public bool WriteReportServiceFile()
        {
            try
            {
                FillReportServiceClassContent();
                return true;
            }
            catch (Exception ex)
            {

            }
            return false;

        }
        public bool WriteGetReportServiceFile()
        {
            try
            {
                FillGetReportServiceClassContent();
                return true;
            }
            catch (Exception ex)
            {

            }
            return false;

        }
        private void FillGetReportServiceClassContent()
        {
            string str = File.ReadAllText("WebReport//GetService.txt", Encoding.GetEncoding("iso-8859-9"));
            StreamWriter file = new StreamWriter(filepath, false, Encoding.UTF8);//(filepath);
            str = str.Replace("#tName", table.name).Replace("#pColType", table.primery_column.dataType).Replace("#pColName", table.primery_column.name).Replace("#colName", GetName());
            file.Write(str);
            file.Close();
        }
        private void FillReportServiceClassContent()
        {
            string str = File.ReadAllText("WebReport//Service.txt", Encoding.GetEncoding("iso-8859-9"));
            StreamWriter file = new StreamWriter(filepath, false, Encoding.UTF8);//(filepath);
            str = str.Replace("#tName", table.name).Replace("#pColType", table.primery_column.dataType).Replace("#pColName", table.primery_column.name).Replace("#colName", GetName()).Replace("#mapVwModelToModel", MapViewModelColumns()).Replace("#mapDbModelToModel", MapDbModelColumns());
            file.Write(str);
            file.Close();
        }
        private string GetName()
        {
            string temp = null;
            foreach (var item in table.columns)
            {
                if (item.name.Contains("name") || item.name.Contains("Name"))
                {
                    temp = item.name;
                    break;
                }
            }
            return temp == null ? table.columns[0].name : temp;
        }

        private string MapViewModelColumns()
        {
            string temp = "";
            foreach (var item in table.columns)
            {
                temp += item.name + " = model._" + table.name + "VwModel." + item.name + ",\n\t\t\t\t\t";
            }
            return temp;
        }
        private string MapDbModelColumns()
        {
            string temp = "";
            foreach (var item in table.columns)
            {
                temp += "updModel." + item.name + " = model._" + table.name + "Model." + item.name + ";\n\t\t\t\t\t";
            }
            return temp;
        }
    }
}
