﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;

namespace Avs.Win.Generate
{
    public class ReportSourceFactory
    {
        private Table table = new Table();
        private string filepath = String.Empty;

        public ReportSourceFactory(Table table)
        {
            this.table = table;
            this.filepath = Constants.ReportSourceFactoryFolder + "\\" + table.name + "Factory.cs";
        }

        public bool WriteReportSourceFactoryFile()
        {
            try
            {
                FillReportSourceFactoryClassContent();
                return true;
            }
            catch (Exception)
            {

            }
            return false;

        }
        private void FillReportSourceFactoryClassContent()
        {
            string str = File.ReadAllText("WebReport//SourceFactory.txt", Encoding.GetEncoding("iso-8859-9"));
            StreamWriter file = new StreamWriter(filepath, false, Encoding.UTF8);//(filepath);
            str = str.Replace("#tName", table.name);
            file.Write(str);
            file.Close();
        }

    }
}
