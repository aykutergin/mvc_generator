﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;

namespace Avs.Win.Generate
{
    public class MonoClient
    {
        private Table table = new Table();
        private string filepath = String.Empty;

        public MonoClient(Table table)
        {
            this.table = table;
            this.filepath = Constants.MonoClientFolder + "\\" + table.name + "Client.cs";
        }

        public bool WriteMonoClientFile()
        {
            try
            {
                FillMonoClientClassContent();
                return true;
            }
            catch (Exception ex)
            {

            }
            return false;

        }
        private void FillMonoClientClassContent()
        {
            string str = File.ReadAllText("WebReport//MonoClient.txt", Encoding.GetEncoding("iso-8859-9"));
            StreamWriter file = new StreamWriter(filepath, false, Encoding.UTF8);//(filepath);
            str = str.Replace("#tName", table.name);
            file.Write(str);
            file.Close();
        }

    }
}
