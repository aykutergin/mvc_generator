﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;

namespace Avs.Win.Generate
{
    public class OpModel
    {
        private Table table=new Table();
        private string filepath=String.Empty;

        public OpModel(Table table)
        {
            this.table = table;
            this.filepath =Constants.OpModelFolder + "\\" + table.name + "Op.cs";
        }
      
        public bool WriteOpModelFile()
        {
            try
            {
                FillOpModelClassContent();
                return true;
            }
            catch (Exception)
            {
                
            }
            return false;
            
        }
        private void FillOpModelClassContent()
        {
           
            StreamWriter file = new StreamWriter(filepath,false, Encoding.UTF8);//(filepath);

            file.WriteLine("using "+Constants.dto_ns+".Filter;");
            file.WriteLine("using "+Constants.dto_ns+".Select;");
            file.WriteLine("using "+Constants.ent_ns+";");
            file.WriteLine("using "+Constants.ent_ns+".DAL;");
            file.WriteLine("using System.Collections.Generic;");
            file.WriteLine("using System.Linq;");
            file.WriteLine("using System.Data.Entity;");

            file.WriteLine("namespace "+Constants.dto_ns+".DtoOpClass");
            file.WriteLine("{");
            file.WriteLine("public partial class "+table.name+"Op");
            file.WriteLine("{");

           
            file.WriteLine("private "+Constants.EntityContextName+" db;");

            
            file.WriteLine("public "+table.name+"Op()");
            file.WriteLine("{");
            file.WriteLine("ContextFactory fac = new ContextFactory();");
            file.WriteLine("db = fac.db;");
            file.WriteLine("}");



            //get all entity with searchlist
            file.WriteLine("public IEnumerable<" + table.name + "> GetEntities(" + table.name + "Filter f = null, " + table.name + "Select s = null)");
            file.WriteLine("{");
            file.WriteLine("IQueryable<" + table.name + "> dtos = db." + table.namePlural + ";");
            file.WriteLine("if (s != null)");
            file.WriteLine("{");
            file.WriteLine("s.SetDtos(dtos);");
            file.WriteLine("s.Include();");
            file.WriteLine("dtos = s.dtos;");
            file.WriteLine("}");
            file.WriteLine("if (f != null)");
            file.WriteLine("{");
            file.WriteLine("f.SetDtos(dtos);");
            file.WriteLine("f.Filter();");
            file.WriteLine("if (f.order != null) f.Order();");
            file.WriteLine("if (f.truncate != null) f.Truncate();");
            file.WriteLine("dtos = f.dtos;");
            file.WriteLine("}");
            file.WriteLine("return dtos.AsEnumerable();");
            file.WriteLine("}");

            //get all with searchlist
            file.WriteLine("public List<" + table.name + "Dto> Get(" + table.name + "Filter f = null, " + table.name + "Select s = null)");
            file.WriteLine("{");
            file.WriteLine("return GetEntities(f,s).Select(x => ToDto(x, s)).ToList();");
            file.WriteLine("}");

            //get entity with id
            file.WriteLine("public "+table.name+ "Dto Get(" + table.primery_column.dataType + " id, " + table.name+"Select s = null)");
            file.WriteLine("{");
            file.WriteLine("var f = new " + table.name + "Filter() { " + table.primery_column.name + " = id };");
            file.WriteLine("return GetEntities(f, s).Select(x => ToDto(x, s)).ToList().FirstOrDefault();");
            file.WriteLine("}");



            //count
            file.WriteLine("public int Count("+table.name+"Filter f = null)");
            file.WriteLine("{");
            file.WriteLine("return GetEntities(f).Count();");
            file.WriteLine("}");

            //create
            file.WriteLine("public "+table.name+"Dto Create("+table.name+"Dto "+table.name+")");
            file.WriteLine("{");
            file.WriteLine("var created = ToEntity("+table.name+");");
            file.WriteLine("db."+table.namePlural+".Add(created);");
            file.WriteLine("db.SaveChanges();");
            file.WriteLine("return ToDto(created, new "+table.name+"Select());");
            file.WriteLine("}");

            //create multi
            file.WriteLine("public List<"+table.name+"Dto> Create(List<"+table.name+"Dto> dtos)");
            file.WriteLine("{");
            file.WriteLine("var created = dtos.Select(x => ToEntity(x));");
            file.WriteLine("db."+table.namePlural+".AddRange(created);");
            file.WriteLine("db.SaveChanges();");
            file.WriteLine("return created.Select(x => ToDto(x, new "+table.name+"Select())).ToList();");
            file.WriteLine("}");


           

            //update
            file.WriteLine("public "+table.name+"Dto Update("+table.name+"Dto "+table.name+")");
            file.WriteLine("{");
            file.WriteLine("var exist = db."+table.namePlural+".Find("+table.name+"."+table.primery_column.name+");");
            file.WriteLine("var updated = ToEntity("+table.name+", exist);");
            file.WriteLine("db.Entry(updated).State = EntityState.Modified;");
            file.WriteLine("db.SaveChanges();");
            file.WriteLine("return ToDto(updated, new "+table.name+"Select());");
            file.WriteLine("}");


            //update multi
            file.WriteLine("public List<" + table.name + "Dto> Update(List<" + table.name + "Dto> dtos)");
            file.WriteLine("{");
            file.WriteLine("List <" + table.name + "Dto> updatedDtos = new List<" + table.name + "Dto>();");
            file.WriteLine("foreach (var item in dtos)");
            file.WriteLine("{");
            file.WriteLine("var exist = db." + table.namePlural + ".Find(item." + table.primery_column.name + ");");
            file.WriteLine("var updated = ToEntity(item, exist);");
            file.WriteLine("db.Entry(updated).State = EntityState.Modified;");
            file.WriteLine("updatedDtos.Add(ToDto(updated, new " + table.name + "Select()));");
            file.WriteLine("}");
            file.WriteLine("db.SaveChanges();");
            file.WriteLine("return updatedDtos;");
            file.WriteLine("}");
            

            //delete
            file.WriteLine("public bool Delete(" + table.primery_column.dataType + " id)");
            file.WriteLine("{");
            file.WriteLine("db."+table.namePlural+".Remove(db."+table.namePlural+".Find(id));");
            file.WriteLine("db.SaveChanges();");
            file.WriteLine("return true;");
            file.WriteLine("}");

            //delete multi
            file.WriteLine("public bool Delete(List<" + table.primery_column.dataType + "> ids)");
            file.WriteLine("{");
            file.WriteLine("foreach (var id in ids)");
            file.WriteLine("{");
            file.WriteLine("db." + table.namePlural + ".Remove(db." + table.namePlural + ".Find(id));");
            file.WriteLine("}");
            file.WriteLine("db.SaveChanges();");
            file.WriteLine("return true;");
            file.WriteLine("}");

            //toDto
            file.WriteLine("internal static "+table.name+"Dto ToDto("+table.name+ " entity, " + table.name + "Select s = null)");
            file.WriteLine("{");
            file.WriteLine("var dto = new "+table.name+"Dto()");
            file.WriteLine("{");
                //columns
                foreach (var item in table.columns)
                {
                        file.WriteLine("" + item.name + " = entity." + item.name+",");
                }
            foreach (Column item in table.columns)
            {
                if (item.isForeign)
                    file.WriteLine("_"+item.foreign.table.name+ " = (s!=null && s.flag" + item.foreign.table.name + " && entity." + item.foreign.table.name+" != null) ? "+item.foreign.table.name+"Op.ToDto(entity."+item.foreign.table.name+ ",new " + item.foreign.table.name + "Select()) : null,");
            }
            foreach (ForeignKey item in table.ref_foreign)
            {
                file.WriteLine(""+item.table.name+ "list = (s != null && s.flag" + item.table.name + "list && entity." + item.table.namePlural+" != null) ? entity."+ item.table.namePlural + ".Select(x => "+item.table.name+ "Op.ToDto(x,new " + item.table.name + "Select() {flag"+table.name+"=false })).ToList() : null,");
            }
            foreach (NNForeignKey item in table.nnrel_foreign)
            {
                file.WriteLine(""+item.targetTable.name+ "list = (s != null && s.flag"+item.targetTable.name+ "list && (entity."+item.gapTable.name+ ".Where(x=>x."+table.primery_column.name+ "==entity." + table.primery_column.name + ") != null)) ? entity." + item.gapTable.name+ ".Where(x => x."+table.primery_column.name+ " == entity." + table.primery_column.name + ").Select(x => " + item.targetTable.name+ "Op.ToDto(x."+item.targetTable.name+ ", new "+item.targetTable.name+ "Select() { flag"+table.name+"list = false })).ToList() : null,");
            }

            file.WriteLine("};");
            file.WriteLine("return dto;");
            file.WriteLine("}");


            //toEntity
            file.WriteLine("internal static "+table.name+" ToEntity("+table.name+ "Dto dto," + table.name + " exist=null)");
            file.WriteLine("{");
    
            file.WriteLine("var e = (exist != null) ? exist : new " + table.name + "();");
            //for

            foreach (var item in table.columns)
                {
                     file.WriteLine("e." + item.name + " = dto." + item.name + ";");
                }
            foreach (Column item in table.columns)
            {
                if (item.isForeign)
                    file.WriteLine("if(dto._"+item.foreign.table.name+" != null) e." + item.foreign.table.name +"="+ item.foreign.table.name+"Op.ToEntity(dto._"+item.foreign.table.name+");");
            }
            foreach (ForeignKey item in table.ref_foreign)
            {
                file.WriteLine( "if(dto."+item.table.name+ "list != null) e."+ item.table.namePlural +"= dto." + item.table.name+"list.Select(x => "+item.table.name+"Op.ToEntity(x)).ToList();");
            }
            foreach (NNForeignKey item in table.nnrel_foreign)
            {
                file.WriteLine("if(dto." + item.targetTable.name + "list != null) e."+item.gapTable.namePlural+"= dto." + item.targetTable.name + "list.Select(x => new "+item.gapTable.name+"()");
                file.WriteLine("{");
                file.WriteLine(""+table.primery_column.name+ " = dto." + table.primery_column.name + ",");
                file.WriteLine("" + item.targetColumn.name + " = x." + item.targetColumn.name + ",");
                file.WriteLine(""+item.targetTable.name+ " = " + item.targetTable.name + "Op.ToEntity(x)");
                file.WriteLine("}).ToList();");
            }
            file.WriteLine("return e;");
            file.WriteLine("}");


            file.WriteLine("}");
            file.WriteLine("}");


file.Close();
            
        }
 
    }
}
