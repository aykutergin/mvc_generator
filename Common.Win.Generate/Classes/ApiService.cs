﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;

namespace Avs.Win.Generate
{
    public class ApiService
    {
        private Table table = new Table();
        private string filepath = String.Empty;

        public ApiService(Table table)
        {
            this.table = table;
            this.filepath = Constants.ApiServiceFolder + "\\" + table.name + "Service.cs";
        }

        public bool WriteApiServiceFile()
        {
            try
            {
                FillApiServiceClassContent();
                return true;
            }
            catch (Exception)
            {

            }
            return false;

        }
        private void FillApiServiceClassContent()
        {

            StreamWriter file = new StreamWriter(filepath, false, Encoding.UTF8);//(filepath);
            file.WriteLine("using "+Constants.dto_ns+";");
            file.WriteLine("using "+Constants.dto_ns+".DtoOpClass;");
            file.WriteLine("using "+Constants.dto_ns+".Filter;");
            file.WriteLine("using "+Constants.dto_ns+".Select;");
            file.WriteLine("using log4net;");
            file.WriteLine("using System;");
            file.WriteLine("using System.Collections.Generic;");
            file.WriteLine("");

            file.WriteLine("namespace "+Constants.api_ns+".Services");
            file.WriteLine("{");
            file.WriteLine("public class "+table.name+"Service");
            file.WriteLine("{");
            file.WriteLine("private static ILog log = LogManager.GetLogger(typeof("+table.name+"Service));");
            file.WriteLine(""+table.name+"Op op = new "+table.name+"Op();");

            //Get id
            file.WriteLine("public "+table.name+ "Dto Get(" + table.primery_column.dataType + " id," + table.name+"Select select=null) {");
            file.WriteLine("try");
            file.WriteLine("{");
            file.WriteLine("return op.Get(id, select);");
            file.WriteLine("}");
            file.WriteLine("catch (Exception ex)");
            file.WriteLine("{");
            file.WriteLine("log.Error(\""+table.name+"Service, get(id) \", ex);");
            file.WriteLine("}");
            file.WriteLine("return null;");
            file.WriteLine("}");
            file.WriteLine("");

            //Get Retrive
            file.WriteLine("public List<"+table.name+"Dto> Get("+table.name+"Filter filter, "+table.name+"Select select = null)");
            file.WriteLine("{");
            file.WriteLine("try");
            file.WriteLine("{");
            file.WriteLine("return op.Get(filter,select);");
            file.WriteLine("}");
            file.WriteLine("catch (Exception ex)");
            file.WriteLine("{");
            file.WriteLine("log.Error(\""+table.name+"Service, get() \", ex);");
            file.WriteLine("}");
            file.WriteLine("return null;");
            file.WriteLine("}");
            file.WriteLine("");

            file.WriteLine("public int? Count(" + table.name + "Filter filter = null)");
            file.WriteLine("{");
            file.WriteLine("try");
            file.WriteLine("{");
            file.WriteLine("return op.Count(filter);");
            file.WriteLine("}");
            file.WriteLine("catch (Exception ex)");
            file.WriteLine("{");
            file.WriteLine("log.Error(\"" + table.name + "Service, get() \", ex);");
            file.WriteLine("}");
            file.WriteLine("return null;");
            file.WriteLine("}");

            //Create
            file.WriteLine("public "+table.name+"Dto Create("+table.name+"Dto "+table.name+"Dto)");
            file.WriteLine("{");
            file.WriteLine("try");
            file.WriteLine("{");
            file.WriteLine("return op.Create("+table.name+"Dto);");
            file.WriteLine("}");
            file.WriteLine("catch (Exception ex)");
            file.WriteLine("{");
            file.WriteLine("log.Error(\""+table.name+"Service, Create\", ex);");
            file.WriteLine("}");
            file.WriteLine("return null;");
            file.WriteLine("}");
            file.WriteLine("");

            //Update
            file.WriteLine("public "+table.name+"Dto Update("+table.name+"Dto "+table.name+"Dto)");
            file.WriteLine("{");
            file.WriteLine("try");
            file.WriteLine("{");
            file.WriteLine("return op.Update("+table.name+"Dto);");
            file.WriteLine("}");
            file.WriteLine("catch (Exception ex)");
            file.WriteLine("{");
            file.WriteLine("log.Error(\""+table.name+"Service, update\", ex);");
            file.WriteLine("}");
            file.WriteLine("return null;");
            file.WriteLine("}");
            file.WriteLine("");

            //Delete
            file.WriteLine("public bool Delete(" + table.primery_column.dataType + " id)");
            file.WriteLine("{");
            file.WriteLine("try");
            file.WriteLine("{");
            file.WriteLine("return op.Delete(id);");
            file.WriteLine("}");
            file.WriteLine("catch (Exception ex)");
            file.WriteLine("{");
            file.WriteLine("log.Error(\""+table.name+"Service, delete\", ex);");
            file.WriteLine("}");
            file.WriteLine("return false;");
            file.WriteLine("}");


            //CreateMulti
            file.WriteLine("public List<" + table.name + "Dto> Create(List<" + table.name + "Dto> " + table.name + "Dto)");
            file.WriteLine("{");
            file.WriteLine("try");
            file.WriteLine("{");
            file.WriteLine("return op.Create(" + table.name + "Dto);");
            file.WriteLine("}");
            file.WriteLine("catch (Exception ex)");
            file.WriteLine("{");
            file.WriteLine("log.Error(\"" + table.name + "Service, CreateAll\", ex);");
            file.WriteLine("}");
            file.WriteLine("return null;");
            file.WriteLine("}");
            file.WriteLine("");

            //UpdateMulti
            file.WriteLine("public List<" + table.name + "Dto> Update(List<" + table.name + "Dto> " + table.name + "Dto)");
            file.WriteLine("{");
            file.WriteLine("try");
            file.WriteLine("{");
            file.WriteLine("return op.Update(" + table.name + "Dto);");
            file.WriteLine("}");
            file.WriteLine("catch (Exception ex)");
            file.WriteLine("{");
            file.WriteLine("log.Error(\"" + table.name + "Service, updateAll\", ex);");
            file.WriteLine("}");
            file.WriteLine("return null;");
            file.WriteLine("}");
            file.WriteLine("");

            //DeleteMulti
            file.WriteLine("public bool Delete(List<" + table.primery_column.dataType + "> ids)");
            file.WriteLine("{");
            file.WriteLine("try");
            file.WriteLine("{");
            file.WriteLine("return op.Delete(ids);");
            file.WriteLine("}");
            file.WriteLine("catch (Exception ex)");
            file.WriteLine("{");
            file.WriteLine("log.Error(\"" + table.name + "Service, deleteAll\", ex);");
            file.WriteLine("}");
            file.WriteLine("return false;");
            file.WriteLine("}");
            file.WriteLine("}");
            file.WriteLine("}");

            file.Close();

        }

    }
}
