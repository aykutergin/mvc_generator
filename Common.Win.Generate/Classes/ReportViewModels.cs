﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;

namespace Avs.Win.Generate
{
    public class ReportViewModels
    {
        private Table table = new Table();
        private string filepath = String.Empty;

        public ReportViewModels(Table table)
        {
            this.table = table;
            this.filepath = Constants.ReportViewModelsFolder + "\\" + table.name + "ViewModel.cs";
        }

        public bool WriteReportViewModelsFile()
        {
            try
            {
                FillReportViewModelsClassContent();
                return true;
            }
            catch (Exception)
            {

            }
            return false;

        }
        public bool WriteGetReportViewModelsFile()
        {
            try
            {
                FillGetReportViewModelsClassContent();
                return true;
            }
            catch (Exception)
            {

            }
            return false;

        }
        private void FillGetReportViewModelsClassContent()
        {
            string str = File.ReadAllText("WebReport//GetViewModels.txt", Encoding.GetEncoding("iso-8859-9"));
            StreamWriter file = new StreamWriter(filepath, false, Encoding.UTF8);//(filepath);
            str = str.Replace("#tName", table.name);
            file.Write(str);
            file.Close();
        }
        private void FillReportViewModelsClassContent()
        {
            string str = File.ReadAllText("WebReport//ViewModels.txt", Encoding.GetEncoding("iso-8859-9"));
            StreamWriter file = new StreamWriter(filepath, false, Encoding.UTF8);//(filepath);
            str = str.Replace("#tName", table.name);
            file.Write(str);
            file.Close();
        }

    }
}
