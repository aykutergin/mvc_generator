﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;

namespace Avs.Win.Generate
{
    public class OpFilter
    {
        private Table table = new Table();
        private string filepath = String.Empty;

        public OpFilter(Table table)
        {
            this.table = table;
            this.filepath = Constants.OpFilterFolder + "\\" + table.name + "Filter.cs";
        }

        public bool WriteOpFilterFile()
        {
            try
            {
                FillOpFilterClassContent();
                return true;
            }
            catch (Exception)
            {

            }
            return false;

        }
        private void FillOpFilterClassContent()
        {

            StreamWriter file = new StreamWriter(filepath, false, Encoding.UTF8);//(filepath);

            file.WriteLine("using System;");
            file.WriteLine("using System.Collections.Generic;");
            file.WriteLine("using System.Linq;");
            file.WriteLine("using System.Data.Entity;");
            file.WriteLine("using "+Constants.ent_ns+";");
            file.WriteLine("using "+Constants.dto_ns+".Select;");
            file.WriteLine("using " + Constants.utility_ns+";");

            file.WriteLine("namespace "+Constants.dto_ns+".Filter");
            file.WriteLine("{");
            file.WriteLine("public partial class " + table.name + "Filter");
            file.WriteLine("{");

            //global prop
            file.WriteLine("internal IQueryable<" + table.name + "> dtos;");
           

        //primary
        file.WriteLine("public " + table.primery_column.dataType + " " + table.primery_column.name + " { get; set; }");

            //foreign
            foreach (Column item in table.columns)
            {
                if (item.isForeign)
                    file.WriteLine("public " + item.dataType.Replace("?","") + " " + item.name + " { get; set; }");
            }
            foreach (ForeignKey item in table.ref_foreign)
            {
                file.WriteLine("public " + item.table.primery_column.dataType.Replace("?", "") + " " + item.table.primery_column.name+" { get; set; }");
            }
            foreach (NNForeignKey item in table.nnrel_foreign)
            {
                file.WriteLine("public " + item.targetColumn.dataType.Replace("?", "") + " " + item.targetColumn.name + " { get; set; }");
            }
            
            //order
            file.WriteLine("public OrderFilter order { get; set; }");
            //paging
            file.WriteLine(" public TruncateFilter truncate { get; set; }");

            file.WriteLine("public void SetDtos(IQueryable<" + table.name + "> dtos)");
            file.WriteLine("{");
            file.WriteLine("this.dtos = dtos;");
            file.WriteLine("}");

            file.WriteLine("partial void AddFilter();");
            file.WriteLine("internal void Filter()");
            file.WriteLine("{");
            file.WriteLine("if (" + table.primery_column.name + " != 0)");
            file.WriteLine("{");
            file.WriteLine("dtos = dtos.Where(x => x." + table.primery_column.name + " == " + table.primery_column.name + ");");
            file.WriteLine("}");
            foreach (Column item in table.columns)
            {
                if (item.isForeign) {
                    file.WriteLine("if (" + item.name + " != 0)");
                    file.WriteLine("{");
                    file.WriteLine("dtos = dtos.Where(x => x." + item.name + " == " + item.name + ");");
                    file.WriteLine("}");
                }
            }
            foreach (ForeignKey item in table.ref_foreign)
            {
                file.WriteLine("if (" + item.table.primery_column.name + " != 0)");
                file.WriteLine("{");
                file.WriteLine("dtos = dtos.Where(x => x."+item.table.namePlural+".Where(y => y." + item.table.primery_column.name + " == " + item.table.primery_column.name + ").Count() > 0);");
                file.WriteLine("}");
            }
            foreach (NNForeignKey item in table.nnrel_foreign)
            {
                file.WriteLine("if (" + item.targetColumn.name + " != 0)");
                file.WriteLine("{");
                file.WriteLine("dtos = dtos.Where(x => x." + item.gapTable.namePlural + ".Where(y => y." + item.targetColumn.name + " == " + item.targetColumn.name + ").Count() > 0);");
                file.WriteLine("}");
            }

            file.WriteLine("AddFilter();");
            file.WriteLine("}");

            file.WriteLine("internal void Order()");
            file.WriteLine("{");
            file.WriteLine("if (!String.IsNullOrEmpty(order.order))");
            file.WriteLine("{");
            file.WriteLine("var data = dtos.AsQueryable();");
            file.WriteLine("dtos = LinqHelper.OrderBy(data, order.order, order.orderIsAsc);");
            file.WriteLine("}");
            file.WriteLine("}");
            file.WriteLine("internal void Truncate()");
            file.WriteLine("{");
            file.WriteLine("if (truncate.offset > 0)");
            file.WriteLine("{");
            file.WriteLine("dtos = dtos.Skip(truncate.offset);");
            file.WriteLine("}");
            file.WriteLine("else if (truncate.page != 0)");
            file.WriteLine("{");
            file.WriteLine("if (truncate.count == 0) truncate.count = 10; //paging default 10");
            file.WriteLine("truncate.offset = (truncate.page - 1) * truncate.count;");
            file.WriteLine("dtos = dtos.Skip(truncate.offset).Take(truncate.count);");
            file.WriteLine("}");
            file.WriteLine("else if (truncate.count > 0)");
            file.WriteLine("{");
            file.WriteLine("dtos = dtos.Take(truncate.count);");
            file.WriteLine("}");
            file.WriteLine("}");
            
            file.WriteLine("}");

            file.WriteLine("public class " + table.name + "Param");
            file.WriteLine("{");
            file.WriteLine("public " + table.name + "Filter filter { get; set; }");
            file.WriteLine("public " + table.name + "Select select { get; set; }");
            file.WriteLine("}");


            file.WriteLine("}");


            file.Close();

        }

    }
}
