﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;

namespace Avs.Win.Generate
{
    public class ReportFilterModel
    {
        private Table table = new Table();
        private string filepath = String.Empty;

        public ReportFilterModel(Table table)
        {
            this.table = table;
            this.filepath = Constants.ReportFilterModelFolder + "\\" + table.name + "FilterModel.cs";
        }

        public bool WriteFilterModelFile()
        {
            try
            {
                FillModelClassContent();
                return true;
            }
            catch (Exception)
            {

            }
            return false;

        }
        private void FillModelClassContent()
        {
            string str = File.ReadAllText("WebReport//FilterModel.txt", Encoding.GetEncoding("iso-8859-9"));
            StreamWriter file = new StreamWriter(filepath, false, Encoding.UTF8);//(filepath);
            str = str.Replace("#tName", table.name).Replace("#attributes", GetAttributes());
            file.Write(str);
            file.Close();
        }
        private string GetAttributes()
        {
            string temp = "";
            foreach (var item in table.columns)
            {
                if ((item.dataType.Contains("int") || item.dataType.Contains("decimal")) && (item.name.Contains("id") || item.name.Contains("Id")))
                {
                    temp += "public " + item.dataType + " " + item.name + " { get; set; }\n\t\t";
                }
            }
            return temp;
        }
    }
}
