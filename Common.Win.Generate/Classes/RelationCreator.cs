﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Avs.Win.Generate.Classes
{
    class RelationCreator
    {
        private Table table = new Table();
        private string filepath = String.Empty;
        private bool isTable = true;

        public RelationCreator(Table table, bool isTable)
        {
            this.table = table;
            // this.filepath = Constants.SqlViewFolder + "\\VW_" + table.name + ".sql";

            this.filepath = Constants.SqlViewFolder + "\\dbrel.sql";
            this.isTable = isTable;

        }

        public bool WriteSqlFile()
        {
            try
            {
                FillSqlContent();
                return true;
            }
            catch (Exception)
            {

            }
            return false;

        }
        private void FillSqlContent()
        {
            string str = File.ReadAllText("WebReport//foreignRel.txt", Encoding.GetEncoding("iso-8859-9"));
            string content = "";
            StreamWriter file = new StreamWriter(filepath, false, Encoding.UTF8);//(filepath);

            foreach (var col in table.columns)
            {
                if (col.name.Substring(col.name.Length-2)=="Id" && table.primery_column.name!=col.name)//eğer son iki karakter Id ise
                {
                    content += str.Replace("#tName", table.name).Replace("#refTName", col.foreign.table.name).Replace("#tColName", col.name).Replace("#refColName", col.name);
                }
            }
            file.Write(str);
            file.Close();

        }

    }
}
