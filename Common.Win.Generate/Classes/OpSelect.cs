﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;

namespace Avs.Win.Generate
{
    public class OpSelect
    {
        private Table table = new Table();
        private string filepath = String.Empty;

        public OpSelect(Table table)
        {
            this.table = table;
            this.filepath = Constants.OpSelectFolder + "\\" + table.name + "Select.cs";
        }

        public bool WriteOpSelectFile()
        {
            try
            {
                FillOpSelectClassContent();
                return true;
            }
            catch (Exception)
            {

            }
            return false;

        }
        private void FillOpSelectClassContent()
        {

            StreamWriter file = new StreamWriter(filepath, false, Encoding.UTF8);//(filepath);
   
            file.WriteLine("using System.Linq;");
            file.WriteLine("using System.Data.Entity;");
            file.WriteLine("using " + Constants.ent_ns + ";");

            file.WriteLine("namespace "+Constants.dto_ns+".Select");
            file.WriteLine("{");
            file.WriteLine("public partial class "+table.name+"Select");
            file.WriteLine("{");

            file.WriteLine("public " + table.name + "Select()");
            file.WriteLine("{");

            foreach (Column item in table.columns)
            {
                if (item.isForeign)
                    file.WriteLine("flag" + item.foreign.table.name + " = false;");
            }

            file.WriteLine("}");

            //global prop
            file.WriteLine("internal IQueryable<" + table.name + "> dtos;");

            foreach (Column item in table.columns)
            {
                if (item.isForeign)
                    file.WriteLine("public bool flag"+item.foreign.table.name+" { get; set; }");
            }
            foreach (ForeignKey item in table.ref_foreign)
            {
                file.WriteLine("public bool flag" + item.table.name + "list { get; set; }");
            }
            foreach (NNForeignKey item in table.nnrel_foreign)
            {
                file.WriteLine("public bool flag" + item.targetTable.name + "list { get; set; }");
            }

            file.WriteLine("public void SetDtos(IQueryable<" + table.name + "> dtos)");
            file.WriteLine("{");
            file.WriteLine("this.dtos = dtos;");
            file.WriteLine("}");

            file.WriteLine("partial void AddInclude();");

            file.WriteLine("internal void Include()");

            file.WriteLine("{");
            foreach (Column item in table.columns)
            {
                if (item.isForeign)
                    file.WriteLine("if (flag" + item.foreign.table.name + ") dtos = dtos.Include(x => x."+ item.foreign.table.name + ");");
            }
            foreach (ForeignKey item in table.ref_foreign)
            {
                file.WriteLine("if (flag" + item.table.name + "list) dtos = dtos.Include(x => x."+ item.table.namePlural + ");");
            }
            foreach (NNForeignKey item in table.nnrel_foreign)
            {
                file.WriteLine("if (flag" + item.targetTable.name + "list) dtos = dtos.Include(x => x." + item.gapTable.namePlural + ");");
            }
            
            file.WriteLine("AddInclude();");
            file.WriteLine("}");



        file.WriteLine("}");
    file.WriteLine("}");

    file.Close();

        }

    }
}
