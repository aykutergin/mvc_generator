﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;

namespace Avs.Win.Generate
{
    public class ReportISource
    {
        private Table table = new Table();
        private string filepath = String.Empty;
        private string filepathget = String.Empty;

        public ReportISource(Table table)
        {
            this.table = table;
            this.filepath = Constants.ReportISourceFolder + "\\I" + table.name + "Source.cs";
            this.filepathget = Constants.ReportGetISourceFolder + "\\I" + table.name + "Source.cs";
        }

        public bool WriteReportISourceFile()
        {
            try
            {
                FillReportISourceClassContent();
                return true;
            }
            catch (Exception ex)
            {

            }
            return false;

        }
        public bool WriteGetReportISourceFile()
        {
            try
            {
                FillGetReportISourceClassContent();
                return true;
            }
            catch (Exception ex)
            {

            }
            return false;

        }
        private void FillGetReportISourceClassContent()
        {
            string str = File.ReadAllText("WebReport//GetISource.txt", Encoding.GetEncoding("iso-8859-9"));
            StreamWriter file = new StreamWriter(filepathget, false, Encoding.UTF8);//(filepath);
            str = str.Replace("#tName", table.name).Replace("#pcoltype", table.primery_column.dataType);
            file.Write(str);
            file.Close();
        }
        private void FillReportISourceClassContent()
        {
            string str = File.ReadAllText("WebReport//ISource.txt", Encoding.GetEncoding("iso-8859-9"));
            StreamWriter file = new StreamWriter(filepath, false, Encoding.UTF8);//(filepath);
            str = str.Replace("#tName", table.name).Replace("#pcoltype", table.primery_column.dataType);
            file.Write(str);
            file.Close();
        }

    }
}
