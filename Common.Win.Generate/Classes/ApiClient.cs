﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;

namespace Avs.Win.Generate
{
    public class ApiClient
    {
        private Table table = new Table();
        private string filepath = String.Empty;

        public ApiClient(Table table)
        {
            this.table = table;
            this.filepath = Constants.ApiClientFolder + "\\" + table.name + "Client.cs";
        }

        public bool WriteApiClientFile()
        {
            try
            {
                FillApiClientClassContent();
                return true;
            }
            catch (Exception)
            {

            }
            return false;

        }
        private void FillApiClientClassContent()
        {
            string str = File.ReadAllText("EClient.txt", Encoding.GetEncoding("iso-8859-9"));
            StreamWriter file = new StreamWriter(filepath, false, Encoding.UTF8);//(filepath);
            str = str.Replace("#dtons", Constants.dto_ns).Replace("#restns", Constants.rest_ns).Replace("#clientns", Constants.client_ns).Replace("#tblname",table.name).Replace("#prcolumntype",table.primery_column.dataType);
            file.Write(str);
            file.Close();
        }

    }
}
