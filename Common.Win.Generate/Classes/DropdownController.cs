﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Windows.Forms;

namespace Avs.Win.Generate
{
    public class DropdownController
    {
        private string controllerfilepath = String.Empty;
        private string servicefilepath = String.Empty;

        public DropdownController()
        {
            this.controllerfilepath = Constants.DropdownControllerFolder + "\\DropdownController.cs";
            this.servicefilepath = Constants.DropdownServiceFolder + "\\DropdownService.cs";
        }

        public bool WriteGetDropdownControllerFile(CheckedListBox clbview)
        {
            try
            {
                string controllerFile = File.ReadAllText("WebReport//Dropdown//DropdownController.txt", Encoding.GetEncoding("iso-8859-9"));
                string controllerContentFile = File.ReadAllText("WebReport//Dropdown//ControllerContent.txt", Encoding.GetEncoding("iso-8859-9"));

                string serviceFile = File.ReadAllText("WebReport//Dropdown//DropdownService.txt", Encoding.GetEncoding("iso-8859-9"));
                string serviceContentFile = File.ReadAllText("WebReport//Dropdown//ServiceContent.txt", Encoding.GetEncoding("iso-8859-9"));
                StreamWriter filecontroller = new StreamWriter(controllerfilepath, false, Encoding.UTF8);//(filepath);
                StreamWriter fileService = new StreamWriter(servicefilepath, false, Encoding.UTF8);//(filepath);

                string controllerContent = string.Empty;
                string serviceContnet = string.Empty;
                for (var j = 0; j < clbview.Items.Count; j++)
                {
                    if (clbview.GetItemChecked(j) == true)
                    {
                        var tableName = clbview.Items[j].ToString().Trim();
                        var table = new TableManager().CreateTable(tableName);
                        if (table.primery_column == null)
                        {
                            table.primery_column = table.columns.FirstOrDefault();
                        }
                        CheckTablePlural(table);
                        controllerContent += controllerContentFile.Replace("#tableName", table.name);
                        serviceContnet += serviceContentFile.Replace("#tableName", table.name).Replace("#primaryKey", table.primery_column.name).Replace("#colName", GetName(table));
                    }
                    Application.DoEvents();
                }
                controllerFile = controllerFile.Replace("#content", controllerContent);
                serviceFile = serviceFile.Replace("#content", serviceContnet);

                filecontroller.Write(controllerFile);
                filecontroller.Close();
                fileService.Write(serviceFile);
                fileService.Close();

                return true;
            }
            catch (Exception ex)
            {

            }
            return false;
        }
        public string GetName(Table table)
        {
            var data = table.columns.Where(x => x.name.ToLower().Contains("name") || x.name.ToLower().Contains("value") || x.name.ToLower().Contains("text")).Select(x => x.name);
            if (data == null || data.Count() == 0)
            {
                return table.columns.FirstOrDefault().name;
            }
            return data.FirstOrDefault().ToString();
        }
        public void CheckTablePlural(Table table)
        {
            table.name = tools.GetSingular(table.name);
            table.namePlural = tools.GetPlural(table.name);
            foreach (var item in table.columns)
            {
                if (item.isForeign)
                {
                    item.foreign.table.name = tools.GetSingular(item.foreign.table.name);
                    item.foreign.table.namePlural = tools.GetPlural(item.foreign.table.name);
                }
            }

        }

    }
}
