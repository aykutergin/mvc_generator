﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;

namespace Avs.Win.Generate
{
    public class ValidationModel
    {
        private Table table = new Table();
        private string filepath = String.Empty;

        public ValidationModel(Table table)
        {
            this.table = table;
            this.filepath = Constants.ReportValidationModelFolder + "\\" + table.name + "ValidationModel.cs";
        }

        public bool WriteModelFile()
        {
            try
            {
                FillModelClassContent();
                return true;
            }
            catch (Exception ex)
            {

            }
            return false;

        }
        private void FillModelClassContent()
        {
            string str = File.ReadAllText("WebReport//ValidationModel.txt", Encoding.GetEncoding("iso-8859-9"));
            StreamWriter file = new StreamWriter(filepath, false, Encoding.UTF8);//(filepath);
            str = str.Replace("#tName", table.name).Replace("#validatorContent", GetValidatorContent());
            file.Write(str);
            file.Close();
        }
        private string GetValidatorContent()
        {
            string context = "";
            foreach (var column in table.columns)
            {
                if (!column.isNullable)
                {
                    if (column.isForeign)
                    {
                        context += "RuleFor(x => x." + column.name + ").GreaterThan(0); \n\t\t\t";
                    }
                    else if (!column.isPrimery)
                    {
                        context += "RuleFor(x => x." + column.name + ").NotNull(); \n\t\t\t";
                    }

                }
            }
            return !string.IsNullOrEmpty(context) ? context.Remove(context.Length - 3) : "";
        }
    }
}
