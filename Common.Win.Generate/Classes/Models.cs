﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;

namespace Avs.Win.Generate
{
    public class Models
    {
        private Table table=new Table();
        private string filepath=String.Empty;

        public Models(Table table)
        {
            this.table = table;
            this.filepath =Constants.ModelsFolder + "\\" + table.name + "Dto.cs";
        }
      
        public bool WriteModelFile()
        {
            try
            {
                FillModelClassContent();
                return true;
            }
            catch (Exception)
            {
                
            }
            return false;
            
        }
        private void FillModelClassContent()
        {
           
            StreamWriter file = new StreamWriter(filepath,false, Encoding.UTF8);//(filepath);
            file.WriteLine("using System;");
            file.WriteLine("using System.Text;");
            file.WriteLine("using System.Collections.Generic;");
            file.WriteLine("using System.Collections;");

            file.WriteLine("namespace "+Constants.dto_ns+"");
            file.WriteLine("{");
            file.WriteLine("public partial class " + table.name + "Dto");
            
            file.WriteLine("{");
         
           
            foreach (Column item in table.columns)
            {
                //if(!item.isNullable)
                file.WriteLine("public " + item.dataType + " " + item.name + "{get;set;}");
             ////new
             //   else
             //   file.WriteLine("public " + item.dataType + "? " + item.name + "{get;set;}");
             //  //new 
                if (item.isForeign)
                    file.WriteLine("public " + item.foreign.table.name + "Dto _" + item.foreign.table.name + "{get;set;}");
               
            }
            foreach (ForeignKey item in table.ref_foreign)
            {
                file.WriteLine("public List<" +item.table.name + "Dto> " + item.table.name + "list {get;set;}");
            }
            foreach (NNForeignKey item in table.nnrel_foreign)
            {
                file.WriteLine("public List<" + item.targetTable.name + "Dto> " + item.targetTable.name + "list {get;set;}");
            }
          

          

            file.WriteLine("}");
            file.WriteLine("}");

            file.Close();
        }
 
    }
}
