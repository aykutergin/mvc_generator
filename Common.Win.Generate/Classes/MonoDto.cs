﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;

namespace Avs.Win.Generate
{
    public class MonoDto
    {
        private Table table = new Table();
        private string filepath = String.Empty;

        public MonoDto(Table table)
        {
            this.table = table;
            this.filepath = Constants.MonoDtoFolder + "\\" + table.name + "Dto.cs";
        }

        public bool WriteMonoDtoToFile()
        {
            try
            {
                FillMonoDtoClassContent();
                return true;
            }
            catch (Exception)
            {

            }
            return false;

        }
        private void FillMonoDtoClassContent()
        {
            string str = File.ReadAllText("WebReport//MonoDto.txt", Encoding.GetEncoding("iso-8859-9"));
            StreamWriter file = new StreamWriter(filepath, false, Encoding.UTF8);//(filepath);
            str = str.Replace("#tName", table.name).Replace("#dtoContent", GetModelContent());
            file.Write(str);
            file.Close();
        }
        private string GetModelContent()
        {
            string context = "";
            foreach (var column in table.columns)
            {
                context += "public " + column.dataType + " " + column.name + " { get; set; } \n\t\t";
            }
            return context.Remove(context.Length - 3);
        }
    }
}
