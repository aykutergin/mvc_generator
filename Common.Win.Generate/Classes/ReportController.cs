﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;

namespace Avs.Win.Generate
{
    public class ReportController
    {
        private Table table = new Table();
        private string filepath = String.Empty;

        public ReportController(Table table)
        {
            this.table = table;
            this.filepath = Constants.ReportControllerFolder + "\\" + table.name + "Controller.cs";
        }

        public bool WriteReportControllerFile()
        {
            try
            {
                FillReportControllerClassContent();
                return true;
            }
            catch (Exception)
            {

            }
            return false;

        }

        public bool WriteGetReportControllerFile()
        {
            try
            {
                FillGetReportControllerClassContent();
                return true;
            }
            catch (Exception)
            {

            }
            return false;

        }
        private void FillGetReportControllerClassContent()
        {
            string str = File.ReadAllText("WebReport//GetController.txt", Encoding.GetEncoding("iso-8859-9"));
            StreamWriter file = new StreamWriter(filepath, false, Encoding.UTF8);//(filepath);
            str = str.Replace("#tName", table.name).Replace("#pColType", table.primery_column.dataType).Replace("#pColName", table.primery_column.name);
            file.Write(str);
            file.Close();
        }
        private void FillReportControllerClassContent()
        {
            string str = File.ReadAllText("WebReport//Controller.txt", Encoding.GetEncoding("iso-8859-9"));
            StreamWriter file = new StreamWriter(filepath, false, Encoding.UTF8);//(filepath);
            str = str.Replace("#tName", table.name).Replace("#pColType", table.primery_column.dataType).Replace("#pColName", table.primery_column.name);
            file.Write(str);
            file.Close();
        }

    }
}
