﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

/// <summary>
/// Summary description for Sql
/// </summary>
public class Sql
{
	  string baglantı = System.Configuration.ConfigurationManager.ConnectionStrings["myDB"].ToString();
      SqlConnection contrans;
      SqlTransaction trans;
      DataTable dt_param;
      ArrayList param_list = new ArrayList();
      string sorgu;
    
    public Sql()
    {
        
    }
    public Sql(string s)
    {
        this.sorgu = s;
    }
    public Sql(string s,string baglanti)
    {
        this.sorgu = s;
        this.baglantı = baglanti;
    }
   

   public Boolean loginselect(string UserName, string Password)
    {
        try
        {
            SqlConnection con = new SqlConnection(baglantı);
            con.Open();
            SqlCommand  cmd = new  SqlCommand("SELECT COUNT(ID) FROM pcb_users WHERE userName='"+UserName+"' AND password='"+Password+"'", con);
            if (con.State==ConnectionState.Closed)
            {
                con.Open();
                
            }
            int result = Convert.ToInt32(cmd.ExecuteScalar());
            con.Close();
            if(result > 0)
            {
                
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (Exception e)
        {
            return false;

        }
       
    
    }

   public int Insert()
    {
        SqlConnection con = new SqlConnection(baglantı);
        int affectedrow = 0;
        try
        {
            con.Open();
            SqlCommand cmd = new SqlCommand(sorgu, con);
            AddAllParameter(cmd);
           affectedrow= cmd.ExecuteNonQuery();
        }
        catch (Exception e)
        {
        }
        finally
        {
            con.Close();
        }
        return affectedrow;


    }

   public object InsertReturnIdentity()
   {
       SqlConnection con = new SqlConnection(baglantı);
       try
       {
           con.Open();

           SqlCommand cmd = new SqlCommand(sorgu, con);
           AddAllParameter(cmd);
           cmd.ExecuteNonQuery();
           cmd.CommandText = "SELECT @@IDENTITY";
           cmd.CommandType = CommandType.Text;
           return cmd.ExecuteScalar();
           
       }
       catch (Exception e)
       {

           return false;
       }
       finally
       {
           con.Close();
       }
       return true;


   }

   public DataTable select()
    {

        SqlConnection con = new SqlConnection(baglantı);
        DataSet dset;

        try
        {
            con.Open();

            SqlCommand cmd = new SqlCommand(sorgu, con);
            AddAllParameter(cmd);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = cmd;
            
            dset = new DataSet();
            adapter.Fill(dset);


        }
        catch (Exception e)
        {
            return null;
        }
        finally
        {
            con.Close();
        }
        return dset.Tables[0];



    }

   public object SelectScalar()
   {

       SqlConnection con = new SqlConnection(baglantı);
       object result = "";
       try
       {
           con.Open();
           SqlCommand cmd = new SqlCommand(sorgu, con);
           AddAllParameter(cmd);
           result=cmd.ExecuteScalar();

       }
       catch (Exception e)
       {
           result = "";
       }
       finally
       {
           con.Close();
       }

       return result;


   }

    #region transection function
    public void TransactionConnect()
    {
        contrans = new SqlConnection(baglantı);
        trans = contrans.BeginTransaction();

    }
    public void TransactionExecuteSqlQuery()
    {
        SqlCommand cmd = new SqlCommand(sorgu, contrans);
        cmd.Transaction = trans;
        cmd.ExecuteScalar();
    }
    public void TransactionClose()
    {
        contrans.Close();
        contrans.Dispose();
        trans.Dispose();
    }
    public void TransactionCommit()
    {
        trans.Commit();
    }
    public void TransactionRollback()
    {
        trans.Rollback();
    }
    #endregion

    #region parameter function
    public void SetParameter(string parameter, object value)
    {
        SqlParameter sqlparam = new SqlParameter("@" + parameter, value);
        param_list.Add(sqlparam);
    }
    private void AddAllParameter(SqlCommand cmd)
    {
        foreach (SqlParameter eleman in param_list)
        {
            cmd.Parameters.Add(eleman);
        }
        
    }
    public void ClearParameter()
    {
        param_list.Clear();
    }

    
    #endregion
















}
