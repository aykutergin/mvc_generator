﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;

namespace Avs.Win.Generate
{
    public class ReportSource
    {
        private Table table = new Table();
        private string filepath = String.Empty;
        private string filepathget = String.Empty;

        public ReportSource(Table table)
        {
            this.table = table;
            this.filepath = Constants.ReportSourceFolder + "\\" + table.name + "Source.cs";
            this.filepathget = Constants.ReportGetSourceFolder + "\\" + table.name + "Source.cs";
        }

        public bool WriteReportSourceFile()
        {
            try
            {
                FillReportSourceClassContent();
                return true;
            }
            catch (Exception ex)
            {

            }
            return false;

        }
        public bool WriteGetReportSourceFile()
        {
            try
            {
                FillGetReportSourceClassContent();
                return true;
            }
            catch (Exception ex)
            {

            }
            return false;

        }
        private void FillGetReportSourceClassContent()
        {
            string str = File.ReadAllText("WebReport//GetSource.txt", Encoding.GetEncoding("iso-8859-9"));
            StreamWriter file = new StreamWriter(filepathget, false, Encoding.UTF8);//(filepath);
            str = str.Replace("#tName", table.name).Replace("#pColType", table.primery_column.dataType).Replace("#pColName", table.primery_column.name).Replace("#filters", GetFilters()).Replace("#countFilt", GetCountFilters());
            file.Write(str);
            file.Close();
        }
        private void FillReportSourceClassContent()
        {
            string str = File.ReadAllText("WebReport//Source.txt", Encoding.GetEncoding("iso-8859-9"));
            StreamWriter file = new StreamWriter(filepath, false, Encoding.UTF8);//(filepath);
            str = str.Replace("#tName", table.name).Replace("#pColType", table.primery_column.dataType).Replace("#pColName", table.primery_column.name).Replace("#projName", GetMainName()).Replace("#filters", GetFilters()).Replace("#selects", GetSelects()).Replace("#countFilt", GetCountFilters());
            file.Write(str);
            file.Close();
        }
        Dictionary<string, string> mainTableNameList = new Dictionary<string, string>() {
            {"Al","Alarm"},
            {"Ap","App"},
            {"Ca","Cash"},
            {"Ch","Cashier"},
            {"Co","Comp"},
            {"Cf","Config"},
            {"Cc","CreditCard"},
            {"Cu","Cust"},
            {"De","Device"},
            {"Dm","DeviceMaint"},
            {"En","Endpoint"},
            {"Eo","Eod"},
            {"Ev","EventLog"},
            {"In","Intro"},
            {"Ki","Kiosk"},
            {"La","Lang"},
            {"Ma","Master"},
            {"Me","Media"},
            {"Mn","Maint"},
            {"Pr","Print"},
            {"Rc","Reco"},
            {"Rp","Repo"},
            {"Ri","Right"},
            {"Tr","Trnx"},
            {"Us","User"}
        };
        private string GetMainName()
        {
            foreach (var item in mainTableNameList)
            {
                if (table.name.Substring(0, 2) == item.Key)
                    return item.Value;
            }
            return string.Empty;
        }
        private string GetFilters()
        {
            string filter = File.ReadAllText("WebReport//SourceFilter.txt", Encoding.GetEncoding("iso-8859-9"));
            string temp = "";
            string content = "";
            foreach (var item in table.columns)
            {
                if ((item.dataType.Contains("int") || item.dataType.Contains("decimal")) && (item.name.Contains("id") || item.name.Contains("Id")))
                {
                    temp = filter;
                    if (item.dataType.Contains("?"))
                    {
                        content+=temp.Replace("#pColVal", item.name + ".Value").Replace("#pColName", item.name).Replace("#nullControl", "f." + item.name + ".HasValue && ");
                    }
                    else
                    {
                        content+= temp.Replace("#pColVal", item.name).Replace("#pColName", item.name).Replace("#nullControl", "");
                    }
                }
            }
            return content;
        }
        //
        private string GetSelects()
        {
            string filter = File.ReadAllText("WebReport//SourceFilterSelect.txt", Encoding.GetEncoding("iso-8859-9"));
            string temp = "";
            string content = "";
            foreach (var item in table.foreigns)
            {
                temp = filter;
                content += temp.Replace("#tName", item.table.name);
            }
            foreach (var item in table.ref_foreign)
            {
                temp = filter;
                content += temp.Replace("#tName", item.table.name + "list");
            }
            return content;
        }
        private string GetCountFilters()
        {
            string filter = File.ReadAllText("WebReport//SourceFilterCount.txt", Encoding.GetEncoding("iso-8859-9"));
            string temp = "";
            string content = "";
            foreach (var item in table.columns)
            {
                if ((item.dataType.Contains("int") || item.dataType.Contains("decimal")) && (item.name.Contains("id") || item.name.Contains("Id")))
                {
                    temp = filter;
                    content += temp.Replace("#pColName", item.name);
                }
            }
            return content;
        }
    }
}
