﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Avs.Win.Generate
{
    public class MenuManager
    {
        public List<_Menu> menulist { get; set; }
        public List<Roles> rolelist { get; set; }

        public MenuManager()
        {
            rolelist = new List<Roles>();
            menulist = new List<_Menu>();
        }
    }
}
