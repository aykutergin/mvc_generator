﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Avs.Win.Generate
{
    public class SubMenu
    {
        public string name { get; set; }
        public string action { get; set;}
        public string controller { get; set; }

        public SubMenu(string name, string action, string controller)
        {
            this.name = name;
            this.action = action;
            this.controller = controller;
        }
    }
}
