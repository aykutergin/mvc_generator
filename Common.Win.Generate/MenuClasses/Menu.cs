﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Avs.Win.Generate
{
    public class _Menu
    {
        
        public string name { get; set; }
        public string action { get; set; }
        public string controller { get; set; }
        public List<SubMenu> submenulist { get; set; }

        public _Menu(string name, string action, string controller)
        {
            this.name = name;
            this.action = action;
            this.controller = controller;
            submenulist = new List<SubMenu>(); 
        }
        public _Menu()
        {
            submenulist = new List<SubMenu>();
        }
    }
}
