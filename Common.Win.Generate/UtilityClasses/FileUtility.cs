﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Avs.Win.Generate
{
    public class FileUtility
    {
        public string inputDefault { get; set; }
        public string inputDate { get; set; }
        public string inputCheckBox { get; set; }
        public string inputDropdown { get; set; }
        public string inputEmail { get; set; }
        public string inputPhone { get; set; }
        public string inputFile { get; set; }
        public string inputImage { get; set; }
        public string inputLink { get; set; }
        public string inputOnlyNumber { get; set; }
        public string inputPassword { get; set; }
        public string inputTextArea { get; set; }
        public string inputHidden { get; set; }
        public string validationDefault { get; set; }
        public string validationOnlyNumber { get; set; }
        public string validationEmail { get; set; }
        public string validationPhone { get; set; }
        public string validationLink { get; set; }
        public string validationTextArea { get; set; }
        public string scriptDate { get; set; }
        public string scriptEmail { get; set; }
        public string scriptPhone { get; set; }
        public string scriptImage { get; set; }
        public string scriptFile { get; set; }

        public FileUtility()
        {
            GetInputsFromFile();
            GetValidationsFromFile();
            GetScriptsFromFile();
        }
        private void GetInputsFromFile()
        {
            inputDefault = File.ReadAllText("WebReport//Views//Inputs//InputDefault.txt", Encoding.GetEncoding("iso-8859-9"));
            inputDate = File.ReadAllText("WebReport//Views//Inputs//Date.txt", Encoding.GetEncoding("iso-8859-9"));
            inputCheckBox = File.ReadAllText("WebReport//Views//Inputs//CheckBox.txt", Encoding.GetEncoding("iso-8859-9"));
            inputDropdown = File.ReadAllText("WebReport//Views//Inputs//Dropdown.txt", Encoding.GetEncoding("iso-8859-9"));
            inputEmail = File.ReadAllText("WebReport//Views//Inputs//Email.txt", Encoding.GetEncoding("iso-8859-9"));
            inputPhone = File.ReadAllText("WebReport//Views//Inputs//Phone.txt", Encoding.GetEncoding("iso-8859-9"));
            inputFile = File.ReadAllText("WebReport//Views//Inputs//File.txt", Encoding.GetEncoding("iso-8859-9"));
            inputImage = File.ReadAllText("WebReport//Views//Inputs//Image.txt", Encoding.GetEncoding("iso-8859-9"));
            inputLink = File.ReadAllText("WebReport//Views//Inputs//Link.txt", Encoding.GetEncoding("iso-8859-9"));
            inputOnlyNumber = File.ReadAllText("WebReport//Views//Inputs//OnlyNumber.txt", Encoding.GetEncoding("iso-8859-9"));
            inputPassword = File.ReadAllText("WebReport//Views//Inputs//Password.txt", Encoding.GetEncoding("iso-8859-9"));
            inputTextArea = File.ReadAllText("WebReport//Views//Inputs//TextArea.txt", Encoding.GetEncoding("iso-8859-9"));
            inputHidden = File.ReadAllText("WebReport//Views//Inputs//Hidden.txt", Encoding.GetEncoding("iso-8859-9"));
        }
        private void GetValidationsFromFile()
        {
            validationDefault = File.ReadAllText("WebReport//Views//Validations//ValidationDefault.txt", Encoding.GetEncoding("iso-8859-9"));
            validationEmail = File.ReadAllText("WebReport//Views//Validations//Email.txt", Encoding.GetEncoding("iso-8859-9"));
            validationPhone = File.ReadAllText("WebReport//Views//Validations//Phone.txt", Encoding.GetEncoding("iso-8859-9"));
            validationLink = File.ReadAllText("WebReport//Views//Validations//Link.txt", Encoding.GetEncoding("iso-8859-9"));
            validationOnlyNumber = File.ReadAllText("WebReport//Views//Validations//OnlyNumber.txt", Encoding.GetEncoding("iso-8859-9"));
            validationTextArea = File.ReadAllText("WebReport//Views//Validations//TextArea.txt", Encoding.GetEncoding("iso-8859-9"));
        }

        private void GetScriptsFromFile()
        {
            scriptDate = File.ReadAllText("WebReport//Views//Scripts//Date.txt", Encoding.GetEncoding("iso-8859-9"));
            scriptEmail = File.ReadAllText("WebReport//Views//Scripts//Email.txt", Encoding.GetEncoding("iso-8859-9"));
            scriptPhone = File.ReadAllText("WebReport//Views//Scripts//Phone.txt", Encoding.GetEncoding("iso-8859-9"));
            scriptImage = File.ReadAllText("WebReport//Views//Scripts//Image.txt", Encoding.GetEncoding("iso-8859-9"));
            scriptFile = File.ReadAllText("WebReport//Views//Scripts//File.txt", Encoding.GetEncoding("iso-8859-9"));
        }

        public static void MoveClasses()
        {
            //string path=Constants.rootPath+"/Classes";
            //FileInfo fileinfo=new FileInfo(path);
            //if(fileinfo.Exists)
            //    Directory.CreateDirectory(path);
            //string[] files = Directory.GetFiles("Classes");
            //foreach (string item in files)
            //{
            //    if(File.Exists(Path.Combine(path,
            //    File.Copy(item, path);
            //}
        }
    }
}
