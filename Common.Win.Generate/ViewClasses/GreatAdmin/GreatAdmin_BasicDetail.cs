﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Avs.Win.Generate
{
    class GreatAdmin_BasicDetail:IView
    {
        public Table table { get; set; }
        public string filePath { get; set; }

        public GreatAdmin_BasicDetail(Table table)
        {
           this.table= table;
           this.filePath = Constants.ViewTableFolder + "\\Details.cshtml";
        }
        #region IView Members

        public void Write()
        {
            StreamWriter file = new StreamWriter(filePath,false, Encoding.UTF8);//(filePath);

            if (table.tabletype == TableType.Default)
            {
                #region title- model -layout

                file.WriteLine("@model myLibrary.Models."+table.name+"Model");
                file.WriteLine("@{");
                file.WriteLine("ViewBag.Title = \"" + table.primery_column.description + "\";");
                file.WriteLine("Layout = \"~/Views_B/Shared/_AdminLayout.cshtml\";");
                file.WriteLine("string RedirectUrl;");
                file.WriteLine("if (HttpContext.Current.Request.UrlReferrer != null)");
                file.WriteLine("{");
                file.WriteLine("RedirectUrl = HttpContext.Current.Request.UrlReferrer.OriginalString;");
                file.WriteLine("}");
                file.WriteLine("else");
                file.WriteLine("{");
                file.WriteLine("RedirectUrl = Url.Action(\"Index\");");
                file.WriteLine("}");
                file.WriteLine("}");
                file.WriteLine("");


                /**/
            file.WriteLine("<script type=\"text/javascript\">");

file.WriteLine("    $(document).ajaxComplete(function () {");
     file.WriteLine("   $(\".fancyopener\").fancybox({");
           file.WriteLine("'titlePosition': 'inside',");
            file.WriteLine("'transitionIn': 'none',");
            file.WriteLine("'transitionOut': 'none'");
        file.WriteLine("});");
    file.WriteLine("})");

    file.WriteLine("</script>");
                /**/
                
                #endregion

                ViewManager.WriteHeadLine(file, table);

                ViewManager.WriteDetail(file, table);

                file.WriteLine("<p>");
                file.WriteLine("@Html.ActionLink(\"Düzenle\", \"Edit\", new { id = Model."+table.primery_column.name+" }) | <a href=\"@RedirectUrl\">Geri Dön</a>");
                file.WriteLine("</p>");

            }
            else if (table.tabletype == TableType.Image)
            {
                //image view
            }
            else if (table.tabletype == TableType.File)
            {
                //file view
            }
            file.Close();

        }

        #endregion
    }
}
