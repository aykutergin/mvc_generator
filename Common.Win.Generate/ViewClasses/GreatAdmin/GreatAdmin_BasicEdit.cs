﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Avs.Win.Generate
{
    class GreatAdmin_BasicEdit:IView
    {
         public Table table { get; set; }
         public string filePath { get; set; }

         public GreatAdmin_BasicEdit(Table table)
        {
           this.table= table;
           this.filePath = Constants.ViewTableFolder + "\\Edit.cshtml";
        }
        #region IView Members

         public void Write()
         {
             StreamWriter file = new StreamWriter(filePath,false, Encoding.UTF8);//(filePath);

           
                 file.WriteLine("@model myLibrary.Models." + table.name + "Model");
                 file.WriteLine("@{");
                 file.WriteLine("ViewBag.Title = \"" + table.primery_column.description + " İçin Yeni Kayıt\";");
                 file.WriteLine("Layout = \"~/Views_B/Shared/_AdminLayout.cshtml\";");
                 file.WriteLine("string RedirectUrl;");
                 file.WriteLine("if (HttpContext.Current.Request.UrlReferrer != null)");
                 file.WriteLine("{");
                 file.WriteLine("RedirectUrl = HttpContext.Current.Request.UrlReferrer.OriginalString;");
                 file.WriteLine("}");
                 file.WriteLine("else");
                 file.WriteLine("{");
                 file.WriteLine("RedirectUrl = Url.Action(\"Index\");");
                 file.WriteLine("}");
                 file.WriteLine("}");
                 file.WriteLine("");
                 file.WriteLine("<div class=\"box\">");
                 WriteHeadLine(file, table, "İçin Düzenleme");
                 file.WriteLine("<div class=\"box-content\">");
                 WriteEditor(file, table,"Edit");
                 file.WriteLine("</div>");
                 file.WriteLine("</div>");

            
             file.Close();
         }

         public void WriteHeadLine(StreamWriter file, Table table, string header)
         {
             file.WriteLine("");
             file.WriteLine("<div class=\"headlines\">");
             file.WriteLine("<h2>");
             file.WriteLine("<span>" + table.primery_column.description + " " + header + "</span></h2>");
             file.WriteLine("<a href=\"#help\" class=\"help\"></a>");
             file.WriteLine("</div>");
             file.WriteLine("");
         }

         public void WriteEditor(StreamWriter file, Table table, string action)
         {
             file.WriteLine(" @using (Html.BeginForm(\"" + action + "\", \"Admin" + table.name + "\", new { RedirectUrl = RedirectUrl }, FormMethod.Post, new { @class = \"formBox\" }))");
             file.WriteLine("{");
             file.WriteLine(" <div style=\"padding: 10px\" class=\"framed\"> ");
            
             file.WriteLine("<div><p>Bilgilerinizi Eksiksiz ve Doğru Bir Biçimde Giriniz.</p></div>");

             file.WriteLine("  <div class=\"columns\">");
             file.WriteLine("       <div class=\"six-columns\">");

             int i = table.columns.Count;
             int y = 0;

             foreach (Column item in table.columns)
             {
                 if (item.name.StartsWith("Res"))
                   i--;
                 
             }
             foreach (Column item in table.columns)
             {
                 y++;
                 if (item.name.StartsWith("Res"))
                 {
                     // sutun sayısını azaltki doğru yerden ikiye bölsün.
                
                     continue;
                 }
                 switch (item.columntype)
                 {
                     case ColumnType.Date:
                         file.WriteLine("<div class=\"clearfix\">");
                         file.WriteLine("<div class=\"lab\"><label for=\"input-one\">@Html.LabelFor(model => Model." + item.name + ")<span>*</span></label></div>");
                         file.WriteLine("<div class=\"con\">@Html.TextBoxFor(model => Model." + item.name + ", new { @class = \"input datepicker\", id = \"input-one\" })");
                         file.WriteLine("@Html.ValidationMessageFor(model => model." + item.name + ")");
                         file.WriteLine("</div>");
                         file.WriteLine("</div>");
                         //write datepicker
                         break;
                     case ColumnType.Text:
                     case ColumnType.Name:
                     case ColumnType.Email:
                     case ColumnType.Url:
                         file.WriteLine("<div class=\"clearfix\">");
                         file.WriteLine("<div class=\"lab\"><label for=\"input-one\">@Html.LabelFor(model => Model." + item.name + ")<span>*</span></label></div>");
                         file.WriteLine("<div class=\"con\">@Html.TextBoxFor(model => Model." + item.name + ", new { @class = \"input\", id = \"input-one\" })");
                         file.WriteLine("@Html.ValidationMessageFor(model => model." + item.name + ")</div>");
                         file.WriteLine("</div>");
                         //write textbox
                         break;
                     case ColumnType.TextArea:
                         file.WriteLine("<div class=\"clearfix\">");
                         file.WriteLine("<div class=\"lab\"><label for=\"textarea-one\">@Html.LabelFor(model => Model." + item.name + ")<span>*</span></label></div>");
                         file.WriteLine("<div class=\"con\">@Html.TextAreaFor(model => Model." + item.name + ", new { @class = \"textarea\", id = \"textarea-one\" })");
                         file.WriteLine("@Html.ValidationMessageFor(model => model." + item.name + ")");
                         file.WriteLine(" </div>");
                         file.WriteLine("</div>");
                         //write textarea
                         break;
                     case ColumnType.HtmlText:
                         file.WriteLine("<div class=\"clearfix\">");
                         file.WriteLine("<div class=\"lab\"><label for=\"input textarea-wysiwyg\">@Html.LabelFor(model => Model." + item.name + ")<span>*</span></label></div>");
                         file.WriteLine("<div class=\"confull\">@Html.TextAreaFor(model => Model." + item.name + ", new { @class = \"input wysiwyg\", id = \"input-one\" })");
                         file.WriteLine("@Html.ValidationMessageFor(model => model." + item.name + ")");
                         file.WriteLine("</div></div>");
                         //html editor işlemleri
                         break;
                     case ColumnType.CheckBox:
                         file.WriteLine("<div class=\"clearfix\">");
                         file.WriteLine("<div class=\"lab\"><label for=\"input-one checkbox\">@Html.LabelFor(model => Model." + item.name + ")<span>*</span></label></div>");
                         file.WriteLine("<div class=\"concheck\">@Html.CheckBoxFor(model => Model." + item.name + ", new { @class = \"input checkbox\", id = \"input-one\" })");
                         file.WriteLine("@Html.ValidationMessageFor(model => model." + item.name + ")");
                         file.WriteLine("</div></div>");
                         //write checkbox
                         break;
                     case ColumnType.ComboBox:
                         
                             file.WriteLine("<div class=\"clearfix\">");
                             file.WriteLine("<div class=\"lab\"><label for=\"input-one\">@Html.LabelFor(model => Model." + item.name + ")<span>*</span></label></div>");
                             file.WriteLine("<div class=\"con\">@Html.DropDownListFor(model => Model." + item.name + ", ViewBag." + item.name + " as SelectList, new { @class = \"input\", id = \"input-one\" })");
                             file.WriteLine("@Html.ValidationMessageFor(model => model." + item.name + ")");
                             file.WriteLine("</div></div>");
                         
                         //combobox işlemleri
                         break;
                     default:
                         break;
                 }
                 if (y == (i/2))
                 {
                     file.WriteLine("     </div>");
                  file.WriteLine("<div class=\"six-columns\">");
                 }
             }


             file.WriteLine("<div class=\"btn-submit\"><!-- Submit form -->");
             file.WriteLine("<input type=\"submit\" value=\"Ekle\" class=\"button\" />");
             file.WriteLine("ya da <a href=\"@RedirectUrl \" class=\"cancel\">Geri Dön</a>");
             file.WriteLine("</div>");
             file.WriteLine("</div>");
             file.WriteLine("</div>");

             file.WriteLine("</div>");
             file.WriteLine("}");
         }

        #endregion
    }
}
