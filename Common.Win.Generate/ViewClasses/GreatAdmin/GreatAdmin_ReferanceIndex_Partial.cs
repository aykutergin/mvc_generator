﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Avs.Win.Generate
{
    class GreatAdmin_ReferanceIndex_Partial:IView
    {
         public Table table { get; set; }
         public string filePath { get; set; }
         //public ForeignKey foreign { get; set; }

         public GreatAdmin_ReferanceIndex_Partial(Table table)
        {
           this.table= table;
           //this.foreign = foreign;
           this.filePath = Constants.ViewTableFolder + "\\_" + table.name + ".cshtml";
        }
        #region IView Members

        public void Write()
        {
            StreamWriter file = new StreamWriter(filePath,false, Encoding.UTF8);//(filePath);

            if (table.tabletype == TableType.Default)
            {
                #region title- model -layout
                file.WriteLine("@using MvcPaging");
                file.WriteLine("@using myLibrary.Classes");
                file.WriteLine("@model IPagedList<myLibrary.Models."+table.name+"Model>");
                file.WriteLine("@{");
                file.WriteLine("ViewBag.Title = \"" + table.primery_column.description + "\";");
                file.WriteLine("Layout = null;");
                file.WriteLine("string RedirectUrl;");
                file.WriteLine("string CurrentUrl = HttpContext.Current.Request.Url.OriginalString;");
                file.WriteLine("if (HttpContext.Current.Request.UrlReferrer != null)");
                file.WriteLine("{");
                file.WriteLine("RedirectUrl = HttpContext.Current.Request.UrlReferrer.OriginalString;");
                file.WriteLine("}");
                file.WriteLine(" else");
                file.WriteLine("{");
                file.WriteLine("RedirectUrl = Url.Action(\"Index\");");
                file.WriteLine("}");
                file.WriteLine("RouteValueDictionary tempdic = RouteValueProvider.GetCurrentRouteValues(Url.RequestContext.RouteData.Values, Request.QueryString);");
                file.WriteLine("tempdic[\"action\"] = \"_" + table.name + "\";");
                file.WriteLine("tempdic[\"page\"] = \"1\";");
                file.WriteLine("}");
                file.WriteLine("");
                #endregion


                WriteScriptForBasic(file, table);
                WriteFaceUploadDiv(file, table);
                file.WriteLine("<div style=\"padding-left: 10px;\">@Html.ActionLink(\"Yeni " + table.primery_column.description + " Ekle\", \"Create\", \"Admin"+table.name+"\")</div>");
                file.WriteLine("");


                file.WriteLine("<table class=\"tab tab-drag\">");
                file.WriteLine("<tr class=\"top nodrop nodrag\">");
                WriteTableHeader(file, table);
                file.WriteLine("</tr>");
                
                file.WriteLine("@foreach (var item in Model)");
                file.WriteLine("{");
                file.WriteLine("<tr>");
                WriteTableData(file, table);
                file.WriteLine("</tr>");

              
                file.WriteLine("<tr style=\"height: 0px; overflow: hidden;\">");
                file.WriteLine("<td colspan=\"13\" style=\"border: 0px; padding: 0px; margin: 0px;\" class=\"SubDivTd\">");
                file.WriteLine("<div id=\"SubDiv@(item."+table.primery_column.name+")\" class=\"SubDiv\" style=\"height:0px; overflow:hidden; display:none;\">");
                foreach (ForeignKey foreign in table.ref_foreign)
                {
                    //file.WriteLine("<div id=\"pagingdiv@(item." + table.primery_column.name + "_" + foreign.table.name + " class=\"pagingdiv\" >");
                    file.WriteLine("<div id=\"pagingdiv@(item." + table.primery_column.name + " + \"_" + foreign.table.name + "Group\")\" class=\"pagingdiv\" >");
                    file.WriteLine("@Html.Action(\"_" + table.name + "" + foreign.table.name + "\", new { page = 1, id = item." + table.primery_column.name + ",OrderBy = \"\", OrderType = \"\"  })");
                    file.WriteLine("</div>");
                }
                
                file.WriteLine("</div>");
                file.WriteLine("</td>");
                file.WriteLine("</tr>");

                file.WriteLine("}");
                file.WriteLine("</table>");

                ViewManager.WriteClearFilter(file, table);

                ViewManager.WritePagination(file, table, "MainDiv");


            }

            else if (table.tabletype == TableType.Image)
            {
                //image view
            }
            else if (table.tabletype == TableType.File)
            {
               //file view
            }

            file.Close();

        }

        public void WriteScriptForBasic(StreamWriter file, Table table)
        {
            file.WriteLine("<script type=\"text/javascript\">");
            file.WriteLine("function areyousure() {");
            file.WriteLine("return confirm(\"Bu " + table.primery_column.description + " silmek istediğinize eminmisiniz?\");");
            file.WriteLine("}");
            bool isfile = false;
            foreach (Column item in table.columns)
            {
                if (item.columntype == ColumnType.Image || item.columntype == ColumnType.File)
                {
                    isfile = true;
                    file.WriteLine("function set" + item.name + "ID(id) {");
                    file.WriteLine("document.getElementById(\""+table.name + item.name + "PrimaryID\").value = id;");
                    file.WriteLine("}");
                }
            }
            foreach (ForeignKey foreign in table.ref_foreign)
            {
                foreach (Column item in foreign.table.columns)
                {
                    if (item.columntype == ColumnType.Image || item.columntype == ColumnType.File)
                    {
                        isfile = true;
                        file.WriteLine("function set" + item.name + "ID_"+foreign.table.name+"(id) {");
                        file.WriteLine("document.getElementById(\"" + table.name + foreign.table.name + item.name + "PrimaryID\").value = id;");
                        file.WriteLine("}");
                    }
                }

            }
            if (isfile)
            {
                file.WriteLine("$(document).ready(function () {");

                file.WriteLine("$(\".picuploader\").fancybox({");
                file.WriteLine("'titlePosition': 'inside',");
                file.WriteLine("'transitionIn': 'none',");
                file.WriteLine("'transitionOut': 'none'");
                file.WriteLine("});");
                file.WriteLine("});");
            }
            file.WriteLine("</script>");


        }
        public void WriteFaceUploadDiv(StreamWriter file, Table table)
        {
            foreach (Column item in table.columns)
            {
                if (item.columntype == ColumnType.Image || item.columntype == ColumnType.File)
                {
                    file.WriteLine("<div style=\"display: none;\">");
                    file.WriteLine("<div id=\"uploadpic" + item.name + "\">");
                    file.WriteLine("@using (Html.BeginForm(\"_Upload" + item.name + "\", \"Admin" + table.name + "\", new { RedirectUrl = RedirectUrl }, FormMethod.Post, new { enctype = \"multipart/form-data\" }))");
                    file.WriteLine("{");
                    file.WriteLine("<div>");
                    file.WriteLine(item.description + " :");
                    file.WriteLine("<input type=\"file\" name=\"file" + item.name + "\" id=\"resim\" />");
                    file.WriteLine("<input id=\""+table.name + item.name + "PrimaryID\" name=\"PrimaryID\" type=\"hidden\" value=\"1\" />");
                    file.WriteLine("<input type=\"submit\" name=\"Onayla\" value=\"Onayla\" />");
                    file.WriteLine("</div>  ");
                    file.WriteLine("}");
                    file.WriteLine("</div>");
                    file.WriteLine("</div>");
                }
            }

            foreach (ForeignKey foreign in table.ref_foreign)
            {
                foreach (Column item in foreign.table.columns)
                {
                    if (item.columntype == ColumnType.Image || item.columntype == ColumnType.File)
                    {
                        file.WriteLine("<div style=\"display: none;\">");
                        file.WriteLine("<div id=\"uploadpic" + item.name +"_"+foreign.table.name+ "\">");
                        file.WriteLine("@using (Html.BeginForm(\"_Upload" + item.name + "\", \"Admin" + foreign.table.name + "\", new { RedirectUrl = CurrentUrl }, FormMethod.Post, new { enctype = \"multipart/form-data\" }))");
                        file.WriteLine("{");
                        file.WriteLine("<div>");
                        file.WriteLine(item.description + " :");
                        file.WriteLine("<input type=\"file\" name=\"file" + item.name + "\" id=\"resim\" />");
                        file.WriteLine("<input id=\"" + table.name +foreign.table.name+ item.name + "PrimaryID\" name=\"PrimaryID\" type=\"hidden\" value=\"1\" />");
                        file.WriteLine("<input type=\"submit\" name=\"Onayla\" value=\"Onayla\" />");
                        file.WriteLine("</div>  ");
                        file.WriteLine("}");
                        file.WriteLine("</div>");
                        file.WriteLine("</div>");
                    }
                }
                
            }
        }

        public void WriteTableHeader(StreamWriter file, Table table)
        {
            file.WriteLine("<th class=\"dragHandle\">");
            file.WriteLine("&nbsp;");
            file.WriteLine("</th>");
           
                file.WriteLine("<th>");
                file.WriteLine("</th>");
           
            foreach (Column item in table.columns)
            {
                switch (item.columntype)
                {
                    case ColumnType.Image:
                        file.WriteLine("<th>");
                        file.WriteLine(item.description);
                        file.WriteLine("</th>");
                        break;
                    case ColumnType.File:
                        file.WriteLine("<th>");
                        file.WriteLine(item.description);
                        file.WriteLine("</th>");
                        break;
                    default:
                        break;
                }
            }
            foreach (Column item in table.columns)
            {
                if (item.columntype == ColumnType.HtmlText) continue;
                if (item.columntype == ColumnType.TextArea) continue;
                if (item.columntype == ColumnType.Primery) continue;
                if (item.columntype == ColumnType.Image) continue;
                if (item.columntype == ColumnType.File) continue;

                file.WriteLine("<th>");
                switch (item.columntype)
                {
                    case ColumnType.Date:
                    case ColumnType.ComboBox:
                    case ColumnType.CheckBox:
                    case ColumnType.Name:

                        file.WriteLine("@{");
                        file.WriteLine("RouteValueProvider.AddValue(tempdic, \"OrderBy\", \"" + item.name + "\");");
                        file.WriteLine("RouteValueProvider.AddValue(tempdic, \"OrderType\", \"asc\");");
                        file.WriteLine("if (ViewBag.OrderBy != null) { if (ViewBag.OrderBy == \"" + item.name + "\") { RouteValueProvider.AddValue(tempdic, \"OrderType\", \"desc\"); } }");
                        file.WriteLine("}");
                        string title = (item.description == String.Empty) ? item.name : item.description;
                        file.WriteLine("@Ajax.ActionLink(\"" + title + "\", \"_" + table.name + "\", tempdic, new AjaxOptions { UpdateTargetId = \"MainDiv\" })");

                        break;

                    default:
                        file.WriteLine(item.description);
                        break;
                }
                file.WriteLine("</th>");
            }

            file.WriteLine(" <th class=\"action\">");
            file.WriteLine("İşlemler");
            file.WriteLine("</th>");
        }

        public static void WriteTableData(StreamWriter file, Table table)
        {
            file.WriteLine("<td class=\"dragHandle\">");
            file.WriteLine("&nbsp;");
            file.WriteLine("</td>");

           
                file.WriteLine("<td>");
                file.WriteLine("<a style=\"font-size:24px; cursor:pointer;  color:Blue;\" title=\"Ayrıntıları Görmek İçin Tıklayın.\" onclick=\"openCloseDiv(@item." + table.primery_column.name + ")\">+</a>");
                file.WriteLine("</td>");
           
            foreach (Column item in table.columns)
            {
                switch (item.columntype)
                {
                    case ColumnType.Image:
                        file.WriteLine("<td>");
                        file.WriteLine("<a class=\"picuploader\" href=\"#uploadpic" + item.name + "\" onclick=\"set" + item.name + "ID(@item." + table.primery_column.name + ")\">");
                        file.WriteLine("@if(item."+item.name+"!=null)");
                        file.WriteLine("{");

                        file.WriteLine("<img src=\"@Url.Content(item." + item.name + ")\" style=\"width:50px; height:50px;\" alt=\"\" />");
                        file.WriteLine("}");
                        file.WriteLine("else");
                        file.WriteLine("{");
                        file.WriteLine("<img src=\"@Url.Content(\"~/Content/default.jpg\")\" style=\"width:50px; height:50px;\" alt=\"\" />");
                        file.WriteLine("}");

file.WriteLine("</a>");
                        file.WriteLine("</td>");
                        break;
                    case ColumnType.File:
                        file.WriteLine("<td>");
                        file.WriteLine("<a class=\"picuploader\" href=\"#uploadpic" + item.name + "\" onclick=\"set" + item.name + "ID(@item." + table.primery_column.name + ")\">");
                        file.WriteLine("<img src=\"@Url.Content(\"~/Content/images/default.jpg\")\" style=\"width:24px; height:24px;\" alt=\"\" />");
                        file.WriteLine("</a>");
                        file.WriteLine("@if (item." + item.name + " != null) ");
                        file.WriteLine("{");
                        file.WriteLine("<a href=\"@Url.Content(item." + item.name + ")\" target=\"_blank\" >@item." + item.name + ".Substring(item." + item.name + ".LastIndexOf('/')+1)</a> ");
                        file.WriteLine("}");
                        file.WriteLine("</td>");
                        break;
                    default:
                        break;
                }
            }
            foreach (Column item in table.columns)
            {
                if (item.columntype == ColumnType.HtmlText) continue;
                if (item.columntype == ColumnType.TextArea) continue;
                if (item.columntype == ColumnType.Primery) continue;
                if (item.columntype == ColumnType.Image) continue;
                if (item.columntype == ColumnType.File) continue;

                file.WriteLine("<td>");
                if (item.isForeign)
                    file.WriteLine("@item._" + item.foreign.table.name + ".Name");
                else
                    file.WriteLine("@item." + item.name);
                file.WriteLine("</td>");
            }

            file.WriteLine(" <td class=\"action\">");

            file.WriteLine("<a href=\"@Url.Action(\"Delete\", \"Admin" + table.name + "\", new { id = item." + table.primery_column.name + ",RedirectUrl = HttpContext.Current.Request.Url.OriginalString })\" onclick=\"delete" + table.name + "()\" class=\"ico ico-delete\" title=\"Sil\">");
                file.WriteLine("Sil</a> <a href=\"@Url.Action(\"Edit\", \"Admin" + table.name + "\", new { id = item." + table.primery_column.name + " })\" class=\"ico ico-edit\" title=\"Düzenle\">");
                file.WriteLine("Düzenle</a> <a href=\"@Url.Action(\"Details\", \"Admin" + table.name + "\", new { id = item." + table.primery_column.name + " })\" class=\"ico ico-detail\" title=\"Ayrıntılar\">");
                file.WriteLine("Detay</a>");
           
           
            file.WriteLine("</td>");
        }
        #endregion
    }
}
