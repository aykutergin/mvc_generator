﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Avs.Win.Generate
{
    class GreatAdmin_HomeIndex:IView
    {
        public string filePath { get; set; }
        public GreatAdmin_HomeIndex()
        {
            this.filePath = Constants.ViewTableFolder + "\\Index.cshtml";
        }
        #region IView Members

        public void Write()
        {
            MenuManager menu = Constants.menumanager;
            StreamWriter file = new StreamWriter(filePath,false, Encoding.UTF8);//(filePath);
            //Menu ile işlem yap
            file.WriteLine("@{");
            file.WriteLine("    ViewBag.Title = \"Index\";");
            file.WriteLine("    Layout = \"~/Views_B/Shared/_AdminLayout.cshtml\";");
            file.WriteLine("}");
            file.WriteLine("<div class=\"box\">");
            file.WriteLine("    <div class=\"headlines\">");
            file.WriteLine("        <h2>");
            file.WriteLine("            <span>Anasayfa</span></h2>");
            file.WriteLine("    </div>");

            file.WriteLine("    <div id=\"HomeDiv\">");
            
            int i = 0;
            foreach (_Menu item in menu.menulist)
            {
                if (item.submenulist.Count == 0)
                {
                    if (i == 1)
                    {
                        file.WriteLine("        <div id=\"References\">");
                    }

                    file.WriteLine("            <div class=\"Ref\">");
                    file.WriteLine("                <img src=\"@Url.Content(\"~/Content/images/defaultpro.jpg\")\" style=\"width:120px; height:120px\" alt=\"\" />");
                    file.WriteLine("                <p>");
                    file.WriteLine("                @Html.ActionLink(\"" + item.name + "\", \"" + item.action + "\", \"" + item.controller + "\",new { }, new {})");
                    file.WriteLine("                </p>");
                    file.WriteLine("            </div>");
                    if (i == menu.menulist.Count-1)
                    {
                        file.WriteLine("        </div>");
                    }
                }
                else
                {
                    
                    file.WriteLine("        <h4>");
                    file.WriteLine("            " + item.name + "</h4>");

                    file.WriteLine("        <div id=\"General\">");
                    file.WriteLine("            <img src=\"@Url.Content(\"~/Content/images/defaultpro.jpg\")\" width=120 height=120 alt=\"\" />");
                    file.WriteLine("            <ul>");
                    foreach (SubMenu submenu in item.submenulist)
                    {
                        file.WriteLine("                <li>@Html.ActionLink(\"" + submenu.name + "\",\"" + submenu.action + "\",\"" + submenu.controller + "\")</li>");
                    }
                    file.WriteLine("            </ul>");
                    file.WriteLine("        </div>");
                }
                i++;
            }
            file.WriteLine("    </div>");
            file.WriteLine("    <div class=\"clear\">");
            file.WriteLine("    </div>");
            file.WriteLine("</div>");
            file.Close();
        }

        #endregion

    }
}
