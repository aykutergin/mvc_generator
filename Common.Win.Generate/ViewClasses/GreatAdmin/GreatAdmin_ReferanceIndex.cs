﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Avs.Win.Generate
{
    class GreatAdmin_ReferanceIndex : IView
    {
        public Table table { get; set; }
        //public ForeignKey foreign { get; set; }
        public string filePath { get; set; }

        public GreatAdmin_ReferanceIndex(Table table)
        {
            this.table = table;
           // this.foreign = foreign;
            this.filePath = Constants.ViewTableFolder + "\\Index.cshtml";
        }
        #region IView Members

        public void Write()
        {
            StreamWriter file = new StreamWriter(filePath,false, Encoding.UTF8);//(filePath);
            //ortak satırlar
            if (table.name.ToLower().Contains("imageurl"))
            {
                //image index view
            }
            else if (table.name.ToLower().Contains("fileurl"))
            {
                //file index view
            }
            else
            {
                #region title- model -layout

                // ViewManager.WriteWiewHeader(file, "@model IPagedList<myLibrary.Models."+table.name+"Model>");
                file.WriteLine("@using MvcPaging");
                file.WriteLine("@model IPagedList<myLibrary.Models."+table.name+"Model>");
                file.WriteLine("@{");
                file.WriteLine("ViewBag.Title = \"" + table.primery_column.description + "\";");
                file.WriteLine("Layout = \"~/Views_B/Shared/_AdminLayout.cshtml\";");
                file.WriteLine("}");
                file.WriteLine("");

                #endregion

                WriteScript(file, table);

                file.WriteLine("<div style=\"display: none;\">");
                file.WriteLine("<div id=\"AddingDiv\" style=\"width: 400px; height: 100px; overflow: auto;\">");
                file.WriteLine("</div>");
                file.WriteLine("</div>");

                 file.WriteLine("<div class=\"box\">");

                WriteHeadLine(file, table);

                WriteFilter(file, table);

                #region action page
                WriteActionDiv(file, table);
                #endregion

                file.WriteLine("</div>");

                file.Close();
                //index view
            }

        }
        public void WriteScript(StreamWriter file, Table table)
        {
            file.WriteLine("<script type=\"text/javascript\">");
            file.WriteLine("     function setCookie(c_name, value, exdays) {");
        file.WriteLine(" var exdate = new Date();");
     file.WriteLine("    exdate.setDate(exdate.getDate() + exdays);");
       file.WriteLine("  var c_value = escape(value) + ((exdays == null) ? \"\" : \"; expires=\" + exdate.toUTCString());");
     file.WriteLine("    document.cookie = c_name + \"=\" + c_value;");
    file.WriteLine(" }");
     file.WriteLine("function getCookie(c_name) {");
     file.WriteLine("    var i, x, y, ARRcookies = document.cookie.split(\";\");");
        file.WriteLine(" for (i = 0; i < ARRcookies.length; i++) {");
       file.WriteLine("      x = ARRcookies[i].substr(0, ARRcookies[i].indexOf(\"=\"));");
      file.WriteLine("       y = ARRcookies[i].substr(ARRcookies[i].indexOf(\"=\") + 1);");
        file.WriteLine("     x = x.replace(/^\\s+|\\s+$/g, \"\");");
       file.WriteLine("      if (x == c_name) {");
        file.WriteLine("         return unescape(y);");
     file.WriteLine("        }");
      file.WriteLine("   }");
    file.WriteLine(" }");
            file.WriteLine("function openCloseDiv(id) {");
            file.WriteLine("if (document.getElementById(\"SubDiv\" + id).style.height == \"auto\") {");
            file.WriteLine("document.getElementById(\"SubDiv\" + id).style.height = \"0px\";");
            file.WriteLine("document.getElementById(\"SubDiv\" + id).style.display = \"none\";");
            file.WriteLine("setCookie(\"OpenedDiv\", 0, 1);");
            file.WriteLine("}");
            file.WriteLine("else {");
     file.WriteLine("   $(\".SubDiv\").css(\"height\", \"0px\");");
file.WriteLine("    $(\".SubDiv\").css(\"display\", \"none\");");
  file.WriteLine("  setCookie(\"OpenedDiv\", id, 1);");
            file.WriteLine("document.getElementById(\"SubDiv\" + id).style.height = \"auto\";");
            file.WriteLine("document.getElementById(\"SubDiv\" + id).style.display = \"table\";");
            file.WriteLine("}");
            file.WriteLine("}");

            file.WriteLine("function delete" + table.name + "(id) {");
            file.WriteLine("if (con" + table.name + "(\"Bu Soruyu Silmek İstediğinize Emin misiniz?\")) {");
            file.WriteLine("window.location = '@Url.Action(\"Delete\", \"Admin" + table.name + "\")/' + id;");
            file.WriteLine("}");
            file.WriteLine("else return false;");
            file.WriteLine("}");

            foreach (ForeignKey item in table.ref_foreign)
            {
                file.WriteLine("function delete" + item.table.name + "(id) {");
                file.WriteLine("if (con" + table.name + "(\"Bu Seçeneği Silmek İstediğinize Emin misiniz?\")) {");
                file.WriteLine("window.location = '@Url.Action(\"Delete\", \"Admin" + table.name + "" + item.table.name + "\")/' + id;");
                file.WriteLine("}");
                file.WriteLine("else return false;");
                file.WriteLine("}");
            }

file.WriteLine("            $(document).ready(function () {");
file.WriteLine("    var id = getCookie(\"OpenedDiv\");");
file.WriteLine("  if (document.getElementById(\"SubDiv\" + id) != null) {");
 file.WriteLine("       document.getElementById(\"SubDiv\" + id).style.height = \"auto\";");
  file.WriteLine("      document.getElementById(\"SubDiv\" + id).style.display = \"table\";");
  file.WriteLine("  }");

file.WriteLine("});");

          
            file.WriteLine("</script>");
        }

        public void WriteHeadLine(StreamWriter file, Table table)
        {
            file.WriteLine("");
            file.WriteLine("<div class=\"headlines\">");
            file.WriteLine("<h2>");
            file.WriteLine("<span>" + table.primery_column.description + " İşlemleri</span></h2>");
            file.WriteLine("<a href=\"#\" class=\"show-filter\">filtreyi göster</a>");
            file.WriteLine("</div>");
            file.WriteLine("");
        }

        public void WriteFilter(StreamWriter file, Table table)
        {
            file.WriteLine("");
            file.WriteLine("<!-- filter -->");
            file.WriteLine("<div class=\"filter\">");
            file.WriteLine("@using (Ajax.BeginForm(\"_" + table.name + "\", \"Admin" + table.name + "References\", new AjaxOptions{ UpdateTargetId = \"MainDiv\" }))");
            file.WriteLine("{");

            foreach (Column item in table.columns)
            {
                switch (item.columntype)
                {

                    case ColumnType.Date:
                        file.WriteLine("<div class=\"filterlabel\">");
                        file.WriteLine("<span>İlk Tarih:</span></div>");
                        file.WriteLine("<div class=\"confilter\">");
                        file.WriteLine("<input style=\"width: 100px;\" type=\"text\" value=\"\" name=\"DateFirst" + item.name + "\" class=\"input datepicker\" />");
                        file.WriteLine("</div>");
                        file.WriteLine("<div class=\"filterlabel\">");
                        file.WriteLine("<span>Son Tarih: </span>");
                        file.WriteLine("</div>");
                        file.WriteLine("<div class=\"confilter\">");
                        file.WriteLine("<input type=\"text\" value=\"\" style=\"width: 100px;\" name=\"DateLast" + item.name + "\" class=\"input datepicker\" />");
                        file.WriteLine("</div>");
                        //date search
                        break;
                    case ColumnType.Name:
                        file.WriteLine("<div class=\"filterlabel\">");
                        file.WriteLine("<span>" + item.description + ":</span>");
                        file.WriteLine("</div>");
                        file.WriteLine("<div class=\"confilter\">");
                        file.WriteLine("<input type=\"text\" name=\"Name" + item.name + "\" class=\"input\" />");
                        file.WriteLine("</div>");
                        //name = search
                        break;
                    case ColumnType.CheckBox:
                        file.WriteLine("<div class=\"filterlabel\">");
                        file.WriteLine(item.description + ":</div> <div class=\"confilter\">");
                        file.WriteLine("<select name=\"Status" + item.name + "\" class=\"select\">");
                        file.WriteLine("<option value=\"\">Farketmez</option>");
                        file.WriteLine("<option value=\"true\">Aktif</option>");
                        file.WriteLine("<option value=\"false\">Pasif</option>");
                        file.WriteLine("</select>");
                        file.WriteLine("</div>");
                        //aktif - pasif
                        break;
                    case ColumnType.ComboBox:
                        file.WriteLine("<div class=\"filterlabel\">");
                        file.WriteLine("Grup:</div>");
                        file.WriteLine("<div class=\"confilter\">");
                        file.WriteLine("@Html.DropDownList(\"" + item.foreign.column.name + "\", ViewBag." + item.foreign.column.name + " as SelectList, new { @class = \"select\" })");
                        file.WriteLine("</div>");
                        //foreign search
                        break;
                    default:
                        break;
                }

                file.WriteLine("<div class=\"clear\"></div>");
              
            }
            file.WriteLine("<input type=\"submit\" value=\"Filtrele\" class=\"submit\" />");
            file.WriteLine("}");
            file.WriteLine("</div>");
            file.WriteLine("<!-- /filter -->");
            file.WriteLine("");

        }

        public void WriteActionDiv(StreamWriter file, Table table)
        {
            file.WriteLine("");
            file.WriteLine("<div id=\"MainDiv\">");
            file.WriteLine("@Html.Action(\"_" + table.name + "\")");
            file.WriteLine("</div>");
            file.WriteLine("");
        }

        #endregion
    }
}
