﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Avs.Win.Generate
{
    class GreatAdmin_ReferanceNew : IView
    {
        public Table table { get; set; }
        public ForeignKey foreign { get; set; }

        public GreatAdmin_ReferanceNew(Table table, ForeignKey foreign)
        {
            this.table = table;
            this.foreign = foreign;
        }
        #region IView Members

        public void Write()
        {
            //ortak satırlar
            if (table.name.ToLower().Contains("imageurl"))
            {
                //image index view
            }
            else if (table.name.ToLower().Contains("fileurl"))
            {
                //file index view
            }
            else
            {
                //index view
            }

        }

        #endregion
    }
}
