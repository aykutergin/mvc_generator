﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Avs.Win.Generate
{
    class GreatAdmin_ReferanceIndex_Partial_Refpart:IView
    {
         public Table table { get; set; }
         public string filePath { get; set; }
         public ForeignKey foreign { get; set; }

         public GreatAdmin_ReferanceIndex_Partial_Refpart(Table table, ForeignKey foreign)
        {
           this.table= table;
           this.foreign = foreign;
           this.filePath = Constants.ViewTableFolder + "\\_" + table.name +foreign.table.name+ ".cshtml";
        }
        #region IView Members

        public void Write()
        {
            StreamWriter file = new StreamWriter(filePath,false, Encoding.UTF8);//(filePath);

            if (foreign.table.tabletype == TableType.Default)
            {
                #region title- model -layout
                file.WriteLine("@using MvcPaging;");
                file.WriteLine("@model IPagedList<myLibrary.Models."+foreign.table.name+"Model>");
                file.WriteLine("@using myLibrary.Classes;");
                file.WriteLine("@{");
                file.WriteLine("Layout = null;");
                file.WriteLine("string imgurl=\"\";");
                file.WriteLine("RouteValueDictionary dic = RouteValueProvider.GetCurrentRouteValues(Url.RequestContext.RouteData.Values, Request.QueryString);");
                file.WriteLine("dic[\"action\"] = \"_" + table.name+foreign.table.name + "\";");
                file.WriteLine("RouteValueProvider.AddValue(dic, \"Page\", \"1\");");
                file.WriteLine("}");
                file.WriteLine("");
                #endregion

                file.WriteLine("<h4>" + foreign.table.primery_column.description + "</h4>");
              
                file.WriteLine("@Html.ActionLink(\"Yeni Kayıt Ekle\", \"CreateWith" + table.name + "\", \"Admin" + foreign.table.name + "\", new { id =ViewBag.ID }, new { title = \"Yeni Kayıt Ekle\" })");
                file.WriteLine("<table class=\"tab\">");
                file.WriteLine("<tr>");
                WriteTableHeader(file, table,foreign);
                file.WriteLine("</tr>");
                
                file.WriteLine("@foreach (var Option in Model)");
                file.WriteLine("{");
                file.WriteLine("<tr>");
                    WriteTableData(file, table,foreign);
                file.WriteLine("</tr>");

                file.WriteLine("}");
                file.WriteLine("</table>");
                WritePagination(file, table);


            }

            else if (foreign.table.tabletype == TableType.Image || foreign.table.tabletype == TableType.File)
            {
                #region title- model -layout
                file.WriteLine("@model IEnumerable<myLibrary.Models." + foreign.table.name + "Model>");
                file.WriteLine("@using myLibrary.Classes;");
                file.WriteLine("@{");
                file.WriteLine("Layout = null;");
                file.WriteLine("string current = HttpContext.Current.Request.Url.OriginalString;");
                file.WriteLine("}");
                file.WriteLine("");
                #endregion
                //image view
                if (!Directory.Exists(Constants.rootPath + "/Content/images/" + table.name))
                {
                    Directory.CreateDirectory(Constants.rootPath + "/Content/images/" + table.name);
                }
                
                file.WriteLine("<script type=\"text/javascript\">");
                file.WriteLine("$(document).ready(function() {");
                file.WriteLine("$(\"#fileuploader_"+foreign.table.name+"@(ViewBag.ID)\").fileUpload({");
                file.WriteLine("'uploader': '@Url.Content(\"~/Content/Admin/uploader.swf\")',");
                file.WriteLine("'cancelImg': '@Url.Content(\"~/Content/admin/images/ico/cancel.png\")',");
                
                file.WriteLine("'script': '@Url.Action(\"Upload_"+table.name+"\", \"Admin"+foreign.table.name+"\",new{id=ViewBag.ID})',");
                file.WriteLine("'folder': '@Url.Action(\"~/Content/images/"+table.name+"\")',");
                file.WriteLine("'fileDesc': 'Resim Dosyası',");
                if (foreign.table.tabletype == TableType.Image)
                {
                    file.WriteLine("'buttonText': 'Resim Ekle',");
                    file.WriteLine("'fileExt': '*.jpg;*.jpeg;*.gif;*.png',");
                }
                else
                {
                    file.WriteLine("'buttonText': 'Dosya Ekle',");
                    file.WriteLine("'fileExt': '*.doc;*.docx;*.pdf;*.xls;*.xlsx;*.ppt;*.pptx;*.txt;*.ini',");
                }
                file.WriteLine("'multi': true,");
                file.WriteLine("'auto': true,");
                file.WriteLine("'onComplete': function () { window.location.reload(true) }");
                file.WriteLine("});");
                file.WriteLine("}");
                file.WriteLine(");");
                file.WriteLine("</script>");
                file.WriteLine("<h4>" + foreign.table.primery_column.description + "</h4>");
              
                file.WriteLine("<div class=\"gallery\">");
                file.WriteLine("@foreach (myLibrary.Models."+foreign.table.name+"Model pic in Model)");
                file.WriteLine("{");
                file.WriteLine("<div class=\"item\">");
                if (foreign.table.tabletype == TableType.Image)
                    file.WriteLine("<div class=\"thumb\"><a href=\"@Url.Content(\"~/\"+pic." + foreign.table.GetImageColumn().name + ")\" class=\"fancy\" rel=\"group\"><img src=\"@Url.Content(\"~/\" + pic." + foreign.table.GetImageColumn().name + ")\" alt=\"\" height=\"150px\" /></a></div>");
                else
                    file.WriteLine("<div class=\"thumb\"><a href=\"@Url.Content(\"~/\"+pic." + foreign.table.GetFileColumn().name + ")\" target=\"_blank\"><img src=\"@Url.Content(\"~/Content/images/default.jpg\")\"/>@pic." + foreign.table.GetNameColumn().name + "</a></div>");
                file.WriteLine("<div class=\"tools\">");
                file.WriteLine("@Html.ActionLink(\"Sil\", \"Delete\", \"Admin" + foreign.table.name + "\", new { id = pic." + foreign.table.primery_column.name + ", redirectUrl = current }, new { @class = \"ico ico-delete\" })");
                file.WriteLine("@Html.ActionLink(\"Düzenle\", \"Edit\", \"Admin" + foreign.table.name + "\", new { id = pic." + foreign.table.primery_column.name + " }, new { @class = \"ico ico-edit\" })");
                file.WriteLine("</div>");
                file.WriteLine("</div> ");
                file.WriteLine("}");
                file.WriteLine("<div id=\"fileuploader_" + foreign.table.name + "@(ViewBag.ID)\"></div>");

                file.WriteLine("</div>");        
         
            }
          

            file.Close();

        }


        public void WriteTableHeader(StreamWriter file, Table table, ForeignKey foreign)
        {
     
           
            foreach (Column item in foreign.table.columns)
            {
                switch (item.columntype)
                {
                    case ColumnType.Image:
                        file.WriteLine("<th>");
                        file.WriteLine(item.description);
                        file.WriteLine("</th>");
                        break;
                    case ColumnType.File:
                        file.WriteLine("<th>");
                        file.WriteLine(item.description);
                        file.WriteLine("</th>");
                        break;
                    default:
                        break;
                }
            }
            foreach (Column item in foreign.table.columns)
            {
                if (item.columntype == ColumnType.HtmlText) continue;
                if (item.columntype == ColumnType.TextArea) continue;
                if (item.columntype == ColumnType.Primery) continue;
                if (item.columntype == ColumnType.Image) continue;
                if (item.columntype == ColumnType.File) continue;
                if (item.columntype == ColumnType.ComboBox) continue;

                file.WriteLine("<th>");
                switch (item.columntype)
                {
                    case ColumnType.Date:
                    case ColumnType.CheckBox:
                    case ColumnType.Name:
                        file.WriteLine("@{");
                        file.WriteLine("RouteValueProvider.AddValue(dic, \"OrderBy\", \"" + item.name + "\");");
                        file.WriteLine("RouteValueProvider.AddValue(dic, \"OrderType\", \"asc\");");
                        file.WriteLine("if (ViewBag.OrderBy != null) { if (ViewBag.OrderBy == \"" + item.name + "\") { RouteValueProvider.AddValue(dic, \"OrderType\", \"desc\"); } }");
                        file.WriteLine("}");
                        string title = (item.description == String.Empty) ? item.name : item.description;
                        file.WriteLine("@Ajax.ActionLink(\"" + title + "\", \"_" + table.name + foreign.table.name + "\", dic, new AjaxOptions { UpdateTargetId = \"pagingdiv\" + ViewBag.ID+\"_" + foreign.table.name + "Group\"})");

                        break;

                    default:
                        file.WriteLine(item.description);
                        break;
                }
                file.WriteLine("</th>");
            }

            file.WriteLine(" <th class=\"action\">");
            file.WriteLine("İşlemler");
            file.WriteLine("</th>");
        }
        public void WriteTableData(StreamWriter file, Table table, ForeignKey foreign)
        {

            foreach (Column Option in foreign.table.columns)
            {
                switch (Option.columntype)
                {
                    case ColumnType.Image:
                        file.WriteLine("<td>");

                        file.WriteLine("<a class=\"picuploader\" href=\"#uploadpic" + Option.name + "_" + foreign.table.name + "\" onclick=\"set" + Option.name + "ID_"+foreign.table.name+"(@Option." +foreign.table.primery_column.name + ")\">");
                        file.WriteLine("@if (Option."+Option.name+" == null)");
                        file.WriteLine("{");
                        file.WriteLine("imgurl = \"default.jpg\";");
                        file.WriteLine("}");
                        file.WriteLine("else");
                        file.WriteLine("{");
                        file.WriteLine("imgurl=Option."+Option.name+";");
                        file.WriteLine("}");
                        file.WriteLine("<img src=\"@Url.Content(imgurl)\" style=\"width:50px; height:50px;\" alt=\"\" />");
                        file.WriteLine("</a>");
                        file.WriteLine("</td>");
                        break;
                    case ColumnType.File:
                        file.WriteLine("<td>");
                        file.WriteLine("<a class=\"picuploader\" href=\"#uploadpic" + Option.name + "_" + foreign.table.name + "\" onclick=\"set" + Option.name + "ID_" + foreign.table.name + "(@Option." + foreign.table.primery_column.name + ")\">");
                        file.WriteLine("<img src=\"@Url.Content(\"~/Content/images/default.jpg\")\" style=\"width:24px; height:24px;\" alt=\"\" />");
                        file.WriteLine("</a>");
                        file.WriteLine("@if (Option." + Option.name + " != null) ");
                        file.WriteLine("{");
                        file.WriteLine("<a href=\"@Url.Content(Option." + Option.name + ")\" target=\"_blank\" >@Option." + Option.name + ".Substring(Option." + Option.name + ".LastIndexOf('/')+1)</a> ");
                        file.WriteLine("}");
                        file.WriteLine("</td>");
                        break;
                    default:
                        break;
                }
            }
            foreach (Column Option in foreign.table.columns)
            {
                if (Option.columntype == ColumnType.HtmlText) continue;
                if (Option.columntype == ColumnType.TextArea) continue;
                if (Option.columntype == ColumnType.Primery) continue;
                if (Option.columntype == ColumnType.Image) continue;
                if (Option.columntype == ColumnType.File) continue;
                if (Option.columntype == ColumnType.ComboBox) continue;

                file.WriteLine("<td>");
                if (Option.isForeign)
                    file.WriteLine("@Option._" + Option.foreign.table.name + ".Name");
                else
                    file.WriteLine("@Option." + Option.name);
                file.WriteLine("</td>");
            }

            file.WriteLine(" <td class=\"action\">");
                file.WriteLine("@Html.ActionLink(\"Edit\", \"EditWith" + table.name + "\", \"Admin" + foreign.table.name + "\", new { id = Option." + foreign.table.primery_column.name + " }, new { @class = \"ico ico-edit\", title = \"Düzenle\" })");
                file.WriteLine("@Html.ActionLink(\"Details\", \"Details\", \"Admin" + foreign.table.name + "\", new { id = Option." + foreign.table.primery_column.name + " }, new { @class = \"ico ico-detail\", title = \"Detay\" })");
                file.WriteLine("@Html.ActionLink(\"Delete\", \"Delete\", \"Admin" + foreign.table.name + "\", new { id = Option." + foreign.table.primery_column.name + ",redirectUrl=HttpContext.Current.Request.Url.OriginalString }, new { @class = \"ico ico-delete\", onclick = \"delete" + table.name + foreign.table.name + "()\", title = \"Sil\" })");
            
          
            file.WriteLine("</td>");
        }
        public void WritePagination(StreamWriter file, Table table)
        {
            file.WriteLine("<div class=\"pagination\">");
            file.WriteLine("@{dic = RouteValueProvider.GetCurrentRouteValues(Url.RequestContext.RouteData.Values, Request.QueryString);");
            file.WriteLine("dic[\"action\"] = \"_" + table.name+foreign.table.name + "\";");
            file.WriteLine("dic[\"id\"] = ViewBag.ID;");

            file.WriteLine("}");
            file.WriteLine("@Html.Raw(Ajax.Pager(new AjaxOptions");
            file.WriteLine("{");
            file.WriteLine("UpdateTargetId = \"pagingdiv\" + ViewBag.ID" + "_" + foreign.table.name);
            file.WriteLine("}, Model.PageSize, Model.PageNumber, Model.PageCount, dic))");
            file.WriteLine("</div>");
        }
        #endregion
    }
}
