﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Avs.Win.Generate
{
    class GreatAdmin_NNRelIndex : IView
    {
        public NNForeignKey nnforeign { get; set; }
        public Table table { get; set; }

        public GreatAdmin_NNRelIndex(NNForeignKey nnforeign, Table table)
        {
            this.nnforeign = nnforeign;
            this.table = table;
        }
        #region IView Members

        public void Write()
        {
            //ortak satırlar
            if (table.name.ToLower().Contains("imageurl"))
            {
                //image index view
            }
            else if (table.name.ToLower().Contains("fileurl"))
            {
                //file index view
            }
            else
            {
                //index view
            }

        }

        #endregion
    }
}
