﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Avs.Win.Generate
{
    class GreatAdmin_BasicIndex_Partial:IView
    {
         public Table table { get; set; }
         public string filePath { get; set; }

         public GreatAdmin_BasicIndex_Partial(Table table)
        {
           this.table= table;
           this.filePath = Constants.ViewTableFolder + "\\_" + table.name + ".cshtml";
        }
        #region IView Members

        public void Write()
        {
            StreamWriter file = new StreamWriter(filePath,false, Encoding.UTF8);//(filePath);

            if (table.tabletype == TableType.Default)
            {
                #region title- model -layout
                file.WriteLine("@using MvcPaging");
                file.WriteLine("@using myLibrary.Classes");
                file.WriteLine("@model IPagedList<myLibrary.Models."+table.name+"Model>");
                file.WriteLine("@{");
                file.WriteLine("ViewBag.Title = \"" + table.primery_column.description + "\";");
                file.WriteLine("Layout = null;");
                file.WriteLine("string RedirectUrl;");
                file.WriteLine("if (HttpContext.Current.Request.UrlReferrer != null)");
                file.WriteLine("{");
                file.WriteLine("RedirectUrl = HttpContext.Current.Request.UrlReferrer.OriginalString;");
                file.WriteLine("}");
                file.WriteLine(" else");
                file.WriteLine("{");
                file.WriteLine("RedirectUrl = Url.Action(\"Index\");");
                file.WriteLine("}");

                file.WriteLine("RouteValueDictionary tempdic = RouteValueProvider.GetCurrentRouteValues(Url.RequestContext.RouteData.Values, Request.QueryString);");
                file.WriteLine("tempdic[\"action\"] = \"_" + table.name + "\";");
                file.WriteLine("tempdic[\"page\"] = \"1\";");
                file.WriteLine("}");
                file.WriteLine("");
                #endregion
                ViewManager.WriteScriptForBasic(file, table);
                ViewManager.WriteFaceUploadDiv(file, table);
                file.WriteLine("<div style=\"padding-left: 10px;\">@Html.ActionLink(\"Yeni "+table.primery_column.description+" Ekle\", \"Create\")</div>");
                file.WriteLine("");


                file.WriteLine("<table class=\"tab tab-drag\">");
                file.WriteLine("<tr class=\"top nodrop nodrag\">");
                ViewManager.WriteTableHeader(file, table, "MainDiv",null);
                file.WriteLine("</tr>");
                
                file.WriteLine("@foreach (var item in Model)");
                file.WriteLine("{");
                file.WriteLine("<tr>");
                    ViewManager.WriteTableData(file, table,null);
                file.WriteLine("</tr>");
                file.WriteLine("}");
                file.WriteLine("</table>");

                ViewManager.WriteClearFilter(file, table);

                ViewManager.WritePagination(file, table, "MainDiv");


            }

            else if (table.tabletype == TableType.Image || table.tabletype == TableType.File )
            {
                //image view
                file.WriteLine("@model IEnumerable<myLibrary.Models."+table.name+"Model>");
                file.WriteLine("@using myLibrary.Classes;");
                file.WriteLine("@{");
                file.WriteLine("Layout = null;");
                file.WriteLine("string current = HttpContext.Current.Request.Url.OriginalString;");
                file.WriteLine("}");

                file.WriteLine("<script type=\"text/javascript\">");
                file.WriteLine("    $(document).ready(function() {");
              
                file.WriteLine("     $(\"#fileuploader_LogoImage\").fileUpload({");
                file.WriteLine("         'uploader': '@Url.Content(\"~/Content/Admin/uploader.swf\")',");
                file.WriteLine("         'cancelImg': '@Url.Content(\"~/Content/admin/images/ico/cancel.png\")',");
                
                file.WriteLine("         'script': '@Url.Action(\"Upload\", \"Admin"+table.name+"\")',");
                file.WriteLine("         'folder': '@Url.Action(\"~/Content/images/"+table.name+"\")',");
                file.WriteLine("         'fileDesc': 'Resim Dosyası',");
                if (table.tabletype == TableType.Image)
                {
                    file.WriteLine("'fileExt': '*.jpg;*.jpeg;*.gif;*.png',");
                    file.WriteLine("         'buttonText': 'Resim Ekle',");
                }
                else
                {
                    file.WriteLine("         'buttonText': 'Dosya Ekle',");
                    file.WriteLine("'fileExt': '*.doc;*.docx;*.pdf;*.xls;*.xlsx;*.ppt;*.pptx;*.txt;*.ini',");
                }
                file.WriteLine("         'multi': true,");
                file.WriteLine("         'auto': true,");
                file.WriteLine("         'onComplete': function () { window.location.reload(true) }");
                file.WriteLine("     });");
                file.WriteLine(" }");
                file.WriteLine(");");
                file.WriteLine("</script>");
                file.WriteLine("<div class=\"gallery\">");
                file.WriteLine("@foreach(myLibrary.Models."+table.name+"Model pic in Model)");
                file.WriteLine("{");
                file.WriteLine("<div class=\"item\">");
                if (table.tabletype == TableType.Image)
                    file.WriteLine("<div class=\"thumb\"><a href=\"@Url.Content(\"~/\"+pic." + table.GetImageColumn().name + ")\" class=\"fancy\" rel=\"group\"><img src=\"@Url.Content(\"~/\" + pic." + table.GetImageColumn().name + ")\" alt=\"\" height=\"150px\" /></a></div>");
                else
                    file.WriteLine("<div class=\"thumb\"><a href=\"@Url.Content(\"~/\"+pic." + table.GetFileColumn().name + ")\" target=\"_blank\"><img src=\"@Url.Content(\"~/Content/images/default.jpg\")\"/>@pic." + table.GetNameColumn().name + "</a></div>");
                
                file.WriteLine("<div class=\"tools\">");
                file.WriteLine("@Html.ActionLink(\"Sil\", \"Delete\", \"Admin"+table.name+"\", new { id = pic."+table.primery_column.name+", redirectUrl = current }, new { @class = \"ico ico-delete\" })");
                file.WriteLine("@Html.ActionLink(\"Düzenle\", \"Edit\", \"Admin"+table.name+"\", new { id = pic."+table.primery_column.name+" }, new { @class = \"ico ico-edit\" })");
                file.WriteLine("</div>");
                file.WriteLine("</div> ");
                file.WriteLine("}");
                file.WriteLine("<div id=\"fileuploader_LogoImage\"></div>");
                file.WriteLine("</div>");

            }
            
            file.Close();

        }

        #endregion
    }
}
