﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Avs.Win.Generate
{
    class GreatAdmin_BasicNew:IView
    {
         public Table table { get; set; }
         public string filePath { get; set; }

         public GreatAdmin_BasicNew(Table table)
        {
           this.table= table;
           this.filePath = Constants.ViewTableFolder + "\\Create.cshtml";
        }
        #region IView Members

        public void Write()
        {
           StreamWriter file = new StreamWriter(filePath,false, Encoding.UTF8);//(filePath);

           if (table.tabletype == TableType.Default)
           {
               file.WriteLine("@model myLibrary.Models."+table.name+"Model");
               file.WriteLine("@{");
               file.WriteLine("ViewBag.Title = \""+table.primery_column.description+" İçin Yeni Kayıt\";");
               file.WriteLine("Layout = \"~/Views_B/Shared/_AdminLayout.cshtml\";");
               file.WriteLine("string RedirectUrl;");
               file.WriteLine("if (HttpContext.Current.Request.UrlReferrer != null)");
               file.WriteLine("{");
               file.WriteLine("RedirectUrl = HttpContext.Current.Request.UrlReferrer.OriginalString;");
               file.WriteLine("}");
               file.WriteLine("else");
               file.WriteLine("{");
               file.WriteLine("RedirectUrl = Url.Action(\"Index\");");
               file.WriteLine("}");
               file.WriteLine("}");

               file.WriteLine("");
               file.WriteLine("<div class=\"box\">");
               ViewManager.WriteHeadLineWithHelp(file, table, "İçin Yeni Kayıt Ekleme");
               file.WriteLine("<div class=\"box-content\">");
               ViewManager.WriteEditor(file, table,"Create",null);

               file.WriteLine("</div>");
               file.WriteLine("</div>");
               
           }
           else if (table.tabletype == TableType.Image)
           {
               //image view
           }
           else if (table.tabletype == TableType.File)
           {
               //file view
           }
           file.Close();
        }

        #endregion
    }
}
