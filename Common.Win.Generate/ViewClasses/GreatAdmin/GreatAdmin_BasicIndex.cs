﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Avs.Win.Generate
{
    class GreatAdmin_BasicIndex:IView
    {
         public Table table { get; set; }

         public string filePath { get; set; }

         public GreatAdmin_BasicIndex(Table table)
        {
           this.table= table;
           this.filePath = Constants.ViewTableFolder+"\\Index.cshtml";
        }

        #region IView Members

        public void Write()
        {

            StreamWriter file = new StreamWriter(filePath,false, Encoding.UTF8);//(filePath);

            if (table.tabletype == TableType.Default)
            {

                #region title- model -layout

                // ViewManager.WriteWiewHeader(file, "@model IPagedList<myLibrary.Models."+table.name+"Model>");
                file.WriteLine("@using MvcPaging");
                file.WriteLine("@model IPagedList<myLibrary.Models."+table.name+"Model>");
                file.WriteLine("@{");
                file.WriteLine("ViewBag.Title = \"" + table.primery_column.description + "\";");
                file.WriteLine("Layout = \"~/Views_B/Shared/_AdminLayout.cshtml\";");
                file.WriteLine("}");
                file.WriteLine("");

                #endregion

                file.WriteLine("<div class=\"box\">");

                #region headline
                WriteHeadLineWithFilter(file, table);
                #endregion

                #region filter

                WriteFilter(file, table,"MainDiv",table.name);

                #endregion

                #region action page
                WriteActionDiv(file, table);
                #endregion

                file.WriteLine("</div>");
            }
            else if (table.tabletype == TableType.Image || table.tabletype == TableType.File)
            { 
                //image view
                file.WriteLine("@using MvcPaging");
                file.WriteLine("@{");
                file.WriteLine("ViewBag.Title = \""+table.primery_column.description+"\";");
                file.WriteLine("Layout = \"~/Views_B/Shared/_AdminLayout.cshtml\";");
                file.WriteLine("}");

                file.WriteLine("<div class=\"box\">");

                file.WriteLine("<div class=\"headlines\">");
                file.WriteLine("<h2>");
                file.WriteLine("<span>" + table.primery_column.description + " İşlemleri</span></h2>");
                file.WriteLine("</div>");

                file.WriteLine("<div id=\"MainDiv\">");
                file.WriteLine("@Html.Action(\"_"+table.name+"\")");
                file.WriteLine("</div>");

                file.WriteLine("</div>");
            }
          

            file.Close();


        }

        public void WriteHeadLineWithFilter(StreamWriter file, Table table)
        {
            file.WriteLine("");
            file.WriteLine("<div class=\"headlines\">");
            file.WriteLine("<h2>");
            file.WriteLine("<span>" + table.primery_column.description + " İşlemleri</span></h2>");
            file.WriteLine("<a href=\"#\" class=\"show-filter\">filtreyi göster</a>");
            file.WriteLine("</div>");
            file.WriteLine("");
        }

        public void WriteFilter(StreamWriter file, Table table, string targetDivID, string controller)
        {
            file.WriteLine("");
            file.WriteLine("<!-- filter -->");
            file.WriteLine("<div class=\"filter\">");
            file.WriteLine("@using (Ajax.BeginForm(\"_" + table.name + "\", \"Admin" + controller + "\", new AjaxOptions{ UpdateTargetId = \"" + targetDivID + "\" }))");
            file.WriteLine("{");

            file.WriteLine("  <div class=\"columns\">");
            file.WriteLine("       <div class=\"six-columns\">");

            int i = (table.columns.Count + (table.columns.Count % 2)) / 2;
            int y = 0;
            foreach (Column item in table.columns)
            {
                if (item.name.StartsWith("Res"))
                    i--;

            }
            foreach (Column item in table.columns)
            {
                y++;
               
                switch (item.columntype)
                {

                    case ColumnType.Date:
                        file.WriteLine("<div class=\"filterlabel\">");
                        file.WriteLine("<span>İlk Tarih:</span></div>");
                        file.WriteLine("<div class=\"confilter\">");
                        file.WriteLine("<input style=\"width: 100px;\" type=\"text\" value=\"\" name=\"DateFirst" + item.name + "\" class=\"input datepicker\" />");
                        file.WriteLine("</div>");
                        file.WriteLine("<div class=\"filterlabel\">");
                        file.WriteLine("<span>Son Tarih: </span>");
                        file.WriteLine("</div>");
                        file.WriteLine("<div class=\"confilter\">");
                        file.WriteLine("<input type=\"text\" value=\"\" style=\"width: 100px;\" name=\"DateLast" + item.name + "\" class=\"input datepicker\" />");
                        file.WriteLine("</div>");
                        //date search
                        break;
                    case ColumnType.Name:
                        file.WriteLine("<div class=\"filterlabel\">");
                        file.WriteLine("<span>" + item.description + ":</span>");
                        file.WriteLine("</div>");
                        file.WriteLine("<div class=\"confilter\">");
                        file.WriteLine("<input type=\"text\" name=\"Name" + item.name + "\" class=\"input\" />");
                        file.WriteLine("</div>");
                        //name = search
                        break;
                    case ColumnType.CheckBox:
                        file.WriteLine("<div class=\"filterlabel\">");
                        file.WriteLine(item.description + ":</div> <div class=\"confilter\">");
                        file.WriteLine("<select name=\"Status" + item.name + "\" class=\"select\">");
                        file.WriteLine("<option value=\"\">Farketmez</option>");
                        file.WriteLine("<option value=\"true\">AKTİF</option>");
                        file.WriteLine("<option value=\"false\">PASİF</option>");
                        file.WriteLine("</select>");
                        file.WriteLine("</div>");
                        //aktif - pasif
                        break;
                    case ColumnType.ComboBox:
                        file.WriteLine("<div class=\"filterlabel\">");
                        file.WriteLine("Grup:</div>");
                        file.WriteLine("<div class=\"confilter\">");
                        file.WriteLine("@Html.DropDownList(\"" + item.foreign.column.name + "\", ViewBag." + item.foreign.column.name + " as SelectList, new { @class = \"select\" })");
                        file.WriteLine("</div>");
                        //foreign search
                        break;
                    default:
                        break;
                }

                file.WriteLine("<div class=\"clear\"></div>");
                if (y == i)
                {
                    file.WriteLine("     </div>");
                    file.WriteLine("<div class=\"six-columns\">");
                }
            }
            file.WriteLine("<input type=\"submit\" value=\"Filtrele\" class=\"submit\" />");
            file.WriteLine("</div>");
            file.WriteLine("</div>");

            file.WriteLine("}");
            file.WriteLine("</div>");
            file.WriteLine("<!-- /filter -->");
            file.WriteLine("");

        }

        public void WriteActionDiv(StreamWriter file, Table table)
        {
            file.WriteLine("");
            file.WriteLine("<div id=\"MainDiv\">");
            file.WriteLine("@Html.Action(\"_" + table.name + "\", new { page = 1 })");
            file.WriteLine("</div>");
            file.WriteLine("");
        }

        #endregion
    }
}
