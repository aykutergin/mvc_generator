﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Avs.Win.Generate
{
    class AceAdmin_BasicIndex:IView
    {
         public Table table { get; set; }

         public string filePath { get; set; }

         public AceAdmin_BasicIndex(Table table)
        {
           this.table= table;
           this.filePath = Constants.ViewTableFolder+"\\Index.cshtml";
        }

        #region IView Members

        public void Write()
        {

            StreamWriter file = new StreamWriter(filePath,false, Encoding.UTF8);//(filePath);

            if (table.tabletype == TableType.Default)
            {

                #region title- model -layout

                // ViewManager.WriteWiewHeader(file, "@model IPagedList<myLibrary.Models."+table.name+"Model>");
                file.WriteLine("@using MvcPaging");
                file.WriteLine("@model List<myLibrary.Models."+table.name+"Model>");
                file.WriteLine("@{");
                file.WriteLine("ViewBag.Title = \"" + table.primery_column.description + "\";");
                file.WriteLine("Layout = \"~/Views_B/Shared/_ALayout.cshtml\";");
                file.WriteLine("}");
                file.WriteLine("");

                #endregion

                #region section and title
                file.WriteLine("@section _js{");
    file.WriteLine("@Html.Partial(\"_ScriptForm\");");
    file.WriteLine("@Html.Partial(\"_ScriptTable\");");
file.WriteLine("}");
file.WriteLine("@section _header{");
   file.WriteLine("<h1 style=\"font-weight: bold\">");
        file.WriteLine(table.primery_column.description+" Yönetimi");
    file.WriteLine("</h1>");
file.WriteLine("}");

                #endregion

                #region filter

                WriteFilterAce(file, table,table.name);

                #endregion

                file.WriteLine("<hr />");

                #region action page
                WriteActionDivAce(file, table);
                #endregion


            }
           
          

            file.Close();


        }


        public void WriteFilterAce(StreamWriter file, Table table, string controller)
        {
            file.WriteLine("<div class=\"row\">");
            file.WriteLine("@using (Html.BeginForm(\"Index\", \"Admin" + controller + "\", FormMethod.Post))");
      file.WriteLine("{");
          file.WriteLine("<div class=\"col-sm-12\">");

          
            
           
            foreach (Column item in table.columns)
            {
             
                switch (item.columntype)
                {

                    case ColumnType.Date:
                       file.WriteLine("<div class=\"col-sm-2\">");
                 file.WriteLine("<label>");
                     file.WriteLine(item.description+" Aralığı</label>");
                 file.WriteLine("<div class=\"row\">");
                     file.WriteLine("<div class=\"col-xs-8 col-sm-11\">");
                         file.WriteLine("<div class=\"input-daterange input-group\">");
                         file.WriteLine("<input type=\"text\" class=\"input-sm form-control\" name=\"DateFirst" + item.name + "\" data-date-format=\"dd-mm-yyyy\"  />");
                             file.WriteLine("<span class=\"input-group-addon\"><i class=\"fa fa-exchange\"></i></span>");
                             file.WriteLine("<input type=\"text\" class=\"input-sm form-control\" name=\"DateLast" + item.name + "\" data-date-format=\"dd-mm-yyyy\" />");
                         file.WriteLine("</div>");
                     file.WriteLine("</div>");
                 file.WriteLine("</div>");
             file.WriteLine("</div>");
                        //date search
                        break;
                    case ColumnType.Name:
                      
                        //name = search
                        break;
                    case ColumnType.CheckBox:
                        file.WriteLine("<div class=\"col-sm-2\">");
                          file.WriteLine("<label>");
                     file.WriteLine(item.description+"</label>");
                     file.WriteLine("<select name=\"Status" + item.name + "\" class=\"form-control\">");
                        file.WriteLine("<option value=\"\">Farketmez</option>");
                        file.WriteLine("<option value=\"true\">AKTİF</option>");
                        file.WriteLine("<option value=\"false\">PASİF</option>");
                        file.WriteLine("</select>");
                        file.WriteLine("</div>");
                        //aktif - pasif
                        break;
                    case ColumnType.ComboBox:
                       file.WriteLine("<div class=\"col-sm-2\">");
                 file.WriteLine("<label>");
                     file.WriteLine(item.description+"</label>");
                     file.WriteLine("@Html.DropDownList(\"" + item.foreign.column.name + "\", ViewBag." + item.foreign.column.name + " as SelectList, new { @class = \"form-control\" })");
             file.WriteLine("</div>");
                        //foreign search
                        break;
                    default:
                        break;
                }

             
            }

            file.WriteLine("<div class=\"col-sm-2\">");
            file.WriteLine("<label>");
            file.WriteLine("</label>");
            file.WriteLine(" <br />");
            file.WriteLine("<button type=\"submit\" class=\"btn btn-info\">");
            file.WriteLine(" Sonuçları Getir <i class=\"ace-icon fa fa-filter  align-top bigger-125 icon-on-right\">");
            file.WriteLine("</i>");
            file.WriteLine("</button>");
            file.WriteLine("</div>");
        



            file.WriteLine("</div>");

            file.WriteLine("}");
            file.WriteLine("</div>");
            file.WriteLine("<!-- /filter -->");
            file.WriteLine("");

        }

       
        public void WriteActionDivAce(StreamWriter file, Table table)
        {
            //buraya al 
            file.WriteLine(" <div class=\"pull-left\">");
            file.WriteLine(" @Html.ActionLink(\"Oluştur\", \"Create\", \"Admin" + table.name + "\", new { " + GetForeignParameter(table) + " }, new { @class = \"btn btn-default\" })");
            file.WriteLine("</div>");

            file.WriteLine("<div class=\"pull-right tableTools-container\">");
            file.WriteLine("</div>");

            file.WriteLine("");
            file.WriteLine("@Html.Partial(\"_" + table.name + "\", Model)");
            file.WriteLine("");
        }

        public static string GetForeignParameter(Table table)
        {
            string parameter = String.Empty;
            if (table.foreigns.Count < 1) return parameter;

            foreach (ForeignKey item in table.foreigns)
            {
                parameter = parameter + String.Format("{0} = ViewBag.Param{1} ,", item.column.name, item.column.name);

            }
            parameter = parameter.Remove(parameter.LastIndexOf(','));
            return parameter;
        }

        #endregion
    }
}
