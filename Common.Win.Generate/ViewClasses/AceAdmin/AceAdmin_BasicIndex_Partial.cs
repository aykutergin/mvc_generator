﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Avs.Win.Generate
{
    class AceAdmin_BasicIndex_Partial:IView
    {
         public Table table { get; set; }
         public string filePath { get; set; }

         public AceAdmin_BasicIndex_Partial(Table table)
        {
           this.table= table;
           this.filePath = Constants.ViewTableFolder + "\\_" + table.name + ".cshtml";
        }
        #region IView Members

        public void Write()
        {
            StreamWriter file = new StreamWriter(filePath,false, Encoding.UTF8);//(filePath);

            if (table.tabletype == TableType.Default)
            {
                string foreignparams = GetForeignParameter(table);
                string addfparams = "";
                if (foreignparams != String.Empty) addfparams += "," + foreignparams;
                #region title- model -layout
                file.WriteLine("@using MvcPaging");
                file.WriteLine("@using myLibrary.Classes");
                file.WriteLine("@model List<myLibrary.Models."+table.name+"Model>");
                file.WriteLine("@{");
                file.WriteLine("ViewBag.Title = \"" + table.primery_column.description + "\";");
                file.WriteLine("Layout = null;");
                file.WriteLine("string RedirectUrl;");
                file.WriteLine("if (HttpContext.Current.Request.UrlReferrer != null)");
                file.WriteLine("{");
                file.WriteLine("RedirectUrl = HttpContext.Current.Request.UrlReferrer.OriginalString;");
                file.WriteLine("}");
                file.WriteLine(" else");
                file.WriteLine("{");
                file.WriteLine("RedirectUrl = Url.Action(\"Index\");");
                file.WriteLine("}");

  
                file.WriteLine("}");
                file.WriteLine("");
                #endregion
                
                file.WriteLine("");


                file.WriteLine("<div class=\"row\">");
    file.WriteLine("<div class=\"col-xs-12\">");
        file.WriteLine("<div class=\"clearfix\">");


             
        file.WriteLine("</div>");

                  file.WriteLine(" <div class=\"space-6\"></div>");
        file.WriteLine("<div>");


        file.WriteLine("<table class=\" dynamic-tablee table table-striped table-bordered table-hover\">");
                 file.WriteLine("<thead>");
                    file.WriteLine(" <tr>");
                ViewManager.WriteTableHeaderAce(file, table,null);
                file.WriteLine("</tr>");
                file.WriteLine("</thead>");
                file.WriteLine("<tbody>");
               
                
                file.WriteLine("@foreach (var item in Model)");
                file.WriteLine("{");
                file.WriteLine("<tr>");
                ViewManager.WriteTableDataAce(file, table, null, addfparams);
                file.WriteLine("</tr>");
               
                file.WriteLine("}");
                file.WriteLine("</tbody>");
                file.WriteLine("</table>");

                file.WriteLine("</div>");
   file.WriteLine(" </div>");
file.WriteLine("</div>");


            }

          
            
            file.Close();

        }

        public static string GetForeignParameter(Table table)
        {
            string parameter = String.Empty;
            if (table.foreigns.Count < 1) return parameter;

            foreach (ForeignKey item in table.foreigns)
            {
                parameter = parameter + String.Format("{0} = ViewBag.Param{1} ,", item.column.name, item.column.name);

            }
            parameter = parameter.Remove(parameter.LastIndexOf(','));
            return parameter;
        }

        #endregion
    }
}
