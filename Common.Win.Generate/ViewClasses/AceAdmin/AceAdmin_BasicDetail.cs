﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Avs.Win.Generate
{
    class AceAdmin_BasicDetail:IView
    {
        public Table table { get; set; }
        public string filePath { get; set; }

        public AceAdmin_BasicDetail(Table table)
        {
           this.table= table;
           this.filePath = Constants.ViewTableFolder + "\\Details.cshtml";
        }
        #region IView Members

        public void Write()
        {
            StreamWriter file = new StreamWriter(filePath,false, Encoding.UTF8);//(filePath);

            if (table.tabletype == TableType.Default)
            {
                #region title- model -layout

                file.WriteLine("@model myLibrary.Models."+table.name+"Model");
                file.WriteLine("@{");
                file.WriteLine("ViewBag.Title = \"" + table.primery_column.description + "\";");
                file.WriteLine("Layout = \"~/Views_B/Shared/_ALayout.cshtml\";");
                file.WriteLine("string RedirectUrl;");
                file.WriteLine("if (HttpContext.Current.Request.UrlReferrer != null)");
                file.WriteLine("{");
                file.WriteLine("RedirectUrl = HttpContext.Current.Request.UrlReferrer.OriginalString;");
                file.WriteLine("}");
                file.WriteLine("else");
                file.WriteLine("{");
                file.WriteLine("RedirectUrl = Url.Action(\"Index\");");
                file.WriteLine("}");
                file.WriteLine("}");
                file.WriteLine("");


                
                
                #endregion

                file.WriteLine("@section _js{");
                file.WriteLine("@Html.Partial(\"_ScriptForm\");");
                file.WriteLine("@Html.Partial(\"_ScriptTable\");");
                file.WriteLine("@Html.Partial(\"_ScriptDetail\");");
                file.WriteLine("}");
                file.WriteLine("@section _header{");
                file.WriteLine("<h1 style=\"font-weight: bold\">");
                file.WriteLine(table.primery_column.description +" Detay");
                file.WriteLine("</h1>");
                file.WriteLine("}");




                ViewManager.WriteDetailAce(file, table);

                
              
           

            }
            else if (table.tabletype == TableType.Image)
            {
                //image view
            }
            else if (table.tabletype == TableType.File)
            {
                //file view
            }
            file.Close();

        }

        #endregion
    }
}
