﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Avs.Win.Generate
{
    public class ViewManager
    {
        #region Search - Filter

        public static void WriteFilter(StreamWriter file, Table table, string targetDivID, string controller)
        {
            file.WriteLine("");
            file.WriteLine("<!-- filter -->");
            file.WriteLine("<div class=\"filter\">");
            file.WriteLine("@using (Ajax.BeginForm(\"_" + table.name + "\", \"Admin" + controller + "\", new AjaxOptions{ UpdateTargetId = \"" + targetDivID + "\" , OnBegin = \"showSpinner\", OnComplete = \"hideSpinner\"  }))");
            file.WriteLine("{");
            bool isColumnExist = false;
            foreach (Column item in table.columns)
            {
                switch (item.columntype)
                {

                    case ColumnType.Date:
                        file.WriteLine("<div class=\"filterlabel\">");
                        file.WriteLine("<span>İlk Tarih:</span></div>");
                        file.WriteLine("<div class=\"confilter\">");
                        file.WriteLine("<input style=\"width: 100px;\" type=\"text\" value=\"\" name=\"DateFirst" + item.name + "\" class=\"input date-picker\" />");
                        file.WriteLine("</div>");
                        file.WriteLine("<div class=\"filterlabel\">");
                        file.WriteLine("<span>Son Tarih: </span>");
                        file.WriteLine("</div>");
                        file.WriteLine("<div class=\"confilter\">");
                        file.WriteLine("<input type=\"text\" value=\"\" style=\"width: 100px;\" name=\"DateLast" + item.name + "\" class=\"input date-picker\" />");
                        file.WriteLine("</div>");
                        isColumnExist = true;
                        //date search
                        break;
                    case ColumnType.Name:
                        file.WriteLine("<div class=\"filterlabel\">");
                        file.WriteLine("<span>" + item.description + ":</span>");
                        file.WriteLine("</div>");
                        file.WriteLine("<div class=\"confilter\">");
                        file.WriteLine("<input type=\"text\" name=\"Name" + item.name + "\" class=\"input\" />");
                        file.WriteLine("</div>");
                        //name = search
                        isColumnExist = true;
                        break;
                    case ColumnType.CheckBox:
                        file.WriteLine("<div class=\"filterlabel\">");
                        file.WriteLine(item.description + ":</div> <div class=\"confilter\">");
                        file.WriteLine("<select name=\"Status" + item.name + "\" class=\"select\">");
                        file.WriteLine("<option value=\"\">Farketmez</option>");
                        file.WriteLine("<option value=\"AKTİF\">True</option>");
                        file.WriteLine("<option value=\"PASİF\">false</option>");
                        file.WriteLine("</select>");
                        file.WriteLine("</div>");
                        isColumnExist = true;
                        //aktif - pasif
                        break;
                    case ColumnType.ComboBox:
                        file.WriteLine("<div class=\"filterlabel\">");
                        file.WriteLine(item.description + ":</div>");
                        file.WriteLine("<div class=\"confilter\">");
                        file.WriteLine("@Html.DropDownList(\"" + item.foreign.column.name + "\", ViewBag." + item.foreign.column.name + " as SelectList, new { @class = \"select\" })");
                        file.WriteLine("</div>");
                        isColumnExist = true;
                        //foreign search
                        break;
                    default:
                        break;
                }
                file.WriteLine("<div class=\"clear\"></div>");

            }
            if (isColumnExist)
            {
                file.WriteLine("<input type=\"submit\" value=\"Filtrele\" class=\"submit\" />");
            }
            file.WriteLine("}");
            file.WriteLine("</div>");
            file.WriteLine("<!-- /filter -->");
            file.WriteLine("");

        }

        public static void WriteClearFilter(StreamWriter file, Table table)
        {
            file.WriteLine("<div class=\"tab-action\">");
            file.WriteLine("@Html.ActionLink(\"Filtreyi Sıfırla\", \"ResetFilter\")");
            file.WriteLine("</div>");

        }
        #endregion

        #region Editors

        public static void WriteEditor(StreamWriter file, Table table, string action, ForeignKey foreign)
        {
            file.WriteLine(" @using (Html.BeginForm(\"" + action + "\", \"Admin" + table.name + "\", new { RedirectUrl = RedirectUrl }, FormMethod.Post, new { @class = \"form-horizontal\" }))");
            file.WriteLine("{");
            file.WriteLine("<h4 class=\"header blue bolder smaller\">Bilgiler</h4>");
            if (foreign != null)
            {
                string key = (action == "Edit") ? "Model." + foreign.column.name + "" : "ViewBag.ID";
                file.WriteLine("<input type=\"hidden\" name=\"" + foreign.column.name + "\" value=\"@" + key + "\"/>");
                //hidden file
            }



            foreach (Column item in table.columns)
            {
                if (item.name.StartsWith("Res"))
                    continue;


                switch (item.columntype)
                {
                    case ColumnType.Date:
                        file.WriteLine("<div class=\"form-group\">");
                        file.WriteLine("<label class=\"col-sm-3 control-label no-padding-right\">@Html.LabelFor(model => Model." + item.name + ")</label>");
                        file.WriteLine("<div class=\"col-sm-9\">");
                        file.WriteLine("<div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-calendar bigger-110\"></i></span>");
                        file.WriteLine("@Html.TextBoxFor(model => Model." + item.name + ", new { @class = \"col-xs-10 col-sm-5 date-picker\" })");
                        file.WriteLine("@Html.ValidationMessageFor(model => model." + item.name + ")");
                        file.WriteLine("</div>");
                        file.WriteLine("</div>");
                        file.WriteLine("</div>");
                        //write datepicker
                        break;
                    case ColumnType.Text:
                    case ColumnType.Name:
                    case ColumnType.Email:
                    case ColumnType.Url:
                        file.WriteLine("<div class=\"form-group\">");
                        file.WriteLine("<label class=\"col-sm-3 control-label no-padding-right\">@Html.LabelFor(model => Model." + item.name + ")</label>");
                        file.WriteLine("<div class=\"col-sm-9\">@Html.TextBoxFor(model => Model." + item.name + ", new { @class = \"col-xs-10 col-sm-5\" })");
                        file.WriteLine("@Html.ValidationMessageFor(model => model." + item.name + ")</div>");
                        file.WriteLine("</div>");
                        //write textbox
                        break;
                    case ColumnType.TextArea:
                        file.WriteLine("<div class=\"form-group\">");
                        file.WriteLine("<label class=\"col-sm-3 control-label no-padding-right\">@Html.LabelFor(model => Model." + item.name + ")</label>");
                        file.WriteLine("<div class=\"col-sm-9\">@Html.TextAreaFor(model => Model." + item.name + ", new { @class = \"col-xs-10 col-sm-5 \" })");
                        file.WriteLine("@Html.ValidationMessageFor(model => model." + item.name + ")");
                        file.WriteLine(" </div>");
                        file.WriteLine("</div>");
                        //write textarea
                        break;
                    case ColumnType.HtmlText:
                        file.WriteLine("<div class=\"form-group\">");
                        file.WriteLine("<label >@Html.LabelFor(model => Model." + item.name + ")</label>");
                        file.WriteLine("<div class=\"col-sm-9\">@Html.TextAreaFor(model => Model." + item.name + ", new { @class = \"col-xs-10 col-sm-5 wysiwyg-editor\" })");
                        file.WriteLine("@Html.ValidationMessageFor(model => model." + item.name + ")");
                        file.WriteLine("</div></div>");
                        //html editor işlemleri
                        break;
                    case ColumnType.CheckBox:
                        file.WriteLine("<div class=\"form-group\">");
                        file.WriteLine("<label  class=\"col-sm-3 control-label no-padding-right\">@Html.LabelFor(model => Model." + item.name + ")</label>");
                        file.WriteLine("<div class=\"col-sm-9\">@Html.CheckBoxFor(model => Model." + item.name + ", new { @class = \"col-xs-10 col-sm-5 checkbox\" })");
                        file.WriteLine("@Html.ValidationMessageFor(model => model." + item.name + ")");
                        file.WriteLine("</div></div>");
                        //write checkbox
                        break;
                    case ColumnType.ComboBox:
                        if (foreign != null)
                        {
                            if (foreign.column.name != item.name)
                            {
                                file.WriteLine("<div class=\"form-group\">");
                                file.WriteLine("<label class=\"col-sm-3 control-label no-padding-right\">@Html.LabelFor(model => Model." + item.name + ")</label>");
                                file.WriteLine("<div class=\"col-sm-9\">@Html.DropDownListFor(model => Model." + item.name + ", ViewBag." + item.name + " as SelectList, new { @class = \"col-xs-10 col-sm-5\" })");
                                file.WriteLine("@Html.ValidationMessageFor(model => model." + item.name + ")");
                                file.WriteLine("</div></div>");
                            }
                        }
                        else
                        {
                            file.WriteLine("if (ViewBag.Param" + item.name + " == 0)");
                            file.WriteLine("{");


                            file.WriteLine("<div class=\"form-group\">");
                            file.WriteLine("<label class=\"col-sm-3 control-label no-padding-right\">@Html.LabelFor(model => Model." + item.name + ")</label>");
                            file.WriteLine("<div class=\"col-sm-9\">@Html.DropDownListFor(model => Model." + item.name + ", ViewBag." + item.name + " as SelectList, new { @class = \"col-xs-10 col-sm-5\" })");
                            file.WriteLine("@Html.ValidationMessageFor(model => model." + item.name + ")");
                            file.WriteLine("</div></div>");


                             file.WriteLine("}");
                            file.WriteLine("else");
                            file.WriteLine("{");
                            file.WriteLine("<input type=\"hidden\" name=\"" + item.name + "\" value=\"@ViewBag.Param" + item.name + "\"/>");
                            file.WriteLine("}");
                        }
                        //combobox işlemleri
                        break;
                    default:
                        break;
                }

            }

            file.WriteLine("<div class=\"clearfix form-actions\">");
            file.WriteLine("<div class=\"col-md-offset-3 col-md-9\">");
            file.WriteLine("<button class=\"btn btn-info\" type=\"submit\">");
            file.WriteLine(" <i class=\"ace-icon fa fa-check bigger-110\"></i>Oluştur");
            file.WriteLine(" </button>");
            file.WriteLine("&nbsp; &nbsp; &nbsp;");
            file.WriteLine("<a  class=\"btn\" href=\"@RedirectUrl\" style=\"color:White\">");
            file.WriteLine("<i class=\"ace-icon fa fa-undo bigger-110\"></i> Geri Dön </a>");
            file.WriteLine(" </div>");
            file.WriteLine("</div>");

            file.WriteLine("}");
        }
        #endregion


        public static void WriteUniverseEditor(StreamWriter file, Table table, string action, ForeignKey foreign)
        {
            
            if (foreign != null)
            {
                string key = (action == "Edit") ? "Model." + foreign.column.name + "" : "ViewBag.ID";
                file.WriteLine("<input type=\"hidden\" name=\"" + foreign.column.name + "\" value=\"@" + key + "\"/>");
                //hidden file
            }


            int count = table.columns.Count;
            int loop = 0;
            int row_count=0;
            file.WriteLine("<div class=\"container-block\">");
            file.WriteLine("<h4 class=\"container-block__heading\">Bu Formu Kullanarak "+table.primery_column.description+" İçin Yeni Kayıt Oluşturabilirsiniz.</h4>");
            while (count > loop)
            {
                row_count  = 0;
               
                file.WriteLine("<div class=\"row\">");
                for (int i = loop; i < count; i++)
                {
                    Column item = table.columns[i];
               
                    if (item.name.StartsWith("Res"))
                    {
                        loop++;
                        continue;
                        
                    }


                    switch (item.columntype)
                    {
                        case ColumnType.Primery:
                            row_count--;
                            break;
                        case ColumnType.Date:
                            file.WriteLine("<div class=\"col-lg-4\">");
                            file.WriteLine("<div class=\"form-group\">");
                            file.WriteLine("<label for=\"settings-address\"> "+item.description+"</label>");
                            file.WriteLine("<input class=\"form-control flatpickr\" placeholder=\"Tarih Seçiniz\">");
                            file.WriteLine("</div>");
                            file.WriteLine("</div>");
                            
                            break;
                        case ColumnType.Text:
                        case ColumnType.Name:
                        case ColumnType.Email:
                        case ColumnType.Url:
                            file.WriteLine("<div class=\"col-lg-4\">");
                            file.WriteLine("<div class=\"form-group\">");
                            file.WriteLine("<label >"+item.description+"</label>");
                            file.WriteLine("<input type =\"text\" class=\"form-control\"  placeholder=\"\" >");
                            file.WriteLine("</div>");
                            file.WriteLine("</div>");
                            
                            //write textbox
                            break;
                        case ColumnType.TextArea:
                        case ColumnType.HtmlText:
                            if (row_count != 0)
                            {
                                loop--;
                            }
                            else
                            {
                                file.WriteLine("<div class=\"col-lg-8\">");
                                file.WriteLine("<div class=\"form-group\">");
                                file.WriteLine("<label for=\"settings-my-target\">" + item.description + "</label>");
                                file.WriteLine("<textarea rows =\"7\" placeholder=\"\" class=\"form-control\"></textarea>");
                                file.WriteLine("</div>");
                                file.WriteLine("</div>");
                            }
                            //write textarea
                            break;
                      
                        case ColumnType.CheckBox:
                            file.WriteLine("<div class=\"col-lg-4\">");

                            file.WriteLine("<div class=\"form-group\">");
                            file.WriteLine("<label for=\"settings-country\">"+item.description+"</label>");
                            file.WriteLine("<div class=\"form-group\">");
                            file.WriteLine("<div class=\"custom-control\">");
                            file.WriteLine("<label class=\"switch mr-3\">");
                            file.WriteLine("<input type =\"checkbox\">");
                            file.WriteLine("<span class=\"switch-slider\">");
                            file.WriteLine("<span class=\"switch-slider__on\"></span>");
                            file.WriteLine("<span class=\"switch-slider__off\"></span>");
                            file.WriteLine(" </span>");
                            file.WriteLine("</label>");

                            file.WriteLine(" </div>");
                            file.WriteLine(" </div>");
                            
                            file.WriteLine(" </div>");
                            file.WriteLine(" </div>");

                      
                            //write checkbox
                            break;
                        case ColumnType.ComboBox:


                            file.WriteLine("<div class=\"col-lg-4\">");
                            file.WriteLine("<div class=\"form-group\">");
                            file.WriteLine("<label for=\"settings-country\">"+item.description+"</label>");
                            file.WriteLine("<select id =\"settings-country\" class=\"form-control\" data-placeholder=\"\">");

                            file.WriteLine("@foreach(var item in Model._" + item.foreign.table.name + "List)");
                            file.WriteLine("{");
                            file.WriteLine("<option value = \"@item." + item.foreign.table.primery_column.name + "\" > @item." + item.foreign.table.GetNameColumn().name + " </option>");

                            file.WriteLine("}");


                            file.WriteLine("</select>");
                            file.WriteLine(" </div>");
                            file.WriteLine("</div>");


                            //combobox işlemleri
                            break;
                        default:
                            break;
                    }
                    
                    loop++;
                    row_count++;
                    if (row_count > 1) {
                        break;
                    }

                }
               
                file.WriteLine(" </div>");
            }

            file.WriteLine("</div>");

            file.WriteLine("<div class=\"container-block\">");
            file.WriteLine("<h4 class=\"container-block__heading\"></h4>");
            file.WriteLine("<div class=\"form-group\">");
            file.WriteLine("<button class=\"btn btn-info icon-left btn-lg mr-3\" type=\"button\">Kaydet");
            file.WriteLine("<span class=\"btn-icon ua-icon-circle-check\"></span>");
            file.WriteLine("</button>");
            file.WriteLine("</div>");
            file.WriteLine("</div>");
        }

        #region Upload
        public static void WriteFaceUploadDiv(StreamWriter file, Table table)
        {
            foreach (Column item in table.columns)
            {
                if (item.columntype == ColumnType.Image || item.columntype == ColumnType.File)
                {
                    file.WriteLine("<div style=\"display: none;\">");
                    file.WriteLine("<div id=\"uploadpic" + item.name + "\">");
                    file.WriteLine("@using (Html.BeginForm(\"_Upload" + item.name + "\", \"Admin" + table.name + "\", new { RedirectUrl = HttpContext.Current.Request.Url.OriginalString }, FormMethod.Post, new { enctype = \"multipart/form-data\" }))");
                    file.WriteLine("{");
                    file.WriteLine("<div>");
                    file.WriteLine(item.description + " :");
                    file.WriteLine("<input type=\"file\" name=\"file" + item.name + "\" id=\"resim\" />");
                    file.WriteLine("<input id=\"" + item.name + "PrimaryID\" name=\"PrimaryID\" type=\"hidden\" value=\"1\" />");
                    file.WriteLine("<input type=\"submit\" name=\"Onayla\" value=\"Onayla\" />");
                    file.WriteLine("</div>  ");
                    file.WriteLine("}");
                    file.WriteLine("</div>");
                    file.WriteLine("</div>");
                }
            }
        }

        #endregion

        #region Table-Detail

        public static void WriteDetail(StreamWriter file, Table table)
        {
            file.WriteLine("   <div style=\"padding:10px\">");
            file.WriteLine("<div style=\"padding:10px\" class=\"framed\">");
            file.WriteLine("  <div class=\"columns\">");
            file.WriteLine("  <div class=\"six-columns\">");

            int i = table.columns.Count;
            int y = 0;

            foreach (Column column in table.columns)
            {
                if (column.name.StartsWith("Res"))
                    i--;
            }

            foreach (Column column in table.columns)
            {
                switch (column.columntype)
                {
                    case ColumnType.Image:
                        y++;
                        file.WriteLine("<img src=\"@Url.Content(Model." + column.name + ")\" class=\"profileimg\" alt=\"\" width=\"100\" />");
                        //image olarak yazılacak
                        break;
                    default:
                        break;
                }
            }

            foreach (Column column in table.columns)
            {

                y++;
                if (column.name.StartsWith("Res"))
                {
                    continue;
                }

                switch (column.columntype)
                {
                    case ColumnType.Image:
                        break;
                    case ColumnType.File:
                        file.WriteLine("<div class=\"attribute\">");
                        file.WriteLine("<div class=\"display-label\">");
                        file.WriteLine(column.description + "</div>");
                        file.WriteLine("<div class=\"display-field\">");
                        file.WriteLine(" <a href=\"@Url.Content(\"~\" + Model." + column.name + ")\">File Download</a>");
                        file.WriteLine("</div>");
                        file.WriteLine("<div class=\"clear\">");
                        file.WriteLine("</div>");
                        file.WriteLine("</div>");
                        file.WriteLine("");
                        break;
                    case ColumnType.HtmlText:
                        //
                        file.WriteLine("<div class=\"attribute\">");
                        file.WriteLine("<div class=\"display-label\">");
                        file.WriteLine(column.description + "</div>");
                        file.WriteLine("<div class=\"display-field htmldescription\">");
                        file.WriteLine(" @Html.Raw(Model." + column.name + ")");
                        file.WriteLine("</div>");
                        file.WriteLine("<div class=\"clear\">");
                        file.WriteLine("</div>");
                        file.WriteLine("</div>");
                        file.WriteLine("");
                        break;

                    case ColumnType.ComboBox:
                        //
                        file.WriteLine("<div class=\"attribute\">");
                        file.WriteLine("<div class=\"display-label\">");
                        file.WriteLine(column.description + "</div>");
                        file.WriteLine("<div class=\"display-field\">");
                        file.WriteLine("@Html.DisplayFor(model => model._" + column.foreign.table.name + ".Name)");
                        file.WriteLine("</div>");
                        file.WriteLine("<div class=\"clear\">");
                        file.WriteLine("</div>");
                        file.WriteLine("</div>");
                        file.WriteLine("");
                        break;
                    default:
                        //normal yazılacak
                        file.WriteLine("<div class=\"attribute\">");
                        file.WriteLine("<div class=\"display-label\">");
                        file.WriteLine(column.description + "</div>");
                        file.WriteLine("<div class=\"display-field\">");
                        file.WriteLine("@Html.DisplayFor(model => model." + column.name + ")");
                        file.WriteLine("</div>");
                        file.WriteLine("<div class=\"clear\">");
                        file.WriteLine("</div>");
                        file.WriteLine("</div>");
                        file.WriteLine("");
                        break;
                }
                if (y == (i / 2))
                {
                    file.WriteLine("     </div>");
                    file.WriteLine("<div class=\"six-columns\">");
                }
            }
            file.WriteLine("</div>");
            file.WriteLine("</div>");
            file.WriteLine("</div>");
            file.WriteLine("</div>");

        }

        public static void WriteDetailAce(StreamWriter file, Table table)
        {
            string imgpath = "";
            bool hasImage = false;
            string imageColumnName = "";
            foreach (Column column in table.columns)
            {
                switch (column.columntype)
                {
                    case ColumnType.Image:
                        imgpath = "Model." + column.name + "";
                        imageColumnName = column.name;
                        hasImage = true;
                        //image olarak yazılacak
                        break;
                    default:
                        break;
                }
            }
            if (imgpath == String.Empty)
                imgpath = "\"~/Content/activity/avatars/detay.png\"";

            file.WriteLine("<div id=\"user-profile-1\" class=\"user-profile row\">");
            file.WriteLine("    <div class=\"col-xs-12 col-sm-3 center\">");
            file.WriteLine("        <div>");
            file.WriteLine("            <!-- #section:pages/profile.picture -->");
            file.WriteLine("            <span class=\"profile-picture\">");
            file.WriteLine("                <img style=\"width:156px\" class=\"img-responsive\" alt=\"Detay\" src=\"@Url.Content(myLibrary.Tools.GetDefaultFilePath(" + imgpath + "))\" />");
            file.WriteLine("            </span>");

            if (hasImage)
            {
                file.WriteLine("				<span>");
                file.WriteLine("              @using(Html.BeginForm(\"_Upload" + imageColumnName + "\", \"Admin" + table.name + "\", new { }, FormMethod.Post, new { enctype = \"multipart/form-data\" }))");
                file.WriteLine("              {");
                file.WriteLine("                @Html.HiddenFor(model => Model." + table.primery_column.name + ")");
                file.WriteLine("														<div class=\"form-group\">");
                file.WriteLine("															<div class=\"col-xs-10\">");
                file.WriteLine("																<!-- #section:custom/file-input -->");
                file.WriteLine("																<input type=\"file\" name=\"fileimagePath\" id=\"profileFile\" />");
                file.WriteLine("															</div>");
                file.WriteLine("                                                            	<div class=\"col-xs-2\">");
                file.WriteLine("																<!-- #section:custom/file-input -->");
                file.WriteLine("																 <input type=\"submit\" value=\"Değiştir\" class=\"btn btn-xs btn-danger\" />");
                file.WriteLine("															</div>");
                file.WriteLine("														</div>");
                file.WriteLine("              }");
                file.WriteLine("              </span>");
                file.WriteLine("               }");


            }

            file.WriteLine("            <!-- /section:pages/profile.picture -->");
            file.WriteLine("            <div class=\"space-4\">");
            file.WriteLine("            </div>");
            file.WriteLine("            <div class=\"width-80 label label-info label-xlg arrowed-in arrowed-in-right\">");
            file.WriteLine("                <div class=\"inline position-relative\">");
            file.WriteLine("                    <a href=\"#\" class=\"user-title-label dropdown-toggle\" data-toggle=\"dropdown\"><i class=\"ace-icon fa fa-circle light-green\">");
            file.WriteLine("                    </i>&nbsp; <span class=\"white\">@Model." + table.GetNameColumn().name + "</span> </a>");
            file.WriteLine("                </div>");
            file.WriteLine("            </div>");
            file.WriteLine("        </div>");
            file.WriteLine("        <div class=\"space-6\">");
            file.WriteLine("        </div>");
            file.WriteLine("");
            file.WriteLine("        <div class=\"profile-contact-info\">");
            file.WriteLine("            <div class=\"profile-contact-links align-left\">");
            file.WriteLine("                <a href=\"@Url.Action(\"Edit\", new { id = Model." + table.primery_column.name + " })\" class=\"btn btn-link\"><i class=\"ace-icon fa fa-plus-circle bigger-120 green\">");
            file.WriteLine("                </i>Güncelle </a>");
            file.WriteLine("                <a href=\"@RedirectUrl\" class=\"btn btn-link\"><i class=\"ace-icon fa fa-backward bigger-120 black\">");
            file.WriteLine("                </i>Geri Dön </a>");

            file.WriteLine("                <a href=\"@Url.Action(\"Index\",\"Admin"+table.name+"\")\" class=\"btn btn-link\"><i class=\"ace-icon fa fa-backward bigger-120 black\">");
            file.WriteLine("                </i>Listeye Dön </a>");
         
            file.WriteLine("            </div>");
            file.WriteLine("            <div class=\"space-6\">");
            file.WriteLine("            </div>");
            file.WriteLine("            <div class=\"profile-social-links align-center\">");
            file.WriteLine("                <a href=\"#\" class=\"tooltip-info\" title=\"\" data-original-title=\"Visit my Facebook\"><i");
            file.WriteLine("                    class=\"middle ace-icon fa fa-facebook-square fa-2x blue\"></i></a><a href=\"#\" class=\"tooltip-info\"");
            file.WriteLine("                        title=\"\" data-original-title=\"Visit my Twitter\"><i class=\"middle ace-icon fa fa-twitter-square fa-2x light-blue\">");
            file.WriteLine("                        </i></a><a href=\"#\" class=\"tooltip-error\" title=\"\" data-original-title=\"Visit my Pinterest\">");
            file.WriteLine("                            <i class=\"middle ace-icon fa fa-pinterest-square fa-2x red\"></i></a>");
            file.WriteLine("            </div>");
            file.WriteLine("        </div>");
            file.WriteLine("    </div>");
            file.WriteLine("    <div class=\"col-xs-12 col-sm-9\">");
            file.WriteLine("");
            file.WriteLine("        <div class=\"profile-user-info profile-user-info-striped\">");
            foreach (Column column in table.columns)
            {
                if (column.name.StartsWith("Res"))
                {
                    continue;
                }
                file.WriteLine("            <div class=\"profile-info-row\">");
                file.WriteLine("                <div class=\"profile-info-name\">");
                file.WriteLine("                    " + column.description);
                file.WriteLine("                </div>");
                file.WriteLine("                <div class=\"profile-info-value\">");

                switch (column.columntype)
                {
                    case ColumnType.Image:
                        break;
                    case ColumnType.File:
                        file.WriteLine("                    <span class=\"\" id=\"username\"> <a href=\"@Url.Content(\"~\" + Model." + column.name + ")\">File Download</a> </span>");
                        break;
                    case ColumnType.HtmlText:
                        //
                        file.WriteLine("                    <span class=\"\" >  @Html.Raw(Model." + column.name + ")</span>");

                        break;

                    case ColumnType.ComboBox:
                        //
                        file.WriteLine("                    <span class=\"\" > @Html.DisplayFor(model => model._" + column.foreign.table.name + "." + column.foreign.table.GetNameColumn().name + ") </span>");

                        break;
                    case ColumnType.Date:
                        //normal yazılacak
                        file.WriteLine("                    <span class=\"\" > @myLibrary.Tools.GetFormatStringDate(Model." + column.name + ") </span>");

                        break;
                    default:
                        //normal yazılacak
                        file.WriteLine("                    <span class=\"\" > @Html.DisplayFor(model => model." + column.name + ") </span>");

                        break;
                }
                file.WriteLine("                </div>");
                file.WriteLine("            </div>");

            }

            file.WriteLine("");
            file.WriteLine("        </div>");
            file.WriteLine("");
            file.WriteLine("    </div>");
            file.WriteLine("</div>");

           file.WriteLine(" <div class=\"space-12\">");
           file.WriteLine("</div>");

           if (table.ref_foreign.Count > 0)
           {
               writeReferanceTab(file, table);
           }


        }

        public static void writeReferanceTab(StreamWriter file, Table table)
        {
            int i = 0;
            string activeclass = "class=\"active\"";
file.WriteLine("<div id=\"user-profile-2\" class=\"user-profile\">");
file.WriteLine("<div class=\"tabbable\">");
file.WriteLine("<ul class=\"nav nav-tabs padding-18\">");

foreach (ForeignKey foreign in table.ref_foreign)
{
    if(i!=0)activeclass="";
    file.WriteLine("<li " + activeclass + ">");
    file.WriteLine("<a data-toggle=\"tab\" href=\"#" + foreign.table.name + "\"><i class=\"orange ace-icon fa fa-picture-o bigger-120\"></i>" + foreign.table.primery_column.description + " </a>");
    file.WriteLine("</li>");
    i++;
}

        file.WriteLine("</ul>");


        file.WriteLine("<div class=\"tab-content no-border padding-24\">");

             i = 0;
             activeclass = " in active";
             foreach (ForeignKey foreign in table.ref_foreign)
             {
                 if (i != 0) activeclass = "";
                 file.WriteLine("<div id=\"" + foreign.table.name + "\" class=\"tab-pane " + activeclass + "\">");
                 file.WriteLine("<div class=\"profile-feed row\">");
                 file.WriteLine("<div class=\"col-sm-10\">");
                 file.WriteLine(" @Html.Action(\"_" + foreign.table.name + "\", \"Admin" + foreign.table.name + "\", new { " + table.primery_column.name + " = Model." + table.primery_column.name + " })");

                 file.WriteLine("</div>");
                 file.WriteLine("</div>");

                 file.WriteLine("<!-- /.row -->");
                 file.WriteLine("<div class=\"space-12\">");
                 file.WriteLine("</div>");
                 file.WriteLine("</div>");
                 i++;
             }
         
        file.WriteLine("</div>");
    file.WriteLine("</div>");
    file.WriteLine("</div>");
        }

        public static void WriteTableHeader(StreamWriter file, Table table, string targetdivID, ForeignKey foreign)
        {
            file.WriteLine("<th class=\"dragHandle\">");
            file.WriteLine("&nbsp;");
            file.WriteLine("</th>");
            if (foreign != null)
            {
                file.WriteLine("<th>");
                file.WriteLine("</th>");
            }
            foreach (Column item in table.columns)
            {
                if (item.name.StartsWith("Res"))
                    continue;

                switch (item.columntype)
                {
                    case ColumnType.Image:
                        file.WriteLine("<th>");
                        file.WriteLine(item.description);
                        file.WriteLine("</th>");
                        break;
                    case ColumnType.File:
                        file.WriteLine("<th>");
                        file.WriteLine(item.description);
                        file.WriteLine("</th>");
                        break;
                    default:
                        break;
                }
            }
            foreach (Column item in table.columns)
            {
                if (item.columntype == ColumnType.HtmlText) continue;
                if (item.columntype == ColumnType.TextArea) continue;
                if (item.columntype == ColumnType.Primery) continue;
                if (item.columntype == ColumnType.Image) continue;
                if (item.columntype == ColumnType.File) continue;

                file.WriteLine("<th>");
                switch (item.columntype)
                {
                    case ColumnType.Date:
                    case ColumnType.ComboBox:
                    case ColumnType.CheckBox:
                    case ColumnType.Name:

                        file.WriteLine("@{");
                        file.WriteLine("RouteValueProvider.AddValue(tempdic, \"OrderBy\", \"" + item.name + "\");");
                        file.WriteLine("RouteValueProvider.AddValue(tempdic, \"OrderType\", \"asc\");");
                        file.WriteLine("if (ViewBag.OrderBy != null) { if (ViewBag.OrderBy == \"" + item.name + "\") { RouteValueProvider.AddValue(tempdic, \"OrderType\", \"desc\"); } }");
                        file.WriteLine("}");
                        string title = (item.description == String.Empty) ? item.name : item.description;
                        file.WriteLine("@Ajax.ActionLink(\"" + title + "\", \"_" + table.name + "\", tempdic, new AjaxOptions { UpdateTargetId = \"" + targetdivID + "\" , OnBegin = \"showSpinner\", OnComplete = \"hideSpinner\" })");

                        break;

                    default:
                        file.WriteLine(item.description);
                        break;
                }
                file.WriteLine("</th>");
            }

            file.WriteLine(" <th class=\"action\">");
            file.WriteLine("İşlemler");
            file.WriteLine("</th>");
        }

        public static void WriteTableHeaderAce(StreamWriter file, Table table, ForeignKey foreign)
        {
            file.WriteLine("<th class=\"center\">");
            file.WriteLine("<label class=\"pos-rel\">");
            file.WriteLine("<input type=\"checkbox\" class=\"ace\" />");
            file.WriteLine("<span class=\"lbl\"></span>");
            file.WriteLine("</label>");
            file.WriteLine("</th>");

            int remain_column_count = 5;

            foreach (Column item in table.columns)
            {
                if (item.name.StartsWith("Res"))
                    continue;

                if (item.isNullable == true)
                    continue;

                if (remain_column_count == 0)
                    break;

                switch (item.columntype)
                {
                    case ColumnType.Image:
                        remain_column_count--;
                        file.WriteLine("<th class=\"hidden-480\">");
                        file.WriteLine(item.description);
                        file.WriteLine("</th>");
                        break;
                    default:
                        break;
                }
            }

            string classhidden = "";
            foreach (Column item in table.columns)
            {
                classhidden = "class=\"hidden-480\"";
                if (item.name.StartsWith("Res"))
                    continue;

                if (item.isNullable == true)
                    continue;

                if (remain_column_count == 0)
                    break;

                if (item.columntype == ColumnType.HtmlText) continue;
                if (item.columntype == ColumnType.TextArea) continue;
                if (item.columntype == ColumnType.Primery) continue;
                if (item.columntype == ColumnType.File) continue;
                if (item.columntype == ColumnType.Image) continue;

                switch (item.columntype)
                {
                    case ColumnType.ComboBox:
                    case ColumnType.Name:
                        classhidden = "";
                        break;

                    default:
                        break;
                }

                file.WriteLine("<th " + classhidden + ">");
                file.WriteLine(item.description);
                file.WriteLine("</th>");
                remain_column_count--;
            }

            file.WriteLine(" <th> ");
            file.WriteLine("İşlemler");
            file.WriteLine("</th>");

            if (remain_column_count > 0)
            {
                for (int i = 0; i <remain_column_count; i++)
                {
                    file.WriteLine("<th class=\"hidden-480\">");
                    file.WriteLine("</th>");
                }
            }
        }

        public static void WriteTableData(StreamWriter file, Table table, ForeignKey foreign)
        {
            file.WriteLine("<td class=\"dragHandle\">");
            file.WriteLine("&nbsp;");
            file.WriteLine("</td>");

            if (foreign != null)
            {
                file.WriteLine("<td>");
                file.WriteLine("<a style=\"font-size:24px; cursor:pointer;  color:Blue;\" title=\"" + foreign.table.primery_column.description + " Görmek İçin Tıklayın.\" onclick=\"openCloseDiv(@item." + table.primery_column.name + ")\">+</a>");
                file.WriteLine("</td>");
            }
            foreach (Column item in table.columns)
            {
                if (item.name.StartsWith("Res"))
                    continue;

                switch (item.columntype)
                {
                    case ColumnType.Image:
                        file.WriteLine("<td>");
                        file.WriteLine("<a class=\"picuploader\" href=\"#uploadpic" + item.name + "\" onclick=\"set" + item.name + "ID(@item." + table.primery_column.name + ")\">");
                        file.WriteLine("@if (item." + item.name + " != null) ");
                        file.WriteLine("{");
                        file.WriteLine("<img src=\"@Url.Content(item." + item.name + ")\" style=\"width:50px; height:50px;\" alt=\"\" />");
                        file.WriteLine("}");
                        file.WriteLine("else");
                        file.WriteLine("{");
                        file.WriteLine("<img src=\"@Url.Content(\"~/Content/default.jpg\")\" style=\"width:50px; height:50px;\" alt=\"\" />");
                        file.WriteLine("}");
                        file.WriteLine("</a>");
                        file.WriteLine("</td>");
                        break;
                    case ColumnType.File:
                        file.WriteLine("<td>");
                        file.WriteLine("<a class=\"picuploader\" href=\"#uploadpic" + item.name + "\" onclick=\"set" + item.name + "ID(@item." + table.primery_column.name + ")\">");
                        file.WriteLine("<img src=\"@Url.Content(\"~/Content/images/default.jpg\")\" style=\"width:24px; height:24px;\" alt=\"\" />");
                        file.WriteLine("</a>");
                        file.WriteLine("@if (item." + item.name + " != null) ");
                        file.WriteLine("{");
                        file.WriteLine("<a href=\"@Url.Content(item." + item.name + ")\" target=\"_blank\" >@item." + item.name + ".Substring(item." + item.name + ".LastIndexOf('/')+1)</a> ");
                        file.WriteLine("}");
                        file.WriteLine("</td>");
                        break;
                    default:
                        break;
                }
            }
            foreach (Column item in table.columns)
            {
                if (item.columntype == ColumnType.HtmlText) continue;
                if (item.columntype == ColumnType.TextArea) continue;
                if (item.columntype == ColumnType.Primery) continue;
                if (item.columntype == ColumnType.Image) continue;
                if (item.columntype == ColumnType.File) continue;

                file.WriteLine("<td>");
                if (item.isForeign)
                    file.WriteLine("@item._" + item.foreign.table.name + "." + item.foreign.table.GetNameColumn().name);
                else
                    file.WriteLine("@item." + item.name);
                file.WriteLine("</td>");
            }

            file.WriteLine(" <td class=\"action\">");
            if (foreign != null)
            {
                file.WriteLine("<a href=\"@Url.Action(\"Delete\", \"Admin" + table.name + "\", new { id = item." + table.primery_column.name + " ,RedirectUrl = HttpContext.Current.Request.Url.OriginalString })\" onclick=\"delete" + table.name + "()\" class=\"ico ico-delete\" title=\"Sil\">");
                file.WriteLine("Sil</a> <a href=\"@Url.Action(\"Edit\", \"Admin" + table.name + "\", new { id = item." + table.primery_column.name + " })\" class=\"ico ico-edit\" title=\"Düzenle\">");
                file.WriteLine("Düzenle</a> <a href=\"@Url.Action(\"Details\", \"Admin" + table.name + "\", new { id = item." + table.primery_column.name + " })\" class=\"ico ico-detail\" title=\"Ayrıntılar\">");
                file.WriteLine("Detay</a> <a href=\"@Url.Action(\"CreateWith" + table.name + "\", \"Admin" + foreign.table.name + "\", new { id = item." + table.primery_column.name + " })\" class=\"ico ico-detail\" title=\"Ödeme Ekle\">");
                file.WriteLine("" + foreign.table.primery_column.description + "</a>");
            }
            else
            {
                file.WriteLine("<a href=\"@Url.Action(\"Delete\", new { id = item." + table.primery_column.name + ",RedirectUrl = HttpContext.Current.Request.Url.OriginalString })\" onclick=\"areyousure()\" class=\"ico ico-delete\" title=\"Sil\">");
                file.WriteLine("Sil</a> <a href=\"@Url.Action(\"Edit\", new { id = item." + table.primery_column.name + " })\" class=\"ico ico-edit\" title=\"Düzenle\">");
                file.WriteLine("Düzenle</a> <a href=\"@Url.Action(\"Details\", new { id = item." + table.primery_column.name + " })\" class=\"ico ico-detail fancyopener\"  title=\"Ayrıntılar\">");
                file.WriteLine("Detay</a>");
            }
            file.WriteLine("</td>");
        }

        public static void WriteTableDataAce(StreamWriter file, Table table, ForeignKey foreign,string fparam)
        {
            file.WriteLine("<td class=\"center\">");
            file.WriteLine("<label class=\"pos-rel\">");
            file.WriteLine("<input type=\"checkbox\" class=\"ace\" />");
            file.WriteLine(" <span class=\"lbl\"></span>");
            file.WriteLine("</label>");
            file.WriteLine(" </td>");
            int remain_column_count = 5;

            foreach (Column item in table.columns)
            {
                if (item.name.StartsWith("Res"))
                    continue;
                if (item.isNullable == true)
                    continue;

                if (remain_column_count == 0)
                    break;

                switch (item.columntype)
                {
                    case ColumnType.Image:
                        remain_column_count--;
                        file.WriteLine("<td class=\"hidden-480\">");
                        file.WriteLine(" <img src=\"@Url.Content(myLibrary.Tools.GetDefaultFilePath(item." + item.name + "))\" class=\"table-img\" alt=\"@item." + item.name + "\" title=\"@item." + item.name + "\" />");
                        file.WriteLine("</td>");
                        break;
                    default:
                        break;
                }
            }
            string classhidden = "";


            foreach (Column item in table.columns)
            {
                classhidden = "class=\"hidden-480\"";
              
                if (item.name.StartsWith("Res"))
                    continue;

                if (item.isNullable == true)
                    continue;

                if (remain_column_count == 0)
                    break;

                if (item.columntype == ColumnType.HtmlText) continue;
                if (item.columntype == ColumnType.TextArea) continue;
                if (item.columntype == ColumnType.Primery) continue;
                if (item.columntype == ColumnType.Image) continue;
                if (item.columntype == ColumnType.File) continue;

                switch (item.columntype)
                {
                    case ColumnType.ComboBox:
                    case ColumnType.Name:
                        classhidden = "";
                        break;

                    default:
                        break;
                }

                file.WriteLine("<td " + classhidden + ">");
                if (item.isForeign)
                    file.WriteLine("@item._" + item.foreign.table.name + "." + item.foreign.table.GetNameColumn().name);
                else
                    file.WriteLine("@item." + item.name);
                file.WriteLine("</td>");
                remain_column_count--;
            }

            file.WriteLine("<td>");

            file.WriteLine("<div class=\"hidden-sm hidden-xs action-buttons\">");
            file.WriteLine("<a class=\"blue tooltip-info\" data-rel=\"tooltip\" title=\"DETAY\" href=\"@Url.Action(\"Details\", new { id = item." + table.primery_column.name + " })\"><i class=\"ace-icon fa fa-search-plus bigger-130\"></i></a>");

            file.WriteLine("<a class=\"green tooltip-info\" data-rel=\"tooltip\" title=\"GÜNCELLE\"  href=\"@Url.Action(\"Edit\", new { id = item." + table.primery_column.name + fparam + " })\"><i class=\"ace-icon fa fa-pencil bigger-130\"></i></a>");

            file.WriteLine("<a class=\"red tooltip-info\" data-rel=\"tooltip\" title=\"KALDIR\" href=\"#\" data-toggle=\"modal\" data-target=\"#confirm-delete\" data-href=\"@Url.Action(\"Delete\", new { id = item." + table.primery_column.name + ", RedirectUrl = HttpContext.Current.Request.Url.OriginalString })\"> <i class=\"ace-icon fa fa-trash-o bigger-130\"></i></a>");
            file.WriteLine("</div>");
            file.WriteLine("<div class=\"hidden-md hidden-lg\">");
            file.WriteLine("<div class=\"inline pos-rel\">");
            file.WriteLine("<button class=\"btn btn-minier btn-yellow dropdown-toggle\" data-toggle=\"dropdown\" data-position=\"auto\">");
            file.WriteLine("<i class=\"ace-icon fa fa-caret-down icon-only bigger-120\"></i>");
            file.WriteLine("</button>");
            file.WriteLine("<ul class=\"dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close\">");
            file.WriteLine("<li><a href=\"@Url.Action(\"Details\", new { id = item." + table.primery_column.name+" })\" class=\"tooltip-info\" data-rel=\"tooltip\" title=\"Detay\"><span class=\"blue\">");
            file.WriteLine("<i class=\"ace-icon fa fa-search-plus bigger-120\"></i></span></a></li>");
            file.WriteLine("<li><a href=\"@Url.Action(\"Edit\", new { id = item." + table.primery_column.name + fparam +" })\" class=\"tooltip-success\" data-rel=\"tooltip\" title=\"Güncelle\"><span class=\"green\">");
            file.WriteLine("<i class=\"ace-icon fa fa-pencil-square-o bigger-120\"></i></span></a></li>");
            file.WriteLine("<li><a href=\"#\" data-toggle=\"modal\" data-target=\"#confirm-delete\" data-href=\"@Url.Action(\"Delete\", new { id = item." + table.primery_column.name + ", RedirectUrl = HttpContext.Current.Request.Url.OriginalString })\" class=\"tooltip-error\" data-rel=\"tooltip\" title=\"Kaldır\"><span class=\"red\">");
            file.WriteLine("<i class=\"ace-icon fa fa-trash-o bigger-120\"></i></span></a></li>");
            file.WriteLine("</ul>");
            file.WriteLine("</div>");
            file.WriteLine("</div>");

            file.WriteLine("</td>");

            if (remain_column_count > 0)
            {
                for (int i = 0; i <remain_column_count; i++)
                {
                    file.WriteLine("<td class=\"hidden-480\">");
                    file.WriteLine("</td>");
                }
            }


        }
        #endregion

        #region Sorting
        public static void WriteSort(StreamWriter file, Table table)
        {
            foreach (Column item in table.columns)
            {
                switch (item.columntype)
                {

                    case ColumnType.Date:
                        break;
                    case ColumnType.CheckBox:
                        break;
                    case ColumnType.ComboBox:
                        break;
                    case ColumnType.Primery:
                        break;
                    case ColumnType.Name:
                        break;
                    default:
                        break;
                }
            }
        }

        #endregion

        #region Paging
        public static void WriteActionDiv(StreamWriter file, Table table, string targetDivID)
        {
            file.WriteLine("");
            file.WriteLine("<div id=\"" + targetDivID + "\">");
            file.WriteLine("@Html.Action(\"_" + table.name + "\", new { page = 1 })");
            file.WriteLine("</div>");
            file.WriteLine("");
        }

        public static void WritePagination(StreamWriter file, Table table, string TargetDivID)
        {
            file.WriteLine("<div class=\"pagination red\">");
            file.WriteLine("@{ RouteValueDictionary dic = RouteValueProvider.GetCurrentRouteValues(Url.RequestContext.RouteData.Values, Request.QueryString);");
            file.WriteLine("dic[\"action\"] = \"_" + table.name + "\";");

            file.WriteLine("}");
            file.WriteLine("@Html.Raw(Ajax.Pager(new AjaxOptions");
            file.WriteLine("{");
            file.WriteLine("UpdateTargetId = \"" + TargetDivID + "\", OnBegin = \"showSpinner\", OnComplete = \"hideSpinner\" ");
            file.WriteLine("}, Model.PageSize, Model.PageIndex+1, Model.PageCount, dic))");
            file.WriteLine("<ul>");
            file.WriteLine("</ul>");
            file.WriteLine("</div>");
        }

        #endregion

        #region Index-Scripts-Header

        public static void WriteHeadLineWithFilter(StreamWriter file, Table table)
        {
            file.WriteLine("");
            file.WriteLine("<div class=\"headlines\">");
            file.WriteLine("<h2>");
            file.WriteLine("<span>" + table.primery_column.description + " İşlemleri</span></h2>");
            file.WriteLine("<a href=\"#\" class=\"show-filter\">filtreyi göster</a>");
            file.WriteLine("</div>");
            file.WriteLine("");
        }

        public static void WriteHeadLine(StreamWriter file, Table table)
        {
            file.WriteLine("");
            file.WriteLine("<div class=\"box profile\">");
            file.WriteLine("<div class=\"headlines\">");
            file.WriteLine("<h2>");
            file.WriteLine("<span>" + table.primery_column.description + " Detayları</span></h2>");
            file.WriteLine("</div>");
            file.WriteLine("");






        }

        public static void WriteHeadLineWithHelp(StreamWriter file, Table table, string header)
        {
            file.WriteLine("");
            file.WriteLine("<div class=\"headlines\">");
            file.WriteLine("<h2>");
            file.WriteLine("<span>" + table.primery_column.description + " " + header + "</span></h2>");
            file.WriteLine("<a href=\"#help\" class=\"help\"></a>");
            file.WriteLine("</div>");
            file.WriteLine("");
        }

        public static void WriteScriptForBasic(StreamWriter file, Table table)
        {
            file.WriteLine("<script type=\"text/javascript\">");
            file.WriteLine("function areyousure() {");
            file.WriteLine("return confirm(\"Bu " + table.primery_column.description + " silmek istediğinize eminmisiniz?\");");
            file.WriteLine("}");
            bool isfile = false;
            foreach (Column item in table.columns)
            {
                if (item.columntype == ColumnType.Image || item.columntype == ColumnType.File)
                {
                    isfile = true;
                    file.WriteLine("function set" + item.name + "ID(id) {");
                    file.WriteLine("document.getElementById(\"" + item.name + "PrimaryID\").value = id;");
                    file.WriteLine("}");
                }
            }
            if (isfile)
            {
                file.WriteLine("$(document).ready(function () {");

                file.WriteLine("$(\".picuploader\").fancybox({");
                file.WriteLine("'titlePosition': 'inside',");
                file.WriteLine("'transitionIn': 'none',");
                file.WriteLine("'transitionOut': 'none'");
                file.WriteLine("});");
                file.WriteLine("});");
            }
            file.WriteLine("</script>");


        }

        public static void WriteScriptForReferance(StreamWriter file, Table table, ForeignKey foreign)
        {
            file.WriteLine("<script type=\"text/javascript\">");
            file.WriteLine("function openCloseDiv(id) {");
            file.WriteLine("if (document.getElementById(\"SubDiv\" + id).style.height == \"auto\") {");
            file.WriteLine("document.getElementById(\"SubDiv\" + id).style.height = \"0px\";");
            file.WriteLine("document.getElementById(\"SubDiv\" + id).style.display = \"none\";");
            file.WriteLine("}");
            file.WriteLine("else {");
            file.WriteLine("document.getElementById(\"SubDiv\" + id).style.height = \"auto\";");
            file.WriteLine("document.getElementById(\"SubDiv\" + id).style.display = \"table\";");
            file.WriteLine("}");
            file.WriteLine("}");

            file.WriteLine("function delete" + table.name + "(id) {");
            file.WriteLine("if (con" + table.name + "(\"Bu Soruyu Silmek İstediğinize Emin misiniz?\")) {");
            file.WriteLine("window.location = '@Url.Action(\"Delete\", \"Admin" + table.name + "\")/' + id;");
            file.WriteLine("}");
            file.WriteLine("else return false;");
            file.WriteLine("}");

            file.WriteLine("function delete" + foreign.table.name + "(id) {");
            file.WriteLine("if (con" + table.name + "(\"Bu Seçeneği Silmek İstediğinize Emin misiniz?\")) {");
            file.WriteLine("window.location = '@Url.Action(\"Delete\", \"Admin" + table.name + "" + foreign.table.name + "\")/' + id;");
            file.WriteLine("}");
            file.WriteLine("else return false;");
            file.WriteLine("}");
            file.WriteLine("</script>");
        }
        #endregion

    }
}
