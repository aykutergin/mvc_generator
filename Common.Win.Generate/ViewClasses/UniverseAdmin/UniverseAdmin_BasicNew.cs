﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Avs.Win.Generate
{
    class UniverseAdmin_BasicNew:IView
    {
         public Table table { get; set; }
         public string filePath { get; set; }

         public UniverseAdmin_BasicNew(Table table)
        {
           this.table= table;
           this.filePath = Constants.ViewTableFolder + "\\Create.cshtml";
        }
        #region IView Members

        public void Write()
        {

            StreamWriter file = new StreamWriter(filePath, false, Encoding.GetEncoding("iso-8859-9"));//(filePath,false, Encoding.UTF8);

            file.WriteLine("@model Avs.Web.MgmtReport.Models."+table.name+"FormModel");

            file.WriteLine("@{");
            file.WriteLine("Layout = \"~/Views/Shared/_LayoutHome.cshtml\";");
            file.WriteLine("}");


            file.WriteLine("@section _title{");
            file.WriteLine(table.primery_column.description);
            file.WriteLine("}");
            file.WriteLine("@section _scripts{");
            file.WriteLine("<script src = \"@Url.Content(\"~/Content/wp-content/js/preview/datepicker.min.js\")\" ></script>");

            file.WriteLine("}");


            file.WriteLine("<div class=\"container-fluid\">");
            file.WriteLine("<div class=\"page-content__header\">");
            file.WriteLine("<div class=\"page__body page__body--container\">");
            file.WriteLine("<div class=\"container-tabs\">");
            file.WriteLine("<ul class=\"nav nav-tabs\">");
            file.WriteLine("<li class=\"nav-item\">");
            file.WriteLine("<a class=\"nav-link active\" data-toggle=\"tab\" href=\"#settings-account-settings\">"+table.primery_column.description+"</a>");
                    file.WriteLine("</li>");

            file.WriteLine("</ul>");
            file.WriteLine("<div class=\"tab-content\">");
            file.WriteLine("<div class=\"tab-pane active\" id=\"settings-account-settings\">");
            ///////////////////
            ViewManager.WriteUniverseEditor(file, table, "Create", null);

            file.WriteLine(" </div>");

            file.WriteLine("</div>");
            file.WriteLine("</div>");
            file.WriteLine("</div>");
            file.WriteLine("</div>");
            file.WriteLine("</div>");

            
           file.Close();
        }

        #endregion
    }
}
