﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Avs.Win.Generate
{
    class UniverseAdmin_BasicIndex:IView
    {
         public Table table { get; set; }

         public string filePath { get; set; }

         public UniverseAdmin_BasicIndex(Table table)
        {
           this.table= table;
           this.filePath = Constants.ViewTableFolder+"\\Index.cshtml";
        }

        #region IView Members

        public void Write()
        {

            StreamWriter file = new StreamWriter(filePath, false, Encoding.GetEncoding("iso-8859-9"));

            file.WriteLine(" @model Avs.Web.MgmtReport.Models."+table.name+"ViewModel");



            file.WriteLine("@{");
            file.WriteLine(" Layout = \"~/Views/Shared/_LayoutHome.cshtml\";");
            file.WriteLine("}");

            file.WriteLine(" @section _title{");

            file.WriteLine(table.primery_column.description);

            file.WriteLine("}");

            file.WriteLine("@section _styles{");
            file.WriteLine("<link rel = \"stylesheet\" href = \"@Url.Content(\"~/Content/wp-content/vendor/datatables/datatables.min.css\")\">");
            file.WriteLine("}");


            file.WriteLine(" @section _scripts{");





            file.WriteLine("<script src =\"@Url.Content(\"~/Content/wp-content/vendor/jquery-circle-progress/circle-progress.min.js\")\"></script>");

            file.WriteLine("<script src =\"@Url.Content(\"~/Content/wp-content/js/preview/default-dashboard.min.js\")\"></script>");
            file.WriteLine("<script src =\"@Url.Content(\"~/Content/wp-content/vendor/datatables/datatables.min.js\")\"></script>");
            file.WriteLine("<script src =\"@Url.Content(\"~/Content/wp-content/js/preview/datatables.min.js\")\"></script>");


            file.WriteLine("}");


            file.WriteLine("<div class=\"container-fluid\">");

            file.WriteLine("<div class=\"page-content__header\">");
            file.WriteLine("<div>");
            file.WriteLine("<h2 class=\"page-content__header-heading\">"+table.primery_column.description+"</h2>");
            file.WriteLine("</div>");
            file.WriteLine("</div>");
            file.WriteLine("<div class=\"m-datatable\">");
            file.WriteLine("<table id =\"datatable\" class=\"table table-striped\">");
            file.WriteLine("<thead>");
            file.WriteLine("<tr>");
            foreach (var item in table.columns)
            {
                file.WriteLine("<th>"+item.description+"</th>");
            }
         
            
            file.WriteLine(" </tr>");
            file.WriteLine("</thead>");
            file.WriteLine("<tbody>");
            file.WriteLine("@foreach (var item in Model._"+table.name+"List)");
            file.WriteLine("{");
            file.WriteLine("<tr >");

            foreach (var item in table.columns)
            {
                if(item.columntype==ColumnType.ComboBox)
                    file.WriteLine("<td> @item."+item.foreign.table.name+"." + item.foreign.table.GetNameColumn().name + " </td >");
                else
                file.WriteLine("<td> @item."+item.name+" </td >");
            }

          
            file.WriteLine("</tr >");
            file.WriteLine("}");


            file.WriteLine("</tbody>");
            file.WriteLine("</table>");
            file.WriteLine("</div>");
            file.WriteLine("</div>");
            
          

            file.Close();


        }


        #endregion
    }
}
