﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Avs.Win.Generate
{
    class NNRelController
    {
        public Table table { get; set; }
        public MenuManager menumanager { get; set; }
        public NNForeignKey nnforeign { get; set; }

        public NNRelController(Table table,NNForeignKey nnforeign,MenuManager menumanager)
        {
            this.table = table;
            this.nnforeign = nnforeign;
            this.menumanager = menumanager;
        }
        public void Write()
        {
            //ortak satırlar
            if (nnforeign.targetTable.name.ToLower().Contains("imageurl"))
            {
                //image controller view
            }
            else if (nnforeign.targetTable.name.ToLower().Contains("fileurl"))
            {
                //file controller view
            }
            else
            {
                //index controller
            }
            GreatAdmin_NNRelIndex nnrelIndex = new GreatAdmin_NNRelIndex(nnforeign, table);
            nnrelIndex.Write();

            GreatAdmin_NNRelEdit nnrelEdit = new GreatAdmin_NNRelEdit(nnforeign, table);
            nnrelEdit.Write();

            GreatAdmin_NNRelNew nnrelNew = new GreatAdmin_NNRelNew(nnforeign, table);
            nnrelNew.Write();

            GreatAdmin_NNRelDetail nnrelDetail = new GreatAdmin_NNRelDetail(nnforeign, table);
            nnrelDetail.Write();
            
        }
    }
}
