﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Avs.Win.Generate
{
    public class ControllerSharedClasses
    {
        #region namespace
        
        public static void WriteNamespace(StreamWriter file)
        {
            file.WriteLine("using System;");
            file.WriteLine("using System.Collections.Generic;");
            file.WriteLine("using System.Web;");
            file.WriteLine("using System.Web.Mvc;");
            file.WriteLine("using myLibrary.Classes;");
            file.WriteLine("using MvcPaging;");
            file.WriteLine("using myLibrary.Models;");
            file.WriteLine("using System.Web.Helpers;");
            file.WriteLine("using System.IO;");
        }
        #endregion

        #region filter-search
        public static void WriteFilter(StreamWriter file, Table table)
        {
            string paramfilter = GetFilterParameter(table);

            if (paramfilter == String.Empty) return;

            file.WriteLine("[HttpPost]");
            file.WriteLine("public ActionResult Index(" + paramfilter + ")");
            file.WriteLine("{");
            file.WriteLine("try");
            file.WriteLine("{");
            file.WriteLine("SearchList list=new SearchList();");

          
            foreach (Column item in table.columns)
            {
                switch (item.columntype)
                {
                    case ColumnType.Date:
                        file.WriteLine("if (DateFirst" + item.name + " != \"\" && DateLast" + item.name + " != \"\")");
                        file.WriteLine("{");
                     

                        file.WriteLine("DateTime lDate = Convert.ToDateTime(DateLast" + item.name + ");");
                        file.WriteLine("lDate = lDate.AddDays(1);");
                        file.WriteLine(" DateTime  fDate = Convert.ToDateTime(DateFirst" + item.name + ");");
                        file.WriteLine("list.ItemList.Add(new SearchListItem(\"" + item.name + "\", fDate,lDate, SearchTypes.Between));");
                        file.WriteLine("}");
                        //date search
                        break;
                    case ColumnType.Name:
                        file.WriteLine("if (Name" + item.name + " != \"\") {");

                        file.WriteLine("list.ItemList.Add(new SearchListItem(\"" + item.name + "\", Name" + item.name + ", SearchTypes.Like));}");
                        //name = search
                        break;
                    case ColumnType.CheckBox:
                        file.WriteLine("if (Status" + item.name + " != \"\") {");
                      

                        file.WriteLine("list.ItemList.Add(new SearchListItem(\"" + item.name + "\", Status" + item.name + ", SearchTypes.Equal));}");
                        //aktif - pasif
                        break;
                    case ColumnType.ComboBox:
                        file.WriteLine("if (" + item.name + " != 0) {");
                      

                        file.WriteLine("list.ItemList.Add(new SearchListItem(\"" + item.name + "\", " + item.name + ".ToString(), SearchTypes.Equal));}");
                        //foreign search
                        break;
                    default:
                        break;
                }
            }
            file.WriteLine("AddAllComboboxToViewBag(true);");
            file.WriteLine("" + table.name + " " + table.name + " = new " + table.name + "();");


        
        if (table.tabletype == TableType.Image || table.tabletype == TableType.File)
        {
            file.WriteLine("return View(" + table.name + ".Get" + table.name + "(list));");
        }
        else
        {
            file.WriteLine("return View(" + table.name + ".Get" + table.name + "(list));");
        }
            file.WriteLine("}");
            file.WriteLine("catch (Exception ex)");
            file.WriteLine("{");
            file.WriteLine("LogClass.LogToFileException(ex, \"_" + table.name + ", Admin" + table.name + "Controller\");");
            file.WriteLine("return RedirectToAction(\"Index\");");
            file.WriteLine("}");
            file.WriteLine("}");
        }

        public static void WriteInsideSort(StreamWriter file, Table table,ForeignKey foreign)
        {

            file.WriteLine("public ActionResult _" + table.name +foreign.table.name+ "(" + GetInsideSortParameter(foreign) + ")");
            file.WriteLine("{");
                file.WriteLine("try");
                file.WriteLine("{");
                file.WriteLine("" + table.name + " _" + table.name + " = new " + table.name + "();");
                file.WriteLine("ViewBag.ID = id;");
                    file.WriteLine("SearchList list;");
                    file.WriteLine("if (OrderBy == \"\")");
                    file.WriteLine("list = new SearchList();");
                    file.WriteLine("else");
                    file.WriteLine("{");
                    file.WriteLine("list = new SearchList(OrderBy, OrderType);");
                    file.WriteLine("if(OrderType==\"asc\")");
                    file.WriteLine("{");
                    file.WriteLine("ViewBag.OrderBy = OrderBy;");
                    file.WriteLine("}");
                    file.WriteLine("}");
                    if (foreign.table.tabletype == TableType.Image || foreign.table.tabletype == TableType.File)
                    {
                        file.WriteLine("return View(_" + table.name + ".Get" + table.name + "With" + foreign.table.name + "(id,list)." + foreign.table.name + "list);");
                    }
                    else 
                    {
                        file.WriteLine("return View(_" + table.name + ".Get" + table.name + "With" + foreign.table.name + "(id,list)." + foreign.table.name + "list.ToPagedList(page, 10));");
                    }
            
            file.WriteLine("}");
                file.WriteLine("catch (Exception ex)");
                file.WriteLine("{");
                    file.WriteLine("LogClass.LogToFileException(ex, \"_" + table.name + ", Admin" + table.name + "Controller\");");
                    file.WriteLine("return RedirectToAction(\"Index\");");
                file.WriteLine("}");
            file.WriteLine("}");
        }

        public static string GetFilterParameter(Table table)
        {
            string parameter = String.Empty;
          
            foreach (Column item in table.columns)
            {
                switch (item.columntype)
                {
                    case ColumnType.Date:
                        parameter = parameter + "string DateFirst" + item.name + " = \"\",";
                        parameter = parameter + "string DateLast" + item.name + " = \"\",";
                        //date search
                        break;
                    case ColumnType.Name:
                        parameter = parameter + "string Name" + item.name + " = \"\",";
                        //name = search
                        break;
                    case ColumnType.CheckBox:
                        parameter = parameter + "string Status" + item.name + " = \"\",";
                        //aktif - pasif
                        break;
                    case ColumnType.ComboBox:
                        parameter = parameter + "int " + item.name + " = 0,";
                        //foreign search
                        break;
                    default:
                        break;

                 }

            }
            if (parameter != String.Empty)
            {
                parameter = parameter.Remove(parameter.LastIndexOf(','));
            }
            return parameter;
        }

        public static string GetForeignParameter(Table table)
        {
            string parameter = String.Empty;
            if (table.foreigns.Count < 1) return parameter;

            foreach (ForeignKey item in table.foreigns)
            {
                parameter = parameter + String.Format("{0} {1} = 0,", item.column.dataType, item.column.name);
               
            }
            parameter = parameter.Remove(parameter.LastIndexOf(','));
            return parameter;
        }

        public static string GetInsideSortParameter(ForeignKey foreign)
        {
            return foreign.column.dataType + " id, int page = 1, string OrderBy = \"\", string OrderType = \"\"";
        }

        #endregion

        #region add combobox to viewbag

        public static void AddForeignToViewBag(StreamWriter file, Table table)
        {
            foreach (ForeignKey item in table.foreigns)
            {
                file.WriteLine("private void Add" + item.table.name + "ToViewBagAs"+item.column.name+"(bool isAll)");
                file.WriteLine("{");
                file.WriteLine("try");
                file.WriteLine("{");
                file.WriteLine("" + item.table.name + " _" + item.table.name + " = new " + item.table.name + "();");
                file.WriteLine("List<SelectListItem> list = _" + item.table.name + ".Get" + item.table.name + "AsSelectList();");
                file.WriteLine("if (isAll)");
                file.WriteLine("{");
                file.WriteLine("SelectListItem sli = new SelectListItem();");
                file.WriteLine("sli.Value = 0 + \"\";");
                file.WriteLine("sli.Text = \"Hepsi\";");
                file.WriteLine("list.Insert(0, sli);");
                file.WriteLine("}");
                file.WriteLine("ViewBag." + item.column.name + " = list;");
                file.WriteLine("}");
                file.WriteLine("catch (Exception ex)");
                file.WriteLine("{");
                file.WriteLine("LogClass.LogToFileException(ex,\"\");");
                file.WriteLine("}");

                file.WriteLine("}");
            }
        }

        public static void AddAllForeignToViewBag(StreamWriter file, Table table)
        {
            file.WriteLine("private void AddAllComboboxToViewBag(bool isAll)");
            file.WriteLine("{");

            foreach (ForeignKey item in table.foreigns)
            {
                file.WriteLine("Add" + item.table.name + "ToViewBagAs"+item.column.name+"(isAll);");
            }
            file.WriteLine("}");
        }
        #endregion

        #region UploadDiv
        public static void WriteFaceUploadDiv(StreamWriter file, Table table)
        {
            foreach (Column item in table.columns)
            {
                if (item.columntype == ColumnType.Image || item.columntype == ColumnType.File)
                {
                    file.WriteLine("public ActionResult _Upload" + item.name + "(FormCollection coll, string RedirectUrl)");
                    file.WriteLine("{");
                    file.WriteLine("try");
                    file.WriteLine("{");

                    if (!Directory.Exists(Constants.rootPath+"/Content/images/" + table.name))
                    {
                        Directory.CreateDirectory(Constants.rootPath + "/Content/images/" + table.name);
                    }

                    file.WriteLine("string fileUrl = \"/Content/images/" + table.name + "/\" + System.IO.Path.GetFileName(Request.Files[\"file" + item.name + "\"].FileName);");
                    file.WriteLine("var fileName = this.Server.MapPath(Path.Combine(\"~.\", fileUrl));");
                    file.WriteLine("string fileUrlthumb = \"/Content/images/" + table.name + "/thumb_\" + System.IO.Path.GetFileName(Request.Files[\"file" + item.name + "\"].FileName);");
                    file.WriteLine("var fileNamethumb = this.Server.MapPath(Path.Combine(\"~.\", fileUrlthumb));");
                    file.WriteLine("FileInfo finfo = new FileInfo(fileName);");
                    file.WriteLine(table.name + " _" + table.name + " = new " + table.name + "();");
                    file.WriteLine("if (finfo.Exists == false) Request.Files[\"file" + item.name + "\"].SaveAs(fileName);");
                    file.WriteLine(table.name + "Model _" + table.name + "Model = _" + table.name + ".Get" + table.name + "(Convert.ToInt32(coll[\"PrimaryID\"]));");
                    file.WriteLine("_" + table.name + "Model."+item.name+" = fileUrl;");
                    file.WriteLine("_" + table.name + ".Update(_" + table.name + "Model);");
                    file.WriteLine("}");
                    file.WriteLine("catch (Exception ex)");
                    file.WriteLine("{");
                    file.WriteLine("LogClass.LogToFileException(ex,\"_Upload" + item.name + ", Admin" + table.name + "Controller\");");
                    file.WriteLine("}");

                    file.WriteLine("return Redirect(RedirectUrl);");
                    file.WriteLine("}");
                }

            }

        }
        #endregion

        #region Index_Edit_Create_Detail_Delete

        #region index

        public static void WriteIndex(StreamWriter file, Table table)
        {
            file.WriteLine("[Aut]");
            file.WriteLine("public ActionResult Index()");
            file.WriteLine("{");

            file.WriteLine("AddAllComboboxToViewBag(true);");

            file.WriteLine("try");
            file.WriteLine("{");
                file.WriteLine("SearchList list = new SearchList();");
                file.WriteLine("" + table.name + " " + table.name + " = new " + table.name + "();");
                file.WriteLine("return View(" + table.name + ".Get" + table.name + "(list));");
                
            file.WriteLine("}");
            file.WriteLine("catch (Exception ex)");
            file.WriteLine("{");
                file.WriteLine("LogClass.LogToFileException(ex, \"_" + table.name + ", Admin" + table.name + "Controller\");");
                file.WriteLine("return View(new List<"+table.name+"Model>());");
            file.WriteLine("}");
           
            file.WriteLine("}");
            file.WriteLine("");
        }
        #endregion

        #region detail
        public static void WriteDetail(StreamWriter file, Table table)
        {
            
            file.WriteLine("");
            file.WriteLine("[Aut]");
            file.WriteLine("public ActionResult Details(" + table.primery_column.dataType + " id)");
            file.WriteLine("{");
            file.WriteLine("try");
            file.WriteLine("{");
            file.WriteLine("" + table.name + " _" + table.name + " = new " + table.name + "();");
            file.WriteLine("return View(_" + table.name + ".Get" + table.name + "(id));");
            file.WriteLine("}");
            file.WriteLine(" catch (Exception ex)");
            file.WriteLine("{");
            file.WriteLine("LogClass.LogToFileException(ex, \"Details, Admin" + table.name + "Controller\");");
            file.WriteLine("}");
            file.WriteLine("return View();");
            file.WriteLine("}");
            file.WriteLine("");
            
        }
        #endregion

        #region create
        public static void WriteCreate(StreamWriter file, Table table)
        {
            
            file.WriteLine("");
            file.WriteLine("[Aut]");
            file.WriteLine("public ActionResult Create("+GetForeignParameter(table)+")");
            file.WriteLine("{");
            file.WriteLine("try");
            file.WriteLine("{");

            if (table.foreigns.Count > 0)
            {
                foreach (ForeignKey item in table.foreigns)
                {
                    file.WriteLine("ViewBag.Param" + item.column.name + " = " + item.column.name + ";");
                }
            }

          

            file.WriteLine("AddAllComboboxToViewBag(false);");
            file.WriteLine("}");
            file.WriteLine("catch (Exception ex)");
            file.WriteLine("{");
            file.WriteLine("LogClass.LogToFileException(ex, \"Create, Admin" + table.name + "Controller\");");
            file.WriteLine("}");
            file.WriteLine("return View();");
            file.WriteLine("}");
            file.WriteLine("");
        }

        public static void WriteCreate(StreamWriter file, Table table,ForeignKey foreign)
        {

            file.WriteLine("");
            file.WriteLine("[Aut]");
            file.WriteLine("public ActionResult CreateWith" + foreign.table.name + "(" + foreign.column.dataType + " id)");
            file.WriteLine("{");
            file.WriteLine("try");
            file.WriteLine("{");
            file.WriteLine(" ViewBag.ID = id;");
            file.WriteLine("AddAllComboboxToViewBag(false);");
            file.WriteLine("}");
            file.WriteLine("catch (Exception ex)");
            file.WriteLine("{");
            file.WriteLine("LogClass.LogToFileException(ex, \"CreateWith" + foreign.table.name + ", Admin" + table.name + "Controller\");");
            file.WriteLine("}");
            file.WriteLine("return View();");
            file.WriteLine("}");
            file.WriteLine("");
        }
        #endregion

        #region create post
        public static void WriteCreatePost(StreamWriter file, Table table)
        {

            file.WriteLine("[Aut]");
            file.WriteLine("[HttpPost, ValidateInput(false)]");
            file.WriteLine("public ActionResult Create(" + table.name + "Model model, FormCollection collection,string RedirectUrl)");
            file.WriteLine("{");
            file.WriteLine("AlertModel alertmodel = null;");
            file.WriteLine("" + table.name + " _" + table.name + " = new " + table.name + "();");
            file.WriteLine("try");
            file.WriteLine("{");
            file.WriteLine("_" + table.name + ".Insert(model);");
            file.WriteLine("alertmodel = new AlertModel { alertType = AlertTypeEnum.Success, alert = \"Kayıt Başarıyla oluşturulmuştur.\", header = \"Oluşturma Başarılı\" };");
            file.WriteLine("}");
            file.WriteLine("catch(Exception ex)");
            file.WriteLine("{");
            file.WriteLine(" alertmodel = new AlertModel { alertType = AlertTypeEnum.Danger, alert = ex.Message, header = \"Hata Oluştu. İşlem Başarısız\" };");
            file.WriteLine("LogClass.LogToFileException(ex, \"Create, Admin" + table.name + "Controller\");");
            file.WriteLine("AddAllComboboxToViewBag(false);");
            file.WriteLine("}");
            file.WriteLine("Session.Add(\"SystemAlert\", alertmodel);");
            file.WriteLine("return Redirect(RedirectUrl);");
            file.WriteLine("}");
            file.WriteLine("");


            
        }
        #endregion

        #region edit
        public static void WriteEdit(StreamWriter file, Table table)
        {
            string fparam = GetForeignParameter(table);
            if (fparam != "") fparam = "," + fparam;
            file.WriteLine("");
            file.WriteLine("[Aut]");
            file.WriteLine("public ActionResult Edit(" + table.primery_column.dataType + " id "+fparam+")");
            file.WriteLine("{");
            file.WriteLine("try");
            file.WriteLine("{");


            if (table.foreigns.Count > 0)
            {
                foreach (ForeignKey item in table.foreigns)
                {
                    file.WriteLine("ViewBag.Param" + item.column.name + " = " + item.column.name + ";");
                }
            }

            file.WriteLine(" " + table.name + " _" + table.name + " = new " + table.name + "();");
            file.WriteLine("AddAllComboboxToViewBag(false);");
            file.WriteLine("return View(_" + table.name + ".Get" + table.name + "(id));");
            file.WriteLine("}");
            file.WriteLine("catch (Exception ex)");
            file.WriteLine("{");
            file.WriteLine("LogClass.LogToFileException(ex, \"Edit, Admin" + table.name + "Controller\");");
            file.WriteLine("}");
            file.WriteLine("return View();");
            file.WriteLine("}");
            file.WriteLine("");
            
        }

        public static void WriteEdit(StreamWriter file, Table table, ForeignKey foreign)
        {

            file.WriteLine("");
            file.WriteLine("[Aut]");
            file.WriteLine("public ActionResult EditWith" + foreign.table.name + "(" + table.primery_column.dataType + " id)");
            file.WriteLine("{");
            file.WriteLine("try");
            file.WriteLine("{");
            file.WriteLine(" " + table.name + " _" + table.name + " = new " + table.name + "();");
            file.WriteLine("AddAllComboboxToViewBag(false);");
            file.WriteLine("return View(_" + table.name + ".Get" + table.name + "(id));");
            file.WriteLine("}");
            file.WriteLine("catch (Exception ex)");
            file.WriteLine("{");
            file.WriteLine("LogClass.LogToFileException(ex, \"Edit, Admin" + table.name + "Controller\");");
            file.WriteLine("}");
            file.WriteLine("return View();");
            file.WriteLine("}");
            file.WriteLine("");

        }
        #endregion

        #region edit post
        public static void WriteEditPost(StreamWriter file, Table table)
        {

            file.WriteLine("[Aut]");
            file.WriteLine("[HttpPost, ValidateInput(false)]");
            file.WriteLine("public ActionResult Edit(" + table.primery_column.dataType + " id," + table.name + "Model model, FormCollection collection,string RedirectUrl)");
            file.WriteLine("{");
            file.WriteLine("AlertModel alertmodel = null;");
            file.WriteLine("try");
            file.WriteLine("{");
            file.WriteLine("model." + table.primery_column.name + " = id;");
            file.WriteLine(table.name + " _" + table.name + " = new " + table.name + "();");
            file.WriteLine("_" + table.name + ".Update(model);");
            file.WriteLine("alertmodel = new AlertModel { alertType = AlertTypeEnum.Success, alert = \"Kayıt Başarıyla güncellenmiştir.\", header = \"Güncelleme Başarılı\" };");
            file.WriteLine("}");
            file.WriteLine("catch(Exception ex)");
            file.WriteLine("{");
            file.WriteLine("alertmodel = new AlertModel { alertType = AlertTypeEnum.Danger, alert = ex.Message, header = \"Hata Oluştu. İşlem Başarısız\" };");
            file.WriteLine("LogClass.LogToFileException(ex, \"Edit, Admin" + table.name + "Controller\");");
            file.WriteLine("AddAllComboboxToViewBag(false);");
            file.WriteLine("}");
            file.WriteLine("Session.Add(\"SystemAlert\", alertmodel);");
            file.WriteLine("return Redirect(RedirectUrl);");
            file.WriteLine("}");
            file.WriteLine("");

        }
        #endregion

        #region delete
        public static void WriteDelete(StreamWriter file, Table table)
        {
            file.WriteLine("[Aut]");
            file.WriteLine("public ActionResult Delete(" + table.primery_column.dataType + " id,string redirectUrl=\"\")");
            file.WriteLine("{");
            file.WriteLine("AlertModel alertmodel = null;");
            file.WriteLine("try");
            file.WriteLine("{");
            file.WriteLine("" + table.name + " _" + table.name + " = new " + table.name + "();");
            file.WriteLine("_" + table.name + ".Delete(id);");
            file.WriteLine("alertmodel = new AlertModel { alertType = AlertTypeEnum.Success, alert = \"Kayıt Başarıyla silinmiştir.\", header = \"Silme Başarılı\" };");
            //file.WriteLine("return RedirectToAction(\"Index\");");
            file.WriteLine("}");
            file.WriteLine("catch (Exception ex)");
            file.WriteLine("{");
            file.WriteLine(" alertmodel = new AlertModel { alertType = AlertTypeEnum.Danger, alert = ex.Message, header = \"Hata Oluştu. İşlem Başarısız\" };");
            file.WriteLine("LogClass.LogToFileException(ex, \"Delete, Admin" + table.name + "Controller\");");
            file.WriteLine("}");

            file.WriteLine("Session.Add(\"SystemAlert\", alertmodel);");
            file.WriteLine("if(redirectUrl==\"\")");
                file.WriteLine("return RedirectToAction(\"Index\");");
            file.WriteLine("else");
                file.WriteLine("return Redirect(redirectUrl);");
            file.WriteLine("}");

        }
        #endregion


        #endregion


        internal static void WriteResetFilter(StreamWriter file, Table table)
        {
            file.WriteLine("public ActionResult ResetFilter()");
            file.WriteLine("{");
            file.WriteLine("Session[\"" + table.name + "LastPage\"]=1;");
            file.WriteLine("Session[\"" + table.name + "SearchList\"]=null;");
            file.WriteLine("return RedirectToAction(\"Index\");");

            file.WriteLine("}");

        }

        internal static void WritePartial(StreamWriter file, Table table)
        {
            file.WriteLine("[Aut]");
            file.WriteLine("public ActionResult _"+table.name+"(" + GetForeignParameter(table) + ")");
            file.WriteLine("{");
            file.WriteLine("try");
            file.WriteLine("{");
            file.WriteLine("SearchList list=new SearchList();");

    

            foreach (ForeignKey item in table.foreigns)
            {
                       file.WriteLine("ViewBag.Param" + item.column.name + " = " + item.column.name + ";");
                       file.WriteLine("if (" + item.column.name + " != 0) {");
                       file.WriteLine("list.ItemList.Add(new SearchListItem(\"" + item.column.name + "\"," + item.column.name + ", SearchTypes.Equal));}");
            }
            file.WriteLine("" + table.name + " " + table.name + " = new " + table.name + "();");

            file.WriteLine("return View(" + table.name + ".Get" + table.name + "(list));");
            
            file.WriteLine("}");
            file.WriteLine("catch (Exception ex)");
            file.WriteLine("{");
            file.WriteLine("LogClass.LogToFileException(ex, \"_" + table.name + ", Admin" + table.name + "Controller\");");
            file.WriteLine("return RedirectToAction(\"Index\");");
            file.WriteLine("}");
            file.WriteLine("}");
        }
    }
}
