﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Avs.Win.Generate
{
    class BasicController
    {
        public Table table { get; set; }
        
        public string filePath { get; set; }

        public BasicController(Table table)
        {
            this.table = table;
            this.filePath = Constants.ControllerFolder + "\\A" + table.name + "Controller.cs";
        }
        public void Write()
        {
            StreamWriter file = new StreamWriter(filePath,false, Encoding.UTF8);//(filePath);
            file.WriteLine("using System.Web.Mvc;");
            file.WriteLine("using Avs.Web.MgmtReport.Models;");
            
            file.WriteLine("namespace Avs.Web.MgmtReport.Controllers");
            file.WriteLine("{");
             file.WriteLine("public partial class A" + table.name + "Controller : Controller");
             file.WriteLine("{");

            file.WriteLine("public ActionResult Index()");

            file.WriteLine("{");
            file.WriteLine("" + table.name + "ViewModel _" + table.name + " = new " + table.name + "ViewModel();");
            file.WriteLine("_" + table.name + ".FillModel();");
            file.WriteLine("_" + table.name + ".FillSelectList();");
            file.WriteLine("return View(_" + table.name + ");");
            file.WriteLine("}");


            file.WriteLine("public ActionResult Create()");
            file.WriteLine("{");
            file.WriteLine("" + table.name + "FormModel _" + table.name + " = new " + table.name + "FormModel();");
            file.WriteLine("_" + table.name + ".FillSelectList();");
            file.WriteLine("return View(_" + table.name + ");");
            file.WriteLine("}");


            file.WriteLine("public ActionResult Graph()");
            file.WriteLine("{");
            file.WriteLine("return View();");
            file.WriteLine("}");

            file.WriteLine("}");//end class
            file.WriteLine("}"); //end namespace
            //ortak satırlar
                file.Close();
           
              
                UniverseAdmin_BasicNew basicNew = new UniverseAdmin_BasicNew(table);
                basicNew.Write();

            UniverseAdmin_BasicIndex basicIndex = new UniverseAdmin_BasicIndex(table);
            basicIndex.Write();




        }
    }
}
