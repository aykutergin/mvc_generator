﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Avs.Win.Generate
{
    class HomeController
    {

        public string filePath { get; set; }
        public HomeController()
        {
            this.filePath = Constants.ControllerFolder + "\\AdminHomeController.cs";
        }
        public void Write()
        {
            //controller yazılacak
            StreamWriter file = new StreamWriter(filePath,false, Encoding.UTF8);//(filePath);
            file.WriteLine("using System;");
            file.WriteLine("using System.Collections.Generic;");
            file.WriteLine("using System.Linq;");
            file.WriteLine("using System.Web;");
            file.WriteLine("using System.Web.Mvc;");

            file.WriteLine("namespace myLibrary.Controllers");
            file.WriteLine("{");
            file.WriteLine(" public partial class AdminHomeController : Controller");
            file.WriteLine("{");

            file.WriteLine("[Aut]");
            file.WriteLine("public ActionResult Index()");
            file.WriteLine("{");
            file.WriteLine("return View();");
            file.WriteLine("}");


            file.WriteLine("}");
            file.WriteLine("}");
            file.Close();

            Constants.ViewTableFolder = Constants.ViewFolder + "//AdminHome";
            tools.IsExistCreateDirectory(Constants.ViewTableFolder);
            GreatAdmin_HomeIndex homeindex = new GreatAdmin_HomeIndex();
            homeindex.Write();
        }
    }
}
