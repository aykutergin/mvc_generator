﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Avs.Win.Generate
{
    class ReferanceController
    {
        public Table table { get; set; }
    
        //public ForeignKey foreign { get; set; }
        public string filePath { get; set; }

        public ReferanceController(Table table)
        {
            this.table = table;
           // this.foreign = foreign;
            this.filePath = Constants.ControllerFolder + "\\Admin" + table.name + "ReferencesController.cs";
            
        }
        public void Write()
        {
            StreamWriter file = new StreamWriter(filePath,false, Encoding.UTF8);//(filePath);
            ControllerSharedClasses.WriteNamespace(file);
            file.WriteLine("using myLibrary;");
            file.WriteLine("namespace myLibrary.Controllers");
            file.WriteLine("{");
            file.WriteLine("public partial class Admin" + table.name + "ReferencesController : Controller");
            file.WriteLine("{");

            //ortak satırlar
            if (table.name.ToLower().Contains("imageurl"))
            {
                //image controller view
            }
            else if (table.name.ToLower().Contains("fileurl"))
            {
                //file controller view
            }
            else
            {
                ControllerSharedClasses.WriteIndex(file, table);
                _Menu menu = new _Menu(table.primery_column.description, "Index", "Admin" + table.name + "References");
                Constants.refMenuList.Add(menu);

                ControllerSharedClasses.WriteFilter(file, table);
                ControllerSharedClasses.WriteResetFilter(file, table);

                foreach (ForeignKey item in table.ref_foreign)
                {
                    ControllerSharedClasses.WriteInsideSort(file, table, item);
                }
                ControllerSharedClasses.AddForeignToViewBag(file, table);

                ControllerSharedClasses.AddAllForeignToViewBag(file, table);

                //index controller


            }
            file.WriteLine("}");
            file.WriteLine("}");

            file.Close();

            GreatAdmin_ReferanceIndex refIndex = new GreatAdmin_ReferanceIndex(table);
            refIndex.Write();
            GreatAdmin_ReferanceIndex_Partial refIndex_Partial = new GreatAdmin_ReferanceIndex_Partial(table);
            refIndex_Partial.Write();

            foreach (ForeignKey foreign in table.ref_foreign)
            {
               
                    GreatAdmin_ReferanceIndex_Partial_Refpart refIndex_Partial_refpart = new GreatAdmin_ReferanceIndex_Partial_Refpart(table, foreign);
                    refIndex_Partial_refpart.Write();
               
            }
            
     
        }
    }
}
